-- -----------------------------------------------------
-- Table profile
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS profile (
  id BIGINT NOT NULL,
  firstname VARCHAR(45) NULL DEFAULT NULL,
  lastname VARCHAR(255) NOT NULL,
  gender CHAR(1) NOT NULL,
  birthdate DATE NULL DEFAULT NULL,
  day VARCHAR(45) NULL DEFAULT NULL,
  month VARCHAR(45) NULL DEFAULT NULL,
  year VARCHAR(45) NULL DEFAULT NULL,
  email VARCHAR(255) NOT NULL,
  alternate_email VARCHAR(45) NULL DEFAULT NULL,
  password VARCHAR(255) NOT NULL,
  repassword VARCHAR(45) NULL DEFAULT NULL,
  enabled BOOLEAN NULL DEFAULT NULL,
  account_non_expired BOOLEAN NULL DEFAULT NULL,
  credentials_non_expired BOOLEAN NULL DEFAULT NULL,
  account_non_locked BOOLEAN NULL DEFAULT NULL,
  create_date TIMESTAMP NULL DEFAULT NULL,
  ip_address VARCHAR(255) NULL DEFAULT NULL,
  token VARCHAR(255) NULL DEFAULT NULL,
  role VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table availability
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS availability (
  id VARCHAR(45) NOT NULL,
  availability_day VARCHAR(45) NULL DEFAULT NULL,
  availability_time VARCHAR(45) NULL DEFAULT NULL,
  profile_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_day_of_availability_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

;

CREATE INDEX fk_day_of_availability_profile1_idx ON availability (profile_id ASC);


-- -----------------------------------------------------
-- Table country
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS country (
  id CHAR(3) NOT NULL,
  iso3 CHAR(3) NULL DEFAULT NULL,
  name VARCHAR(200) NOT NULL,
  continent_id VARCHAR(3) NOT NULL,
  currencycode VARCHAR(45) NULL,
  currencyname VARCHAR(45) NULL DEFAULT NULL,
  phone VARCHAR(45) NULL DEFAULT NULL,
  language VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table province
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS province (
  id CHAR(15) NOT NULL,
  name VARCHAR(45) NOT NULL,
  nameascii VARCHAR(45) NULL DEFAULT NULL,
  country_id CHAR(3) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_province_country1
    FOREIGN KEY (country_id)
    REFERENCES country (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_province_country1_idx ON province (country_id ASC);


-- -----------------------------------------------------
-- Table city
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS city (
  id VARCHAR(45) NOT NULL,
  name VARCHAR(45) NOT NULL,
  asciiname VARCHAR(45) NULL,
  latitude VARCHAR(45) NULL DEFAULT NULL,
  longitude VARCHAR(45) NULL,
  province_id CHAR(15) NOT NULL,
  timezone VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_city_province1
    FOREIGN KEY (province_id)
    REFERENCES province (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_city_province1_idx ON city (province_id ASC);


-- -----------------------------------------------------
-- Table location
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS location (
  id VARCHAR(255) NOT NULL,
  location_string VARCHAR(45) NULL DEFAULT NULL,
  location_zip_code VARCHAR(45) NULL DEFAULT NULL,
  country_id CHAR(3) NOT NULL,
  province_id CHAR(15) NOT NULL,
  city_id VARCHAR(45) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_location_city1
    FOREIGN KEY (city_id)
    REFERENCES city (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_location_country1
    FOREIGN KEY (country_id)
    REFERENCES country (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_location_province1
    FOREIGN KEY (province_id)
    REFERENCES province (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_location_country1_idx ON location (country_id ASC);

CREATE INDEX fk_location_province1_idx ON location (province_id ASC);

CREATE INDEX fk_location_city1_idx ON location (city_id ASC);


-- -----------------------------------------------------
-- Table property
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS property (
  id BIGINT NOT NULL,
  property_date_on_market VARCHAR(45) NULL DEFAULT NULL,
  property_description TEXT NULL DEFAULT NULL,
  property_num_of_bathrooms INT NULL DEFAULT NULL,
  property_num_of_bedrooms INT NULL DEFAULT NULL,
  property_price FLOAT NULL DEFAULT NULL,
  property_save_date VARCHAR(45) NULL DEFAULT NULL,
  property_sq_feet VARCHAR(45) NULL DEFAULT NULL,
  profile_id INT NOT NULL,
  property_sale_type VARCHAR(45) NULL DEFAULT NULL,
  property_num_of_floors VARCHAR(45) NULL DEFAULT NULL,
  property_type VARCHAR(45) NULL DEFAULT NULL,
  property_features TEXT NULL DEFAULT NULL,
  property_age VARCHAR(45) NULL DEFAULT NULL,
  location_id VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_property_location1
    FOREIGN KEY (location_id)
    REFERENCES location (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_property_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_property_profile_idx ON property (profile_id ASC);

CREATE INDEX fk_property_location1_idx ON property (location_id ASC);


-- -----------------------------------------------------
-- Table proposal
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS proposal (
  id BIGINT NOT NULL,
  construction_of_property VARCHAR(45) NULL DEFAULT NULL,
  type_of_accommodation VARCHAR(45) NULL DEFAULT NULL,
  property_type VARCHAR(45) NULL DEFAULT NULL,
  age_of_property VARCHAR(45) NULL DEFAULT NULL,
  water VARCHAR(45) NULL DEFAULT NULL,
  ground VARCHAR(45) NULL DEFAULT NULL,
  type_of_exterior_siding VARCHAR(45) NULL DEFAULT NULL,
  windows_siding VARCHAR(45) NULL DEFAULT NULL,
  sewer VARCHAR(45) NULL DEFAULT NULL,
  profile_id INT NOT NULL,
  proposal_type VARCHAR(45) NULL DEFAULT NULL,
  location_id VARCHAR(255) NULL DEFAULT NULL,
  minimum_price FLOAT NULL DEFAULT NULL,
  maximum_price FLOAT NULL DEFAULT NULL,
  windows_construction VARCHAR(45) NULL DEFAULT NULL,
  bedrooms_min VARCHAR(45) NULL,
  bedrooms_max VARCHAR(45) NULL DEFAULT NULL,
  bathrooms_min VARCHAR(45) NULL,
  bathrooms_max VARCHAR(45) NULL DEFAULT NULL,
  master_bedroom_with_bathroom VARCHAR(45) NULL DEFAULT NULL,
  garage VARCHAR(45) NULL DEFAULT NULL,
  private_entry VARCHAR(45) NULL DEFAULT NULL,
  car_shelter VARCHAR(45) NULL DEFAULT NULL,
  security_facilities VARCHAR(45) NULL DEFAULT NULL,
  unhindered_access VARCHAR(45) NULL DEFAULT NULL,
  parking VARCHAR(45) NULL DEFAULT NULL,
  furnished VARCHAR(45) NULL DEFAULT NULL,
  features VARCHAR(255) NULL DEFAULT NULL,
  property_type_in_proposal VARCHAR(45) NULL DEFAULT NULL,
  date_created TIMESTAMP NULL DEFAULT NULL,
  number_of_views INT NULL DEFAULT 0,
  number_of_answer_propositions_seen INT NULL DEFAULT 0,
  number_of_answer_propositions_unseen INT NULL DEFAULT 0,
  rush_priority VARCHAR(45) NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_proposal_location1
    FOREIGN KEY (location_id)
    REFERENCES location (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_proposal_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_proposal_profile_idx ON proposal (profile_id ASC);

CREATE INDEX fk_proposal_location1_idx ON proposal (location_id ASC);


-- -----------------------------------------------------
-- Table binding
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS binding (
  id BIGINT NOT NULL,
  date_of_binding TIMESTAMP NOT NULL,
  property_id BIGINT NOT NULL,
  status_reading VARCHAR(45) NULL,
  status_interest VARCHAR(45) NULL,
  status_highlights VARCHAR(45) NULL,
  profile_id INT NOT NULL,
  proposal_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_binding_property1
    FOREIGN KEY (property_id)
    REFERENCES property (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_binding_items_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_binding_proposal1
    FOREIGN KEY (proposal_id)
    REFERENCES proposal (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_binding_property1_idx ON binding (property_id ASC);

CREATE INDEX fk_binding_items_profile1_idx ON binding (profile_id ASC);

CREATE INDEX fk_binding_proposal1_idx ON binding (proposal_id ASC);


-- -----------------------------------------------------
-- Table budget
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS budget (
  id BIGINT NOT NULL,
  scenario VARCHAR(45) NULL DEFAULT NULL,
  income_applicant VARCHAR(45) NULL DEFAULT NULL,
  income_co_applicant VARCHAR(45) NULL DEFAULT NULL,
  income_others VARCHAR(45) NULL DEFAULT NULL,
  total_income VARCHAR(45) NULL DEFAULT NULL,
  debts_credit_card VARCHAR(45) NULL DEFAULT NULL,
  debts_car_loans VARCHAR(45) NULL DEFAULT NULL,
  debts_other_expenses VARCHAR(45) NULL DEFAULT NULL,
  fee_property_tax VARCHAR(45) NULL DEFAULT NULL,
  fee_condo VARCHAR(45) NULL DEFAULT NULL,
  fee_heating_costs VARCHAR(45) NULL DEFAULT NULL,
  maximum_borrowing_capacity VARCHAR(45) NULL DEFAULT NULL,
  down_payment_entered VARCHAR(45) NULL DEFAULT NULL,
  acompte_from_down_payment_entered VARCHAR(45) NULL DEFAULT NULL,
  mortgage_loan_insurance VARCHAR(45) NULL DEFAULT NULL,
  total_of_mortgage VARCHAR(45) NULL DEFAULT NULL,
  depreciation_period VARCHAR(45) NULL DEFAULT NULL,
  electricity VARCHAR(45) NULL DEFAULT NULL,
  water VARCHAR(45) NULL DEFAULT NULL,
  maintenance_repairs VARCHAR(45) NULL DEFAULT NULL,
  mortgage VARCHAR(45) NULL DEFAULT NULL,
  cable_satellite VARCHAR(45) NULL DEFAULT NULL,
  internet VARCHAR(45) NULL DEFAULT NULL,
  landline VARCHAR(45) NULL DEFAULT NULL,
  mobiles VARCHAR(45) NULL DEFAULT NULL,
  other_communications VARCHAR(45) NULL DEFAULT NULL,
  grocery VARCHAR(45) NULL DEFAULT NULL,
  childcare_costs VARCHAR(45) NULL DEFAULT NULL,
  alimony VARCHAR(45) NULL DEFAULT NULL,
  tuition VARCHAR(45) NULL DEFAULT NULL,
  clothes VARCHAR(45) NULL DEFAULT NULL,
  animal_care VARCHAR(45) NULL DEFAULT NULL,
  other_household VARCHAR(45) NULL DEFAULT NULL,
  restaurant_meals VARCHAR(45) NULL DEFAULT NULL,
  movies_concerts VARCHAR(45) NULL DEFAULT NULL,
  newspapers_books VARCHAR(45) NULL DEFAULT NULL,
  hobbies_social_clubs VARCHAR(45) NULL DEFAULT NULL,
  other_entertainment VARCHAR(45) NULL DEFAULT NULL,
  savings_investment VARCHAR(45) NULL DEFAULT NULL,
  loans_lines_of_credit VARCHAR(45) NULL DEFAULT NULL,
  charitable_donations VARCHAR(45) NULL DEFAULT NULL,
  other_finances VARCHAR(45) NULL DEFAULT NULL,
  habitation VARCHAR(45) NULL DEFAULT NULL,
  automobile VARCHAR(45) NULL DEFAULT NULL,
  life_disability VARCHAR(45) NULL DEFAULT NULL,
  other_insurance VARCHAR(45) NULL DEFAULT NULL,
  pharmaceuticals VARCHAR(45) NULL DEFAULT NULL,
  dental_care VARCHAR(45) NULL DEFAULT NULL,
  vision_care VARCHAR(45) NULL DEFAULT NULL,
  other_health VARCHAR(45) NULL DEFAULT NULL,
  gasoline VARCHAR(45) NULL DEFAULT NULL,
  maintenance VARCHAR(45) NULL DEFAULT NULL,
  parking VARCHAR(45) NULL DEFAULT NULL,
  public_transport VARCHAR(45) NULL DEFAULT NULL,
  other_transport VARCHAR(45) NULL DEFAULT NULL,
  total_monthly_expenses VARCHAR(45) NULL DEFAULT NULL,
  profile_id INT NOT NULL,
  country VARCHAR(45) NULL,
  province VARCHAR(45) NULL,
  city VARCHAR(45) NULL,
  date_created TIMESTAMP NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_budget_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_budget_profile1_idx ON budget (profile_id ASC);


-- -----------------------------------------------------
-- Table calendar
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS calendar (
  id BIGINT NOT NULL,
  start_date TIMESTAMP NULL DEFAULT NULL,
  end_date TIMESTAMP NULL DEFAULT NULL,
  text TEXT NULL DEFAULT NULL,
  profile_id INT NOT NULL,
  property_id INT NOT NULL,
  status VARCHAR(45) NULL DEFAULT NULL,
  profile_id_sender INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_calendar_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_calendar_profile2
    FOREIGN KEY (profile_id_sender)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_calendar_property1
    FOREIGN KEY (property_id)
    REFERENCES property (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_calendar_profile1_idx ON calendar (profile_id ASC);

CREATE INDEX fk_calendar_property1_idx ON calendar (property_id ASC);

CREATE INDEX fk_calendar_profile2_idx ON calendar (profile_id_sender ASC);


-- -----------------------------------------------------
-- Table ip2nation
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS ip2nation (
  id BIGINT NOT NULL,
  country_id CHAR(3) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_ip2nation_country1
    FOREIGN KEY (country_id)
    REFERENCES country (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_ip2nation_country1_idx ON ip2nation (country_id ASC);


-- -----------------------------------------------------
-- Table listing_label
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS listing_label (
  id BIGINT NOT NULL,
  listing_label_name VARCHAR(45) NULL DEFAULT NULL,
  listing_label_value VARCHAR(45) NULL DEFAULT NULL,
  listing_label_type VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table location_has_profile
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS location_has_profile (
  id VARCHAR(45) NOT NULL,
  location_id VARCHAR(255) NOT NULL,
  profile_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_location_has_profile_location1
    FOREIGN KEY (location_id)
    REFERENCES location (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_location_has_profile_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

;

CREATE INDEX fk_location_has_profile_profile1_idx ON location_has_profile (profile_id ASC);

CREATE INDEX fk_location_has_profile_location1_idx ON location_has_profile (location_id ASC);


-- -----------------------------------------------------
-- Table phone
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS phone (
  id BIGINT NOT NULL,
  phone_name VARCHAR(45) NULL,
  phone_number VARCHAR(45) NULL DEFAULT NULL,
  phone_call_availability VARCHAR(45) NULL DEFAULT NULL,
  profile_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_phone_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_phone_profile1_idx ON phone (profile_id ASC);


-- -----------------------------------------------------
-- Table profile_notification
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS profile_notification (
  id VARCHAR(45) NOT NULL,
  profile_id INT NOT NULL,
  profile_id_notifier INT NOT NULL,
  notification_verb VARCHAR(45) NULL DEFAULT NULL,
  notification_object VARCHAR(45) NULL,
  notification_status VARCHAR(45) NULL DEFAULT NULL,
  notification_date TIMESTAMP NULL,
  notification_target VARCHAR(45) NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_profile_has_profile_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_profile_has_profile_profile2
    FOREIGN KEY (profile_id_notifier)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_profile_has_profile_profile2_idx ON profile_notification (profile_id_notifier ASC);

CREATE INDEX fk_profile_has_profile_profile1_idx ON profile_notification (profile_id ASC);


-- -----------------------------------------------------
-- Table property_photo
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS property_photo (
  id BIGINT NOT NULL,
  name VARCHAR(45) NULL DEFAULT NULL,
  thumbnail_filename VARCHAR(45) NULL DEFAULT NULL,
  new_filename VARCHAR(45) NULL DEFAULT NULL,
  content_type VARCHAR(45) NULL DEFAULT NULL,
  size BIGINT NULL,
  thumbnail_size BIGINT NULL DEFAULT NULL,
  date_created VARCHAR(45) NULL DEFAULT NULL,
  last_updated VARCHAR(45) NULL DEFAULT NULL,
  url VARCHAR(45) NULL DEFAULT NULL,
  thumbnail_url VARCHAR(45) NULL DEFAULT NULL,
  delete_url VARCHAR(45) NULL DEFAULT NULL,
  delete_type VARCHAR(45) NULL DEFAULT NULL,
  property_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_property_photo_property1
    FOREIGN KEY (property_id)
    REFERENCES property (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_property_photo_property1_idx ON property_photo (property_id ASC);


-- -----------------------------------------------------
-- Table profile_photo
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS profile_photo (
  id INT NOT NULL,
  datecreated TIMESTAMP NULL,
  lastupdated TIMESTAMP NULL,
  name VARCHAR(45) NULL,
  content_type VARCHAR(45) NULL,
  newFilename VARCHAR(45) NULL,
  size BIGINT NULL,
  thumbnailfilename VARCHAR(45) NULL,
  thumbnailsize BIGINT NULL,
  profile_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_profile_photo_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_profile_photo_profile1_idx ON profile_photo (profile_id ASC);


-- -----------------------------------------------------
-- Table proposal_search
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS proposal_search (
  id BIGINT NOT NULL,
  construction_of_property VARCHAR(45) NULL DEFAULT NULL,
  type_of_accommodation VARCHAR(45) NULL DEFAULT NULL,
  property_type VARCHAR(45) NULL DEFAULT NULL,
  age_of_property VARCHAR(45) NULL DEFAULT NULL,
  water VARCHAR(45) NULL DEFAULT NULL,
  ground VARCHAR(45) NULL DEFAULT NULL,
  type_of_exterior_siding VARCHAR(45) NULL DEFAULT NULL,
  windows_siding VARCHAR(45) NULL DEFAULT NULL,
  sewer VARCHAR(45) NULL DEFAULT NULL,
  profile_id INT NOT NULL,
  proposal_type VARCHAR(45) NULL DEFAULT NULL,
  minimum_price FLOAT NULL DEFAULT NULL,
  maximum_price FLOAT NULL DEFAULT NULL,
  windows_construction VARCHAR(45) NULL DEFAULT NULL,
  bedrooms_min VARCHAR(45) NULL,
  bedrooms_max VARCHAR(45) NULL DEFAULT NULL,
  bathrooms_min VARCHAR(45) NULL,
  bathrooms_max VARCHAR(45) NULL DEFAULT NULL,
  master_bedroom_with_bathroom VARCHAR(45) NULL DEFAULT NULL,
  garage VARCHAR(45) NULL DEFAULT NULL,
  private_entry VARCHAR(45) NULL DEFAULT NULL,
  car_shelter VARCHAR(45) NULL DEFAULT NULL,
  security_facilities VARCHAR(45) NULL DEFAULT NULL,
  unhindered_access VARCHAR(45) NULL DEFAULT NULL,
  parking VARCHAR(45) NULL DEFAULT NULL,
  furnished VARCHAR(45) NULL DEFAULT NULL,
  features VARCHAR(255) NULL DEFAULT NULL,
  property_type_in_proposal VARCHAR(45) NULL DEFAULT NULL,
  date_created DATE NULL DEFAULT NULL,
  total_binding INT NULL DEFAULT 0,
  total_status_reading_seen INT NULL DEFAULT 0,
  total_status_reading_unseen INT NULL DEFAULT 0,
  rush_priority VARCHAR(45) NULL,
  name_search VARCHAR(45) NULL,
  country VARCHAR(45) NULL,
  province VARCHAR(45) NULL,
  city VARCHAR(45) NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_proposal_profile10
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_proposal_profile1_idx ON proposal_search (profile_id ASC);


-- -----------------------------------------------------
-- Table property_search
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS property_search (
  id BIGINT NOT NULL,
  property_num_of_bathrooms INT NULL DEFAULT NULL,
  property_num_of_bedrooms INT NULL DEFAULT NULL,
  property_price_min DOUBLE PRECISION NULL DEFAULT NULL,
  property_search_save_date VARCHAR(45) NULL DEFAULT NULL,
  property_sq_feet VARCHAR(45) NULL DEFAULT NULL,
  profile_id BIGINT NOT NULL,
  property_sale_type VARCHAR(45) NULL,
  property_num_of_floors VARCHAR(45) NULL,
  property_type VARCHAR(45) NULL DEFAULT NULL,
  property_features TEXT NULL DEFAULT NULL,
  property_age VARCHAR(45) NULL DEFAULT NULL,
  country VARCHAR(45) NULL,
  city VARCHAR(45) NULL,
  province VARCHAR(45) NULL,
  name_search VARCHAR(45) NULL,
  property_price_max DOUBLE PRECISION NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_property_profile10
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_property_profile1_idx ON property_search (profile_id ASC);


-- -----------------------------------------------------
-- Table profile_settings
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS profile_settings (
  id BIGINT NOT NULL,
  show_getting_started BOOLEAN NULL,
  show_information_completed BOOLEAN NULL,
  show_prequalification_completed BOOLEAN NULL,
  show_search_criteria_property_completed BOOLEAN NULL,
  show_search_criteria_renting_proposal_completed BOOLEAN NULL,
  show_search_criteria_buying_proposal_completed VARCHAR(45) NULL,
  profile_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_profile_settings_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_profile_settings_profile1_idx ON profile_settings (profile_id ASC);


-- -----------------------------------------------------
-- Table membership_payment
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS membership_payment (
  id INT NOT NULL,
  membership_payment_date TIMESTAMP NULL,
  membership_payment_amount VARCHAR(45) NULL,
  membership_payment_status VARCHAR(45) NULL,
  membership_payment_method VARCHAR(45) NULL,
  membership_payment_expiring_date TIMESTAMP NULL,
  profile_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_membership_payment_profile1
    FOREIGN KEY (profile_id)
    REFERENCES profile (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_membership_payment_profile1_idx ON membership_payment (profile_id ASC);


-- -----------------------------------------------------
-- Table view_property
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS view_property (
  id INT NOT NULL,
  ip_count_view VARCHAR(45) NULL,
  ip__name_view VARCHAR(45) NULL,
  property_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_view_property_property1
    FOREIGN KEY (property_id)
    REFERENCES property (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_view_property_property1_idx ON view_property (property_id ASC);


-- -----------------------------------------------------
-- Table view_proposal
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS view_proposal (
  id INT NOT NULL,
  ip_count_view VARCHAR(45) NULL,
  ip_name_view VARCHAR(45) NULL,
  proposal_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_view_proposal_proposal1
    FOREIGN KEY (proposal_id)
    REFERENCES proposal (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_view_proposal_proposal1_idx ON view_proposal (proposal_id ASC);
