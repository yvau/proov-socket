INSERT INTO public.country(id, continent_id, currencycode, currencyname, iso3, language, name, phone)
  VALUES('CA', 'NA', 'CAD', 'Dollar', 'CAN', 'en-CA,fr-CA,iu', 'Canada', '1');
INSERT INTO public.country(id, continent_id, currencycode, currencyname, iso3, language, name, phone)
  VALUES('US', 'NA', 'USD', 'Dollar', 'USA', 'en-US,es-US,haw,fr', 'United States', '1');
