INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.01', 'Alberta', 'Alberta', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.02', 'British Columbia', 'British Columbia', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.03', 'Manitoba', 'Manitoba', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.04', 'New Brunswick', 'New Brunswick', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.13', 'Northwest Territories', 'Northwest Territories', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.07', 'Nova Scotia', 'Nova Scotia', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.14', 'Nunavut', 'Nunavut', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.08', 'Ontario', 'Ontario', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.09', 'Prince Edward Island', 'Prince Edward Island', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.10', 'Quebec', 'Quebec', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.11', 'Saskatchewan', 'Saskatchewan', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.12', 'Yukon', 'Yukon', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('CA.05', 'Newfoundland and Labrador', 'Newfoundland and Labrador', 'CA');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.AR', 'Arkansas', 'Arkansas', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.DC', 'Washington, D.C.', 'Washington, D.C.', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.DE', 'Delaware', 'Delaware', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.FL', 'Florida', 'Florida', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.GA', 'Georgia', 'Georgia', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.KS', 'Kansas', 'Kansas', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.LA', 'Louisiana', 'Louisiana', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.MD', 'Maryland', 'Maryland', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.MO', 'Missouri', 'Missouri', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.MS', 'Mississippi', 'Mississippi', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.NC', 'North Carolina', 'North Carolina', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.OK', 'Oklahoma', 'Oklahoma', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.SC', 'South Carolina', 'South Carolina', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.TN', 'Tennessee', 'Tennessee', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.TX', 'Texas', 'Texas', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.WV', 'West Virginia', 'West Virginia', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.AL', 'Alabama', 'Alabama', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.CT', 'Connecticut', 'Connecticut', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.IA', 'Iowa', 'Iowa', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.IL', 'Illinois', 'Illinois', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.IN', 'Indiana', 'Indiana', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.ME', 'Maine', 'Maine', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.MI', 'Michigan', 'Michigan', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.MN', 'Minnesota', 'Minnesota', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.NE', 'Nebraska', 'Nebraska', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.NH', 'New Hampshire', 'New Hampshire', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.NJ', 'New Jersey', 'New Jersey', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.NY', 'New York', 'New York', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.OH', 'Ohio', 'Ohio', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.RI', 'Rhode Island', 'Rhode Island', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.VT', 'Vermont', 'Vermont', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.WI', 'Wisconsin', 'Wisconsin', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.CA', 'California', 'California', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.CO', 'Colorado', 'Colorado', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.NM', 'New Mexico', 'New Mexico', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.NV', 'Nevada', 'Nevada', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.UT', 'Utah', 'Utah', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.AZ', 'Arizona', 'Arizona', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.ID', 'Idaho', 'Idaho', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.MT', 'Montana', 'Montana', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.ND', 'North Dakota', 'North Dakota', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.OR', 'Oregon', 'Oregon', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.SD', 'South Dakota', 'South Dakota', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.WA', 'Washington', 'Washington', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.WY', 'Wyoming', 'Wyoming', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.HI', 'Hawaii', 'Hawaii', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.AK', 'Alaska', 'Alaska', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.KY', 'Kentucky', 'Kentucky', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.MA', 'Massachusetts', 'Massachusetts', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.PA', 'Pennsylvania', 'Pennsylvania', 'US');
INSERT INTO public.province(id, name, nameascii, country_id)
  VALUES('US.VA', 'Virginia', 'Virginia', 'US');
