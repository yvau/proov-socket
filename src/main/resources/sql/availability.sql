INSERT INTO public.availability(id, availability_day, availability_time, profile_id)
  VALUES('1.sunday', 'sunday', '10:00 Am,11:00 Am', 1);
INSERT INTO public.availability(id, availability_day, availability_time, profile_id)
  VALUES('1.monday', 'monday', '10:00 Am,11:00 Am,12:00 Pm,1:00 Pm,2:00 Pm', 1);
INSERT INTO public.availability(id, availability_day, availability_time, profile_id)
  VALUES('1.tuesday', 'tuesday', '11:00 Am,12:00 Pm,1:00 Pm,2:00 Pm,3:00 Pm', 1);
INSERT INTO public.availability(id, availability_day, availability_time, profile_id)
  VALUES('1.wednesday', 'wednesday', '11:00 Am,12:00 Pm,1:00 Pm,2:00 Pm,3:00 Pm,4:00 Pm,5:00 Pm', 1);
INSERT INTO public.availability(id, availability_day, availability_time, profile_id)
  VALUES('1.thursday', 'thursday', '9:00 Am,10:00 Am,11:00 Am,12:00 Pm,5:00 Pm', 1);
INSERT INTO public.availability(id, availability_day, availability_time, profile_id)
  VALUES('1.friday', 'friday', '12:00 Pm,1:00 Pm,2:00 Pm,3:00 Pm,4:00 Pm', 1);
INSERT INTO public.availability(id, availability_day, availability_time, profile_id)
  VALUES('1.saturday', 'saturday', '9:00 Am,10:00 Am,11:00 Am,12:00 Pm,4:00 Pm', 1);
