package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the view_property database table.
 * 
 */
@Entity
@Table(name="view_property")
@NamedQuery(name="ViewProperty.findAll", query="SELECT v FROM ViewProperty v")
public class ViewProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="ip__name_view")
	private String ipNameView;

	@Column(name="ip_count_view")
	private int ipCountView;

	//bi-directional many-to-one association to Property
	@ManyToOne
	private Property property;

	public ViewProperty() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIpNameView() {
		return this.ipNameView;
	}

	public void setIpNameView(String ipNameView) {
		this.ipNameView = ipNameView;
	}

	public int getIpCountView() {
		return this.ipCountView;
	}

	public void setIpCountView(int ipCountView) {
		this.ipCountView = ipCountView;
	}

	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

}