package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the profile_photo database table.
 * 
 */
@Entity
@Table(name="profile_photo")
@NamedQuery(name="ProfilePhoto.findAll", query="SELECT p FROM ProfilePhoto p")
public class ProfilePhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(name="content_type")
	private String contentType;

	private Timestamp datecreated;

	private Timestamp lastupdated;

	private String name;

	private String newfilename;

	private Long size;

	private String thumbnailfilename;

	private Long thumbnailsize;

	//bi-directional one-to-one association to Profile
	@OneToOne
	private Profile profile;

	public ProfilePhoto() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Timestamp getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(Timestamp datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getLastupdated() {
		return this.lastupdated;
	}

	public void setLastupdated(Timestamp lastupdated) {
		this.lastupdated = lastupdated;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNewfilename() {
		return this.newfilename;
	}

	public void setNewfilename(String newfilename) {
		this.newfilename = newfilename;
	}

	public Long getSize() {
		return this.size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getThumbnailfilename() {
		return this.thumbnailfilename;
	}

	public void setThumbnailfilename(String thumbnailfilename) {
		this.thumbnailfilename = thumbnailfilename;
	}

	public Long getThumbnailsize() {
		return this.thumbnailsize;
	}

	public void setThumbnailsize(Long thumbnailsize) {
		this.thumbnailsize = thumbnailsize;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}