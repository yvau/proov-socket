package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the listing_label database table.
 * 
 */
@Entity
@Table(name="listing_label")
@NamedQuery(name="ListingLabel.findAll", query="SELECT l FROM ListingLabel l")
public class ListingLabel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="listing_label_name")
	private String listingLabelName;

	@Column(name="listing_label_type")
	private String listingLabelType;

	@Column(name="listing_label_value")
	private String listingLabelValue;

	public ListingLabel() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getListingLabelName() {
		return this.listingLabelName;
	}

	public void setListingLabelName(String listingLabelName) {
		this.listingLabelName = listingLabelName;
	}

	public String getListingLabelType() {
		return this.listingLabelType;
	}

	public void setListingLabelType(String listingLabelType) {
		this.listingLabelType = listingLabelType;
	}

	public String getListingLabelValue() {
		return this.listingLabelValue;
	}

	public void setListingLabelValue(String listingLabelValue) {
		this.listingLabelValue = listingLabelValue;
	}

}