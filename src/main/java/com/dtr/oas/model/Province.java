package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the province database table.
 * 
 */
@Entity
@NamedQuery(name="Province.findAll", query="SELECT p FROM Province p")
public class Province implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String id;

	private String name;

	private String nameascii;

	//bi-directional many-to-one association to City
	@OneToMany(mappedBy="province")
	private List<City> cities;

	//bi-directional many-to-one association to Location
	@OneToMany(mappedBy="province")
	private List<Location> locations;

	//bi-directional many-to-one association to Country
	@ManyToOne
	private Country country;

	public Province() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameascii() {
		return this.nameascii;
	}

	public void setNameascii(String nameascii) {
		this.nameascii = nameascii;
	}

	public List<City> getCities() {
		return this.cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public City addCity(City city) {
		getCities().add(city);
		city.setProvince(this);

		return city;
	}

	public City removeCity(City city) {
		getCities().remove(city);
		city.setProvince(null);

		return city;
	}

	public List<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Location addLocation(Location location) {
		getLocations().add(location);
		location.setProvince(this);

		return location;
	}

	public Location removeLocation(Location location) {
		getLocations().remove(location);
		location.setProvince(null);

		return location;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}