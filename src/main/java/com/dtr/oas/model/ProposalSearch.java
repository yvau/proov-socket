package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the proposal_search database table.
 * 
 */
@Entity
@Table(name="proposal_search")
@NamedQuery(name="ProposalSearch.findAll", query="SELECT p FROM ProposalSearch p")
public class ProposalSearch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="age_of_property")
	private String ageOfProperty;

	@Column(name="bathrooms_max")
	private String bathroomsMax;

	@Column(name="bathrooms_min")
	private String bathroomsMin;

	@Column(name="bedrooms_max")
	private String bedroomsMax;

	@Column(name="bedrooms_min")
	private String bedroomsMin;

	@Column(name="car_shelter")
	private String carShelter;

	private String city;

	@Column(name="construction_of_property")
	private String constructionOfProperty;

	private String country;

	@Temporal(TemporalType.DATE)
	@Column(name="date_created")
	private Date dateCreated;

	private String features;

	private String furnished;

	private String garage;

	private String ground;

	@Column(name="master_bedroom_with_bathroom")
	private String masterBedroomWithBathroom;

	@Column(name="maximum_price")
	private double maximumPrice;

	@Column(name="minimum_price")
	private double minimumPrice;

	@Column(name="name_search")
	private String nameSearch;

	private String parking;

	@Column(name="private_entry")
	private String privateEntry;

	@Column(name="property_type")
	private String propertyType;

	@Column(name="property_type_in_proposal")
	private String propertyTypeInProposal;

	@Column(name="proposal_type")
	private String proposalType;

	private String province;

	@Column(name="rush_priority")
	private String rushPriority;

	@Column(name="security_facilities")
	private String securityFacilities;

	private String sewer;

	@Column(name="total_binding")
	private Integer totalBinding;

	@Column(name="total_status_reading_seen")
	private Integer totalStatusReadingSeen;

	@Column(name="total_status_reading_unseen")
	private Integer totalStatusReadingUnseen;

	@Column(name="type_of_accommodation")
	private String typeOfAccommodation;

	@Column(name="type_of_exterior_siding")
	private String typeOfExteriorSiding;

	@Column(name="unhindered_access")
	private String unhinderedAccess;

	private String water;

	@Column(name="windows_construction")
	private String windowsConstruction;

	@Column(name="windows_siding")
	private String windowsSiding;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	public ProposalSearch() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgeOfProperty() {
		return this.ageOfProperty;
	}

	public void setAgeOfProperty(String ageOfProperty) {
		this.ageOfProperty = ageOfProperty;
	}

	public String getBathroomsMax() {
		return this.bathroomsMax;
	}

	public void setBathroomsMax(String bathroomsMax) {
		this.bathroomsMax = bathroomsMax;
	}

	public String getBathroomsMin() {
		return this.bathroomsMin;
	}

	public void setBathroomsMin(String bathroomsMin) {
		this.bathroomsMin = bathroomsMin;
	}

	public String getBedroomsMax() {
		return this.bedroomsMax;
	}

	public void setBedroomsMax(String bedroomsMax) {
		this.bedroomsMax = bedroomsMax;
	}

	public String getBedroomsMin() {
		return this.bedroomsMin;
	}

	public void setBedroomsMin(String bedroomsMin) {
		this.bedroomsMin = bedroomsMin;
	}

	public String getCarShelter() {
		return this.carShelter;
	}

	public void setCarShelter(String carShelter) {
		this.carShelter = carShelter;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getConstructionOfProperty() {
		return this.constructionOfProperty;
	}

	public void setConstructionOfProperty(String constructionOfProperty) {
		this.constructionOfProperty = constructionOfProperty;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getFeatures() {
		return this.features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getFurnished() {
		return this.furnished;
	}

	public void setFurnished(String furnished) {
		this.furnished = furnished;
	}

	public String getGarage() {
		return this.garage;
	}

	public void setGarage(String garage) {
		this.garage = garage;
	}

	public String getGround() {
		return this.ground;
	}

	public void setGround(String ground) {
		this.ground = ground;
	}

	public String getMasterBedroomWithBathroom() {
		return this.masterBedroomWithBathroom;
	}

	public void setMasterBedroomWithBathroom(String masterBedroomWithBathroom) {
		this.masterBedroomWithBathroom = masterBedroomWithBathroom;
	}

	public double getMaximumPrice() {
		return this.maximumPrice;
	}

	public void setMaximumPrice(double maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	public double getMinimumPrice() {
		return this.minimumPrice;
	}

	public void setMinimumPrice(double minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	public String getNameSearch() {
		return this.nameSearch;
	}

	public void setNameSearch(String nameSearch) {
		this.nameSearch = nameSearch;
	}

	public String getParking() {
		return this.parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public String getPrivateEntry() {
		return this.privateEntry;
	}

	public void setPrivateEntry(String privateEntry) {
		this.privateEntry = privateEntry;
	}

	public String getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getPropertyTypeInProposal() {
		return this.propertyTypeInProposal;
	}

	public void setPropertyTypeInProposal(String propertyTypeInProposal) {
		this.propertyTypeInProposal = propertyTypeInProposal;
	}

	public String getProposalType() {
		return this.proposalType;
	}

	public void setProposalType(String proposalType) {
		this.proposalType = proposalType;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getRushPriority() {
		return this.rushPriority;
	}

	public void setRushPriority(String rushPriority) {
		this.rushPriority = rushPriority;
	}

	public String getSecurityFacilities() {
		return this.securityFacilities;
	}

	public void setSecurityFacilities(String securityFacilities) {
		this.securityFacilities = securityFacilities;
	}

	public String getSewer() {
		return this.sewer;
	}

	public void setSewer(String sewer) {
		this.sewer = sewer;
	}

	public Integer getTotalBinding() {
		return this.totalBinding;
	}

	public void setTotalBinding(Integer totalBinding) {
		this.totalBinding = totalBinding;
	}

	public Integer getTotalStatusReadingSeen() {
		return this.totalStatusReadingSeen;
	}

	public void setTotalStatusReadingSeen(Integer totalStatusReadingSeen) {
		this.totalStatusReadingSeen = totalStatusReadingSeen;
	}

	public Integer getTotalStatusReadingUnseen() {
		return this.totalStatusReadingUnseen;
	}

	public void setTotalStatusReadingUnseen(Integer totalStatusReadingUnseen) {
		this.totalStatusReadingUnseen = totalStatusReadingUnseen;
	}

	public String getTypeOfAccommodation() {
		return this.typeOfAccommodation;
	}

	public void setTypeOfAccommodation(String typeOfAccommodation) {
		this.typeOfAccommodation = typeOfAccommodation;
	}

	public String getTypeOfExteriorSiding() {
		return this.typeOfExteriorSiding;
	}

	public void setTypeOfExteriorSiding(String typeOfExteriorSiding) {
		this.typeOfExteriorSiding = typeOfExteriorSiding;
	}

	public String getUnhinderedAccess() {
		return this.unhinderedAccess;
	}

	public void setUnhinderedAccess(String unhinderedAccess) {
		this.unhinderedAccess = unhinderedAccess;
	}

	public String getWater() {
		return this.water;
	}

	public void setWater(String water) {
		this.water = water;
	}

	public String getWindowsConstruction() {
		return this.windowsConstruction;
	}

	public void setWindowsConstruction(String windowsConstruction) {
		this.windowsConstruction = windowsConstruction;
	}

	public String getWindowsSiding() {
		return this.windowsSiding;
	}

	public void setWindowsSiding(String windowsSiding) {
		this.windowsSiding = windowsSiding;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}