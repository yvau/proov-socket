package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the availability database table.
 * 
 */
@Entity
@NamedQuery(name="Availability.findAll", query="SELECT a FROM Availability a")
public class Availability implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String id;

	@Column(name="availability_day")
	private String availabilityDay;

	@Column(name="availability_time")
	private String availabilityTime;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	public Availability() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAvailabilityDay() {
		return this.availabilityDay;
	}

	public void setAvailabilityDay(String availabilityDay) {
		this.availabilityDay = availabilityDay;
	}

	public String getAvailabilityTime() {
		return this.availabilityTime;
	}

	public void setAvailabilityTime(String availabilityTime) {
		this.availabilityTime = availabilityTime;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}