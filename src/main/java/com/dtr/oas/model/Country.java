package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the country database table.
 * 
 */
@Entity
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")
public class Country implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String id;

	@Column(name="continent_id")
	private String continentId;

	private String currencycode;

	private String currencyname;

	private String iso3;

	private String language;

	private String name;

	private String phone;

	//bi-directional many-to-one association to Ip2nation
	@OneToMany(mappedBy="country")
	private List<Ip2nation> ip2nations;

	//bi-directional many-to-one association to Location
	@OneToMany(mappedBy="country")
	private List<Location> locations;

	//bi-directional many-to-one association to Province
	@OneToMany(mappedBy="country")
	private List<Province> provinces;

	public Country() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContinentId() {
		return this.continentId;
	}

	public void setContinentId(String continentId) {
		this.continentId = continentId;
	}

	public String getCurrencycode() {
		return this.currencycode;
	}

	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}

	public String getCurrencyname() {
		return this.currencyname;
	}

	public void setCurrencyname(String currencyname) {
		this.currencyname = currencyname;
	}

	public String getIso3() {
		return this.iso3;
	}

	public void setIso3(String iso3) {
		this.iso3 = iso3;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Ip2nation> getIp2nations() {
		return this.ip2nations;
	}

	public void setIp2nations(List<Ip2nation> ip2nations) {
		this.ip2nations = ip2nations;
	}

	public Ip2nation addIp2nation(Ip2nation ip2nation) {
		getIp2nations().add(ip2nation);
		ip2nation.setCountry(this);

		return ip2nation;
	}

	public Ip2nation removeIp2nation(Ip2nation ip2nation) {
		getIp2nations().remove(ip2nation);
		ip2nation.setCountry(null);

		return ip2nation;
	}

	public List<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Location addLocation(Location location) {
		getLocations().add(location);
		location.setCountry(this);

		return location;
	}

	public Location removeLocation(Location location) {
		getLocations().remove(location);
		location.setCountry(null);

		return location;
	}

	public List<Province> getProvinces() {
		return this.provinces;
	}

	public void setProvinces(List<Province> provinces) {
		this.provinces = provinces;
	}

	public Province addProvince(Province province) {
		getProvinces().add(province);
		province.setCountry(this);

		return province;
	}

	public Province removeProvince(Province province) {
		getProvinces().remove(province);
		province.setCountry(null);

		return province;
	}

}