package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the profile_notification database table.
 * 
 */
@Entity
@Table(name="profile_notification")
@NamedQuery(name="ProfileNotification.findAll", query="SELECT p FROM ProfileNotification p")
public class ProfileNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Column(name="notification_date")
	private Timestamp notificationDate;

	@Column(name="notification_object")
	private String notificationObject;

	@Column(name="notification_status")
	private String notificationStatus;

	@Column(name="notification_target")
	private String notificationTarget;

	@Column(name="notification_verb")
	private String notificationVerb;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name="profile_id")
	private Profile profile1;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name="profile_id_notifier")
	private Profile profile2;

	public ProfileNotification() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getNotificationDate() {
		return this.notificationDate;
	}

	public void setNotificationDate(Timestamp notificationDate) {
		this.notificationDate = notificationDate;
	}

	public String getNotificationObject() {
		return this.notificationObject;
	}

	public void setNotificationObject(String notificationObject) {
		this.notificationObject = notificationObject;
	}

	public String getNotificationStatus() {
		return this.notificationStatus;
	}

	public void setNotificationStatus(String notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	public String getNotificationTarget() {
		return this.notificationTarget;
	}

	public void setNotificationTarget(String notificationTarget) {
		this.notificationTarget = notificationTarget;
	}

	public String getNotificationVerb() {
		return this.notificationVerb;
	}

	public void setNotificationVerb(String notificationVerb) {
		this.notificationVerb = notificationVerb;
	}

	public Profile getProfile1() {
		return this.profile1;
	}

	public void setProfile1(Profile profile1) {
		this.profile1 = profile1;
	}

	public Profile getProfile2() {
		return this.profile2;
	}

	public void setProfile2(Profile profile2) {
		this.profile2 = profile2;
	}

}