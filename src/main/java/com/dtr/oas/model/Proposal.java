package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the proposal database table.
 * 
 */
@Entity
@NamedQuery(name="Proposal.findAll", query="SELECT p FROM Proposal p")
public class Proposal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="age_of_property")
	private String ageOfProperty;

	@Column(name="bathrooms_max")
	private Integer bathroomsMax;

	@Column(name="bathrooms_min")
	private Integer bathroomsMin;

	@Column(name="bedrooms_max")
	private Integer bedroomsMax;

	@Column(name="bedrooms_min")
	private Integer bedroomsMin;

	@Column(name="car_shelter")
	private String carShelter;

	@Column(name="construction_of_property")
	private String constructionOfProperty;

	@Column(name="date_created")
	private Timestamp dateCreated;

	private String features;

	private String furnished;

	private String garage;

	private String ground;

	@Column(name="master_bedroom_with_bathroom")
	private String masterBedroomWithBathroom;

	@Column(name="maximum_price")
	private double maximumPrice;

	@Column(name="minimum_price")
	private double minimumPrice;

	@Column(name="number_of_answer_propositions_seen")
	private Integer numberOfAnswerPropositionsSeen;

	@Column(name="number_of_answer_propositions_unseen")
	private Integer numberOfAnswerPropositionsUnseen;

	@Column(name="number_of_views")
	private Integer numberOfViews;

	private String parking;

	@Column(name="private_entry")
	private String privateEntry;

	@Column(name="property_type")
	private String propertyType;

	@Column(name="property_type_in_proposal")
	private String propertyTypeInProposal;

	@Column(name="proposal_type")
	private String proposalType;

	@Column(name="rush_priority")
	private String rushPriority;

	@Column(name="security_facilities")
	private String securityFacilities;

	private String sewer;
	
	@Column(name="type_of_accommodation")
	private String typeOfAccommodation;

	@Column(name="type_of_exterior_siding")
	private String typeOfExteriorSiding;

	@Column(name="unhindered_access")
	private String unhinderedAccess;

	private String water;

	@Column(name="windows_construction")
	private String windowsConstruction;

	@Column(name="windows_siding")
	private String windowsSiding;

	//bi-directional many-to-one association to Binding
	@OneToMany(mappedBy="proposal")
	private List<Binding> bindings;

	//bi-directional many-to-one association to Location
	@ManyToOne(cascade = CascadeType.ALL)
	private Location location;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to ViewProposal
	@OneToMany(mappedBy="proposal")
	private List<ViewProposal> viewProposals;

	public Proposal() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgeOfProperty() {
		return this.ageOfProperty;
	}

	public void setAgeOfProperty(String ageOfProperty) {
		this.ageOfProperty = ageOfProperty;
	}

	public Integer getBathroomsMax() {
		return this.bathroomsMax;
	}

	public void setBathroomsMax(Integer bathroomsMax) {
		this.bathroomsMax = bathroomsMax;
	}

	public Integer getBathroomsMin() {
		return this.bathroomsMin;
	}

	public void setBathroomsMin(Integer bathroomsMin) {
		this.bathroomsMin = bathroomsMin;
	}

	public Integer getBedroomsMax() {
		return this.bedroomsMax;
	}

	public void setBedroomsMax(Integer bedroomsMax) {
		this.bedroomsMax = bedroomsMax;
	}

	public Integer getBedroomsMin() {
		return this.bedroomsMin;
	}

	public void setBedroomsMin(Integer bedroomsMin) {
		this.bedroomsMin = bedroomsMin;
	}

	public String getCarShelter() {
		return this.carShelter;
	}

	public void setCarShelter(String carShelter) {
		this.carShelter = carShelter;
	}

	public String getConstructionOfProperty() {
		return this.constructionOfProperty;
	}

	public void setConstructionOfProperty(String constructionOfProperty) {
		this.constructionOfProperty = constructionOfProperty;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getFeatures() {
		return this.features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getFurnished() {
		return this.furnished;
	}

	public void setFurnished(String furnished) {
		this.furnished = furnished;
	}

	public String getGarage() {
		return this.garage;
	}

	public void setGarage(String garage) {
		this.garage = garage;
	}

	public String getGround() {
		return this.ground;
	}

	public void setGround(String ground) {
		this.ground = ground;
	}

	public String getMasterBedroomWithBathroom() {
		return this.masterBedroomWithBathroom;
	}

	public void setMasterBedroomWithBathroom(String masterBedroomWithBathroom) {
		this.masterBedroomWithBathroom = masterBedroomWithBathroom;
	}

	public double getMaximumPrice() {
		return this.maximumPrice;
	}

	public void setMaximumPrice(double maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	public double getMinimumPrice() {
		return this.minimumPrice;
	}

	public void setMinimumPrice(double minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	public Integer getNumberOfAnswerPropositionsSeen() {
		return this.numberOfAnswerPropositionsSeen;
	}

	public void setNumberOfAnswerPropositionsSeen(Integer numberOfAnswerPropositionsSeen) {
		this.numberOfAnswerPropositionsSeen = numberOfAnswerPropositionsSeen;
	}

	public Integer getNumberOfAnswerPropositionsUnseen() {
		return this.numberOfAnswerPropositionsUnseen;
	}

	public void setNumberOfAnswerPropositionsUnseen(Integer numberOfAnswerPropositionsUnseen) {
		this.numberOfAnswerPropositionsUnseen = numberOfAnswerPropositionsUnseen;
	}

	public Integer getNumberOfViews() {
		return this.numberOfViews;
	}

	public void setNumberOfViews(Integer numberOfViews) {
		this.numberOfViews = numberOfViews;
	}

	public String getParking() {
		return this.parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public String getPrivateEntry() {
		return this.privateEntry;
	}

	public void setPrivateEntry(String privateEntry) {
		this.privateEntry = privateEntry;
	}

	public String getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getPropertyTypeInProposal() {
		return this.propertyTypeInProposal;
	}

	public void setPropertyTypeInProposal(String propertyTypeInProposal) {
		this.propertyTypeInProposal = propertyTypeInProposal;
	}

	public String getProposalType() {
		return this.proposalType;
	}

	public void setProposalType(String proposalType) {
		this.proposalType = proposalType;
	}

	public String getRushPriority() {
		return this.rushPriority;
	}

	public void setRushPriority(String rushPriority) {
		this.rushPriority = rushPriority;
	}

	public String getSecurityFacilities() {
		return this.securityFacilities;
	}

	public void setSecurityFacilities(String securityFacilities) {
		this.securityFacilities = securityFacilities;
	}

	public String getSewer() {
		return this.sewer;
	}

	public void setSewer(String sewer) {
		this.sewer = sewer;
	}
	
	public String getTypeOfAccommodation() {
		return this.typeOfAccommodation;
	}

	public void setTypeOfAccommodation(String typeOfAccommodation) {
		this.typeOfAccommodation = typeOfAccommodation;
	}

	public String getTypeOfExteriorSiding() {
		return this.typeOfExteriorSiding;
	}

	public void setTypeOfExteriorSiding(String typeOfExteriorSiding) {
		this.typeOfExteriorSiding = typeOfExteriorSiding;
	}

	public String getUnhinderedAccess() {
		return this.unhinderedAccess;
	}

	public void setUnhinderedAccess(String unhinderedAccess) {
		this.unhinderedAccess = unhinderedAccess;
	}

	public String getWater() {
		return this.water;
	}

	public void setWater(String water) {
		this.water = water;
	}

	public String getWindowsConstruction() {
		return this.windowsConstruction;
	}

	public void setWindowsConstruction(String windowsConstruction) {
		this.windowsConstruction = windowsConstruction;
	}

	public String getWindowsSiding() {
		return this.windowsSiding;
	}

	public void setWindowsSiding(String windowsSiding) {
		this.windowsSiding = windowsSiding;
	}

	public List<Binding> getBindings() {
		return this.bindings;
	}

	public void setBindings(List<Binding> bindings) {
		this.bindings = bindings;
	}

	public Binding addBinding(Binding binding) {
		getBindings().add(binding);
		binding.setProposal(this);

		return binding;
	}

	public Binding removeBinding(Binding binding) {
		getBindings().remove(binding);
		binding.setProposal(null);

		return binding;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<ViewProposal> getViewProposals() {
		return this.viewProposals;
	}

	public void setViewProposals(List<ViewProposal> viewProposals) {
		this.viewProposals = viewProposals;
	}

	public ViewProposal addViewProposal(ViewProposal viewProposal) {
		getViewProposals().add(viewProposal);
		viewProposal.setProposal(this);

		return viewProposal;
	}

	public ViewProposal removeViewProposal(ViewProposal viewProposal) {
		getViewProposals().remove(viewProposal);
		viewProposal.setProposal(null);

		return viewProposal;
	}

}