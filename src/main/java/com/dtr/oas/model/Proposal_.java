package com.dtr.oas.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Proposal.class)
public class Proposal_ {

    public static volatile SingularAttribute<Proposal, Location> location;
    public static volatile SingularAttribute<Proposal, Profile> profile;
    public static volatile SingularAttribute<Proposal, String> proposalType;
    public static volatile SingularAttribute<Proposal, String> propertyTypeInProposal;
    public static volatile SingularAttribute<Proposal, String> rushPriority;
    public static volatile SingularAttribute<Proposal, Double> minimumPrice;
    public static volatile SingularAttribute<Proposal, Double> maximumPrice;
    public static volatile SingularAttribute<Proposal, Integer> bedroomsMin;
    public static volatile SingularAttribute<Proposal, Integer> bedroomsMax;
    public static volatile SingularAttribute<Proposal, Integer> bathroomsMin;
    public static volatile SingularAttribute<Proposal, Integer> bathroomsMax;
    public static volatile SingularAttribute<Proposal, Integer> numberOfAnswerPropositionsUnseen;
    
    
}
