package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the view_proposal database table.
 * 
 */
@Entity
@Table(name="view_proposal")
@NamedQuery(name="ViewProposal.findAll", query="SELECT v FROM ViewProposal v")
public class ViewProposal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="ip_count_view")
	private int ipCountView;

	@Column(name="ip_name_view")
	private String ipNameView;

	//bi-directional many-to-one association to Proposal
	@ManyToOne
	private Proposal proposal;

	public ViewProposal() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getIpCountView() {
		return this.ipCountView;
	}

	public void setIpCountView(int ipCountView) {
		this.ipCountView = ipCountView;
	}

	public String getIpNameView() {
		return this.ipNameView;
	}

	public void setIpNameView(String ipNameView) {
		this.ipNameView = ipNameView;
	}

	public Proposal getProposal() {
		return this.proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

}