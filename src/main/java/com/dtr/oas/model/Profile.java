package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the profile database table.
 */
@Entity
@NamedQuery(name = "Profile.findAll", query = "SELECT p FROM Profile p")
public class Profile implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "account_non_expired")
  private Boolean accountNonExpired;

  @Column(name = "account_non_locked")
  private Boolean accountNonLocked;

  @Column(name = "alternate_email")
  private String alternateEmail;

  @Temporal(TemporalType.DATE)
  private Date birthdate;

  @Column(name = "create_date")
  private Timestamp createDate;

  @Column(name = "credentials_non_expired")
  private Boolean credentialsNonExpired;

  private String email;

  private Boolean enabled;

  private String firstname;

  private String gender;

  @Column(name = "ip_address")
  private String ipAddress;

  private String lastname;

  private String password;

  @Transient
  private String repassword;

  private String role;

  private String token;

  @Column(name = "notification_count")
  private String notificationCount;

  public String getNotificationCount() {
    return notificationCount;
  }

  public void setNotificationCount(String notificationCount) {
    this.notificationCount = notificationCount;
  }

  //bi-directional many-to-one association to Availability
  @OneToMany(mappedBy = "profile")
  private List<Availability> availabilities;

  //bi-directional many-to-one association to Binding
  @OneToMany(mappedBy = "profile")
  private List<Binding> bindings;

  //bi-directional one-to-one association to Budget
  @OneToOne(mappedBy = "profile")
  private Budget budget;

  //bi-directional many-to-one association to Calendar
  @OneToMany(mappedBy = "profile1")
  private List<Calendar> calendars1;

  //bi-directional many-to-one association to Calendar
  @OneToMany(mappedBy = "profile2")
  private List<Calendar> calendars2;

  //bi-directional many-to-one association to LocationHasProfile
  @OneToMany(mappedBy = "profile")
  private List<LocationHasProfile> locationHasProfiles;

  //bi-directional many-to-one association to MembershipPayment
  @OneToMany(mappedBy = "profile")
  private List<MembershipPayment> membershipPayments;

  //bi-directional many-to-one association to Phone
  @OneToMany(mappedBy = "profile")
  private List<Phone> phones;

  //bi-directional many-to-one association to ProfileNotification
  @OneToMany(mappedBy = "profile1")
  private List<ProfileNotification> profileNotifications1;

  //bi-directional many-to-one association to ProfileNotification
  @OneToMany(mappedBy = "profile2")
  private List<ProfileNotification> profileNotifications2;

  //bi-directional one-to-one association to ProfilePhoto
  @OneToOne(mappedBy = "profile")
  private ProfilePhoto profilePhoto;

  //bi-directional one-to-one association to ProfileSetting
  @JsonIgnore
  @OneToOne(mappedBy = "profile")
  private ProfileSetting profileSetting;

  //bi-directional many-to-one association to Property
  @OneToMany(mappedBy = "profile")
  private List<Property> properties;

  //bi-directional many-to-one association to PropertySearch
  @OneToMany(mappedBy = "profile")
  private List<PropertySearch> propertySearches;

  //bi-directional many-to-one association to Proposal
  @OneToMany(mappedBy = "profile")
  private List<Proposal> proposals;

  //bi-directional many-to-one association to ProposalSearch
  @OneToMany(mappedBy = "profile")
  private List<ProposalSearch> proposalSearches;

  public Profile() {
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Boolean getAccountNonExpired() {
    return this.accountNonExpired;
  }

  public void setAccountNonExpired(Boolean accountNonExpired) {
    this.accountNonExpired = accountNonExpired;
  }

  public Boolean getAccountNonLocked() {
    return this.accountNonLocked;
  }

  public void setAccountNonLocked(Boolean accountNonLocked) {
    this.accountNonLocked = accountNonLocked;
  }

  public String getAlternateEmail() {
    return this.alternateEmail;
  }

  public void setAlternateEmail(String alternateEmail) {
    this.alternateEmail = alternateEmail;
  }

  public Date getBirthdate() {
    return this.birthdate;
  }

  public void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }

  public Timestamp getCreateDate() {
    return this.createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public Boolean getCredentialsNonExpired() {
    return this.credentialsNonExpired;
  }

  public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
    this.credentialsNonExpired = credentialsNonExpired;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean getEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public String getFirstname() {
    return this.firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getGender() {
    return this.gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getIpAddress() {
    return this.ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getLastname() {
    return this.lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRepassword() {
    return this.repassword;
  }

  public void setRepassword(String repassword) {
    this.repassword = repassword;
  }

  public String getRole() {
    return this.role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getToken() {
    return this.token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public List<Availability> getAvailabilities() {
    return this.availabilities;
  }

  public void setAvailabilities(List<Availability> availabilities) {
    this.availabilities = availabilities;
  }

  public Availability addAvailability(Availability availability) {
    getAvailabilities().add(availability);
    availability.setProfile(this);

    return availability;
  }

  public Availability removeAvailability(Availability availability) {
    getAvailabilities().remove(availability);
    availability.setProfile(null);

    return availability;
  }

  public List<Binding> getBindings() {
    return this.bindings;
  }

  public void setBindings(List<Binding> bindings) {
    this.bindings = bindings;
  }

  public Binding addBinding(Binding binding) {
    getBindings().add(binding);
    binding.setProfile(this);

    return binding;
  }

  public Binding removeBinding(Binding binding) {
    getBindings().remove(binding);
    binding.setProfile(null);

    return binding;
  }

  public Budget getBudget() {
    return this.budget;
  }

  public void setBudget(Budget budget) {
    this.budget = budget;
  }

  public List<Calendar> getCalendars1() {
    return this.calendars1;
  }

  public void setCalendars1(List<Calendar> calendars1) {
    this.calendars1 = calendars1;
  }

  public Calendar addCalendars1(Calendar calendars1) {
    getCalendars1().add(calendars1);
    calendars1.setProfile1(this);

    return calendars1;
  }

  public Calendar removeCalendars1(Calendar calendars1) {
    getCalendars1().remove(calendars1);
    calendars1.setProfile1(null);

    return calendars1;
  }

  public List<Calendar> getCalendars2() {
    return this.calendars2;
  }

  public void setCalendars2(List<Calendar> calendars2) {
    this.calendars2 = calendars2;
  }

  public Calendar addCalendars2(Calendar calendars2) {
    getCalendars2().add(calendars2);
    calendars2.setProfile2(this);

    return calendars2;
  }

  public Calendar removeCalendars2(Calendar calendars2) {
    getCalendars2().remove(calendars2);
    calendars2.setProfile2(null);

    return calendars2;
  }

  public List<LocationHasProfile> getLocationHasProfiles() {
    return this.locationHasProfiles;
  }

  public void setLocationHasProfiles(List<LocationHasProfile> locationHasProfiles) {
    this.locationHasProfiles = locationHasProfiles;
  }

  public LocationHasProfile addLocationHasProfile(LocationHasProfile locationHasProfile) {
    getLocationHasProfiles().add(locationHasProfile);
    locationHasProfile.setProfile(this);

    return locationHasProfile;
  }

  public LocationHasProfile removeLocationHasProfile(LocationHasProfile locationHasProfile) {
    getLocationHasProfiles().remove(locationHasProfile);
    locationHasProfile.setProfile(null);

    return locationHasProfile;
  }

  public List<MembershipPayment> getMembershipPayments() {
    return this.membershipPayments;
  }

  public void setMembershipPayments(List<MembershipPayment> membershipPayments) {
    this.membershipPayments = membershipPayments;
  }

  public MembershipPayment addMembershipPayment(MembershipPayment membershipPayment) {
    getMembershipPayments().add(membershipPayment);
    membershipPayment.setProfile(this);

    return membershipPayment;
  }

  public MembershipPayment removeMembershipPayment(MembershipPayment membershipPayment) {
    getMembershipPayments().remove(membershipPayment);
    membershipPayment.setProfile(null);

    return membershipPayment;
  }

  public List<Phone> getPhones() {
    return this.phones;
  }

  public void setPhones(List<Phone> phones) {
    this.phones = phones;
  }

  public Phone addPhone(Phone phone) {
    getPhones().add(phone);
    phone.setProfile(this);

    return phone;
  }

  public Phone removePhone(Phone phone) {
    getPhones().remove(phone);
    phone.setProfile(null);

    return phone;
  }

  public List<ProfileNotification> getProfileNotifications1() {
    return this.profileNotifications1;
  }

  public void setProfileNotifications1(List<ProfileNotification> profileNotifications1) {
    this.profileNotifications1 = profileNotifications1;
  }

  public ProfileNotification addProfileNotifications1(ProfileNotification profileNotifications1) {
    getProfileNotifications1().add(profileNotifications1);
    profileNotifications1.setProfile1(this);

    return profileNotifications1;
  }

  public ProfileNotification removeProfileNotifications1(ProfileNotification profileNotifications1) {
    getProfileNotifications1().remove(profileNotifications1);
    profileNotifications1.setProfile1(null);

    return profileNotifications1;
  }

  public List<ProfileNotification> getProfileNotifications2() {
    return this.profileNotifications2;
  }

  public void setProfileNotifications2(List<ProfileNotification> profileNotifications2) {
    this.profileNotifications2 = profileNotifications2;
  }

  public ProfileNotification addProfileNotifications2(ProfileNotification profileNotifications2) {
    getProfileNotifications2().add(profileNotifications2);
    profileNotifications2.setProfile2(this);

    return profileNotifications2;
  }

  public ProfileNotification removeProfileNotifications2(ProfileNotification profileNotifications2) {
    getProfileNotifications2().remove(profileNotifications2);
    profileNotifications2.setProfile2(null);

    return profileNotifications2;
  }

  public ProfilePhoto getProfilePhoto() {
    return this.profilePhoto;
  }

  public void setProfilePhoto(ProfilePhoto profilePhoto) {
    this.profilePhoto = profilePhoto;
  }

  public ProfileSetting getProfileSetting() {
    return this.profileSetting;
  }

  public void setProfileSetting(ProfileSetting profileSetting) {
    this.profileSetting = profileSetting;
  }

  public List<Property> getProperties() {
    return this.properties;
  }

  public void setProperties(List<Property> properties) {
    this.properties = properties;
  }

  public Property addProperty(Property property) {
    getProperties().add(property);
    property.setProfile(this);

    return property;
  }

  public Property removeProperty(Property property) {
    getProperties().remove(property);
    property.setProfile(null);

    return property;
  }

  public List<PropertySearch> getPropertySearches() {
    return this.propertySearches;
  }

  public void setPropertySearches(List<PropertySearch> propertySearches) {
    this.propertySearches = propertySearches;
  }

  public PropertySearch addPropertySearch(PropertySearch propertySearch) {
    getPropertySearches().add(propertySearch);
    propertySearch.setProfile(this);

    return propertySearch;
  }

  public PropertySearch removePropertySearch(PropertySearch propertySearch) {
    getPropertySearches().remove(propertySearch);
    propertySearch.setProfile(null);

    return propertySearch;
  }

  public List<Proposal> getProposals() {
    return this.proposals;
  }

  public void setProposals(List<Proposal> proposals) {
    this.proposals = proposals;
  }

  public Proposal addProposal(Proposal proposal) {
    getProposals().add(proposal);
    proposal.setProfile(this);

    return proposal;
  }

  public Proposal removeProposal(Proposal proposal) {
    getProposals().remove(proposal);
    proposal.setProfile(null);

    return proposal;
  }

  public List<ProposalSearch> getProposalSearches() {
    return this.proposalSearches;
  }

  public void setProposalSearches(List<ProposalSearch> proposalSearches) {
    this.proposalSearches = proposalSearches;
  }

  public ProposalSearch addProposalSearch(ProposalSearch proposalSearch) {
    getProposalSearches().add(proposalSearch);
    proposalSearch.setProfile(this);

    return proposalSearch;
  }

  public ProposalSearch removeProposalSearch(ProposalSearch proposalSearch) {
    getProposalSearches().remove(proposalSearch);
    proposalSearch.setProfile(null);

    return proposalSearch;
  }

}