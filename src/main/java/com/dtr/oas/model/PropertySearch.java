package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the property_search database table.
 * 
 */
@Entity
@Table(name="property_search")
@NamedQuery(name="PropertySearch.findAll", query="SELECT p FROM PropertySearch p")
public class PropertySearch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String city;

	private String country;

	@Column(name="name_search")
	private String nameSearch;

	@Column(name="property_age")
	private String propertyAge;

	@Column(name="property_features")
	private String propertyFeatures;

	@Column(name="property_num_of_bathrooms")
	private Integer propertyNumOfBathrooms;

	@Column(name="property_num_of_bedrooms")
	private Integer propertyNumOfBedrooms;

	@Column(name="property_num_of_floors")
	private String propertyNumOfFloors;

	@Column(name="property_price_max")
	private double propertyPriceMax;

	@Column(name="property_price_min")
	private double propertyPriceMin;

	@Column(name="property_sale_type")
	private String propertySaleType;

	@Column(name="property_search_save_date")
	private String propertySearchSaveDate;

	@Column(name="property_sq_feet")
	private String propertySqFeet;

	@Column(name="property_type")
	private String propertyType;

	private String province;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	public PropertySearch() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNameSearch() {
		return this.nameSearch;
	}

	public void setNameSearch(String nameSearch) {
		this.nameSearch = nameSearch;
	}

	public String getPropertyAge() {
		return this.propertyAge;
	}

	public void setPropertyAge(String propertyAge) {
		this.propertyAge = propertyAge;
	}

	public String getPropertyFeatures() {
		return this.propertyFeatures;
	}

	public void setPropertyFeatures(String propertyFeatures) {
		this.propertyFeatures = propertyFeatures;
	}

	public Integer getPropertyNumOfBathrooms() {
		return this.propertyNumOfBathrooms;
	}

	public void setPropertyNumOfBathrooms(Integer propertyNumOfBathrooms) {
		this.propertyNumOfBathrooms = propertyNumOfBathrooms;
	}

	public Integer getPropertyNumOfBedrooms() {
		return this.propertyNumOfBedrooms;
	}

	public void setPropertyNumOfBedrooms(Integer propertyNumOfBedrooms) {
		this.propertyNumOfBedrooms = propertyNumOfBedrooms;
	}

	public String getPropertyNumOfFloors() {
		return this.propertyNumOfFloors;
	}

	public void setPropertyNumOfFloors(String propertyNumOfFloors) {
		this.propertyNumOfFloors = propertyNumOfFloors;
	}

	public double getPropertyPriceMax() {
		return this.propertyPriceMax;
	}

	public void setPropertyPriceMax(double propertyPriceMax) {
		this.propertyPriceMax = propertyPriceMax;
	}

	public double getPropertyPriceMin() {
		return this.propertyPriceMin;
	}

	public void setPropertyPriceMin(double propertyPriceMin) {
		this.propertyPriceMin = propertyPriceMin;
	}

	public String getPropertySaleType() {
		return this.propertySaleType;
	}

	public void setPropertySaleType(String propertySaleType) {
		this.propertySaleType = propertySaleType;
	}

	public String getPropertySearchSaveDate() {
		return this.propertySearchSaveDate;
	}

	public void setPropertySearchSaveDate(String propertySearchSaveDate) {
		this.propertySearchSaveDate = propertySearchSaveDate;
	}

	public String getPropertySqFeet() {
		return this.propertySqFeet;
	}

	public void setPropertySqFeet(String propertySqFeet) {
		this.propertySqFeet = propertySqFeet;
	}

	public String getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}