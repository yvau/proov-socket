package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the membership_payment database table.
 * 
 */
@Entity
@Table(name="membership_payment")
@NamedQuery(name="MembershipPayment.findAll", query="SELECT m FROM MembershipPayment m")
public class MembershipPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(name="membership_payment_amount")
	private String membershipPaymentAmount;

	@Column(name="membership_payment_date")
	private Timestamp membershipPaymentDate;

	@Column(name="membership_payment_expiring_date")
	private Timestamp membershipPaymentExpiringDate;

	@Column(name="membership_payment_method")
	private String membershipPaymentMethod;

	@Column(name="membership_payment_status")
	private String membershipPaymentStatus;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	public MembershipPayment() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMembershipPaymentAmount() {
		return this.membershipPaymentAmount;
	}

	public void setMembershipPaymentAmount(String membershipPaymentAmount) {
		this.membershipPaymentAmount = membershipPaymentAmount;
	}

	public Timestamp getMembershipPaymentDate() {
		return this.membershipPaymentDate;
	}

	public void setMembershipPaymentDate(Timestamp membershipPaymentDate) {
		this.membershipPaymentDate = membershipPaymentDate;
	}

	public Timestamp getMembershipPaymentExpiringDate() {
		return this.membershipPaymentExpiringDate;
	}

	public void setMembershipPaymentExpiringDate(Timestamp membershipPaymentExpiringDate) {
		this.membershipPaymentExpiringDate = membershipPaymentExpiringDate;
	}

	public String getMembershipPaymentMethod() {
		return this.membershipPaymentMethod;
	}

	public void setMembershipPaymentMethod(String membershipPaymentMethod) {
		this.membershipPaymentMethod = membershipPaymentMethod;
	}

	public String getMembershipPaymentStatus() {
		return this.membershipPaymentStatus;
	}

	public void setMembershipPaymentStatus(String membershipPaymentStatus) {
		this.membershipPaymentStatus = membershipPaymentStatus;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}