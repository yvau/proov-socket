package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Column(name="location_string")
	private String locationString;

	@Column(name="location_zip_code")
	private String locationZipCode;

	//bi-directional many-to-one association to City
	@ManyToOne
	private City city;

	//bi-directional many-to-one association to Country
	@ManyToOne
	private Country country;

	//bi-directional many-to-one association to Province
	@ManyToOne
	private Province province;

	//bi-directional many-to-one association to LocationHasProfile
	@OneToMany(mappedBy="location")
	private List<LocationHasProfile> locationHasProfiles;

	//bi-directional many-to-one association to Property
	@OneToMany(mappedBy="location")
	private List<Property> properties;

	//bi-directional many-to-one association to Proposal
	@OneToMany(mappedBy="location")
	private List<Proposal> proposals;

	public Location() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocationString() {
		return this.locationString;
	}

	public void setLocationString(String locationString) {
		this.locationString = locationString;
	}

	public String getLocationZipCode() {
		return this.locationZipCode;
	}

	public void setLocationZipCode(String locationZipCode) {
		this.locationZipCode = locationZipCode;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public List<LocationHasProfile> getLocationHasProfiles() {
		return this.locationHasProfiles;
	}

	public void setLocationHasProfiles(List<LocationHasProfile> locationHasProfiles) {
		this.locationHasProfiles = locationHasProfiles;
	}

	public LocationHasProfile addLocationHasProfile(LocationHasProfile locationHasProfile) {
		getLocationHasProfiles().add(locationHasProfile);
		locationHasProfile.setLocation(this);

		return locationHasProfile;
	}

	public LocationHasProfile removeLocationHasProfile(LocationHasProfile locationHasProfile) {
		getLocationHasProfiles().remove(locationHasProfile);
		locationHasProfile.setLocation(null);

		return locationHasProfile;
	}

	public List<Property> getProperties() {
		return this.properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	public Property addProperty(Property property) {
		getProperties().add(property);
		property.setLocation(this);

		return property;
	}

	public Property removeProperty(Property property) {
		getProperties().remove(property);
		property.setLocation(null);

		return property;
	}

	public List<Proposal> getProposals() {
		return this.proposals;
	}

	public void setProposals(List<Proposal> proposals) {
		this.proposals = proposals;
	}

	public Proposal addProposal(Proposal proposal) {
		getProposals().add(proposal);
		proposal.setLocation(this);

		return proposal;
	}

	public Proposal removeProposal(Proposal proposal) {
		getProposals().remove(proposal);
		proposal.setLocation(null);

		return proposal;
	}

}