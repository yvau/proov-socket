package com.dtr.oas.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the property_photo database table.
 * 
 */
@Entity
@Table(name="property_photo")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id")
@NamedQuery(name="PropertyPhoto.findAll", query="SELECT p FROM PropertyPhoto p")
public class PropertyPhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="content_type")
	private String contentType;

	@Column(name="date_created")
	private String dateCreated;

	@Column(name="delete_type")
	private String deleteType;

	@Column(name="delete_url")
	private String deleteUrl;

	@Column(name="last_updated")
	private String lastUpdated;

	private String name;

	@Column(name="new_filename")
	private String newFilename;

	private Long size;

	@Column(name="thumbnail_filename")
	private String thumbnailFilename;

	@Column(name="thumbnail_size")
	private Long thumbnailSize;

	@Column(name="thumbnail_url")
	private String thumbnailUrl;

	private String url;

	//bi-directional many-to-one association to Property
	@ManyToOne
	private Property property;

	public PropertyPhoto() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getDeleteType() {
		return this.deleteType;
	}

	public void setDeleteType(String deleteType) {
		this.deleteType = deleteType;
	}

	public String getDeleteUrl() {
		return this.deleteUrl;
	}

	public void setDeleteUrl(String deleteUrl) {
		this.deleteUrl = deleteUrl;
	}

	public String getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNewFilename() {
		return this.newFilename;
	}

	public void setNewFilename(String newFilename) {
		this.newFilename = newFilename;
	}

	public Long getSize() {
		return this.size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getThumbnailFilename() {
		return this.thumbnailFilename;
	}

	public void setThumbnailFilename(String thumbnailFilename) {
		this.thumbnailFilename = thumbnailFilename;
	}

	public Long getThumbnailSize() {
		return this.thumbnailSize;
	}

	public void setThumbnailSize(Long thumbnailSize) {
		this.thumbnailSize = thumbnailSize;
	}

	public String getThumbnailUrl() {
		return this.thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

}