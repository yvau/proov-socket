package com.dtr.oas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the profile_settings database table.
 * 
 */
@Entity
@Table(name="profile_settings")
@NamedQuery(name="ProfileSetting.findAll", query="SELECT p FROM ProfileSetting p")
public class ProfileSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="show_getting_started")
	private Boolean showGettingStarted;

	@Column(name="show_information_completed")
	private Boolean showInformationCompleted;

	@Column(name="show_prequalification_completed")
	private Boolean showPrequalificationCompleted;
	
	@Column(name="show_notification_visit")
	private Boolean showNotificationVisit;
	
	@Column(name="show_notification_property")
	private Boolean showNotificationProperty;
	
	@Column(name="show_notification_renting_proposal")
	private Boolean showNotificationRentingProposal;
	
	@Column(name="show_notification_membership")
	private Boolean showNotificationMembership;
	
	@Column(name="show_notification_buying_proposal")
	private Boolean showNotificationBuyingProposal;

	@Column(name="show_search_criteria_buying_proposal_completed")
	private Boolean showSearchCriteriaBuyingProposalCompleted;

	@Column(name="show_search_criteria_property_completed")
	private Boolean showSearchCriteriaPropertyCompleted;

	@Column(name="show_search_criteria_renting_proposal_completed")
	private Boolean showSearchCriteriaRentingProposalCompleted;

	//bi-directional one-to-one association to Profile
	@JsonIgnore
	@OneToOne
	private Profile profile;

	public ProfileSetting() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getShowGettingStarted() {
		return this.showGettingStarted;
	}

	public void setShowGettingStarted(Boolean showGettingStarted) {
		this.showGettingStarted = showGettingStarted;
	}

	public Boolean getShowInformationCompleted() {
		return this.showInformationCompleted;
	}

	public void setShowInformationCompleted(Boolean showInformationCompleted) {
		this.showInformationCompleted = showInformationCompleted;
	}

	public Boolean getShowPrequalificationCompleted() {
		return this.showPrequalificationCompleted;
	}

	public void setShowPrequalificationCompleted(Boolean showPrequalificationCompleted) {
		this.showPrequalificationCompleted = showPrequalificationCompleted;
	}

	public Boolean getShowSearchCriteriaBuyingProposalCompleted() {
		return this.showSearchCriteriaBuyingProposalCompleted;
	}

	public void setShowSearchCriteriaBuyingProposalCompleted(Boolean showSearchCriteriaBuyingProposalCompleted) {
		this.showSearchCriteriaBuyingProposalCompleted = showSearchCriteriaBuyingProposalCompleted;
	}

	public Boolean getShowSearchCriteriaPropertyCompleted() {
		return this.showSearchCriteriaPropertyCompleted;
	}

	public void setShowSearchCriteriaPropertyCompleted(Boolean showSearchCriteriaPropertyCompleted) {
		this.showSearchCriteriaPropertyCompleted = showSearchCriteriaPropertyCompleted;
	}

	public Boolean getShowSearchCriteriaRentingProposalCompleted() {
		return this.showSearchCriteriaRentingProposalCompleted;
	}

	public void setShowSearchCriteriaRentingProposalCompleted(Boolean showSearchCriteriaRentingProposalCompleted) {
		this.showSearchCriteriaRentingProposalCompleted = showSearchCriteriaRentingProposalCompleted;
	}

	public Boolean getShowNotificationMembership() {
		return showNotificationMembership;
	}

	public void setShowNotificationMembership(Boolean showNotificationMembership) {
		this.showNotificationMembership = showNotificationMembership;
	}

	public Boolean getShowNotificationVisit() {
		return showNotificationVisit;
	}

	public void setShowNotificationVisit(Boolean showNotificationVisit) {
		this.showNotificationVisit = showNotificationVisit;
	}

	public Boolean getShowNotificationProperty() {
		return showNotificationProperty;
	}

	public void setShowNotificationProperty(Boolean showNotificationProperty) {
		this.showNotificationProperty = showNotificationProperty;
	}

	public Boolean getShowNotificationRentingProposal() {
		return showNotificationRentingProposal;
	}

	public void setShowNotificationRentingProposal(Boolean showNotificationRentingProposal) {
		this.showNotificationRentingProposal = showNotificationRentingProposal;
	}

	public Boolean getShowNotificationBuyingProposal() {
		return showNotificationBuyingProposal;
	}

	public void setShowNotificationBuyingProposal(Boolean showNotificationBuyingProposal) {
		this.showNotificationBuyingProposal = showNotificationBuyingProposal;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}