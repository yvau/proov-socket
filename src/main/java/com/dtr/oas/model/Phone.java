package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the phone database table.
 * 
 */
@Entity
@NamedQuery(name="Phone.findAll", query="SELECT p FROM Phone p")
public class Phone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="phone_call_availability")
	private String phoneCallAvailability;

	@Column(name="phone_name")
	private String phoneName;

	@Column(name="phone_number")
	private String phoneNumber;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	public Phone() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhoneCallAvailability() {
		return this.phoneCallAvailability;
	}

	public void setPhoneCallAvailability(String phoneCallAvailability) {
		this.phoneCallAvailability = phoneCallAvailability;
	}

	public String getPhoneName() {
		return this.phoneName;
	}

	public void setPhoneName(String phoneName) {
		this.phoneName = phoneName;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}