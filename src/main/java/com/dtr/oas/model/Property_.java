package com.dtr.oas.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Property.class)
public class Property_ {
	public static volatile SingularAttribute<Property, String> propertyType;
	public static volatile SingularAttribute<Property, String> propertySaleType;
	public static volatile SingularAttribute<Property, Double> propertyPrice;
	public static volatile SingularAttribute<Property, Integer> propertyNumOfBedrooms;
	public static volatile SingularAttribute<Property, Integer> propertyNumOfBathrooms;
	public static volatile SingularAttribute<Property, Integer> propertySqFeet;
	public static volatile SingularAttribute<Property, Integer> propertyNumOfFloors;
	public static volatile SingularAttribute<Property, Location> location;
	public static volatile SingularAttribute<Property, Profile> profile;

}
