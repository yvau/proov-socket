package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ip2nation database table.
 * 
 */
@Entity
@NamedQuery(name="Ip2nation.findAll", query="SELECT i FROM Ip2nation i")
public class Ip2nation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	//bi-directional many-to-one association to Country
	@ManyToOne
	private Country country;

	public Ip2nation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}