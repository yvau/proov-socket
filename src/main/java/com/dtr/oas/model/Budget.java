package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the budget database table.
 * 
 */
@Entity
@NamedQuery(name="Budget.findAll", query="SELECT b FROM Budget b")
public class Budget implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="acompte_from_down_payment_entered")
	private String acompteFromDownPaymentEntered;

	private String alimony;

	@Column(name="animal_care")
	private String animalCare;

	private String automobile;

	@Column(name="cable_satellite")
	private String cableSatellite;

	@Column(name="charitable_donations")
	private String charitableDonations;

	@Column(name="childcare_costs")
	private String childcareCosts;

	private String city;

	private String clothes;

	private String country;

	@Column(name="date_created")
	private Timestamp dateCreated;

	@Column(name="debts_car_loans")
	private String debtsCarLoans;

	@Column(name="debts_credit_card")
	private String debtsCreditCard;

	@Column(name="debts_other_expenses")
	private String debtsOtherExpenses;

	@Column(name="dental_care")
	private String dentalCare;

	@Column(name="depreciation_period")
	private String depreciationPeriod;

	@Column(name="down_payment_entered")
	private String downPaymentEntered;

	private String electricity;

	@Column(name="fee_condo")
	private String feeCondo;

	@Column(name="fee_heating_costs")
	private String feeHeatingCosts;

	@Column(name="fee_property_tax")
	private String feePropertyTax;

	private String gasoline;

	private String grocery;

	private String habitation;

	@Column(name="hobbies_social_clubs")
	private String hobbiesSocialClubs;

	@Column(name="income_applicant")
	private String incomeApplicant;

	@Column(name="income_co_applicant")
	private String incomeCoApplicant;

	@Column(name="income_others")
	private String incomeOthers;

	private String internet;

	private String landline;

	@Column(name="life_disability")
	private String lifeDisability;

	@Column(name="loans_lines_of_credit")
	private String loansLinesOfCredit;

	private String maintenance;

	@Column(name="maintenance_repairs")
	private String maintenanceRepairs;

	@Column(name="maximum_borrowing_capacity")
	private String maximumBorrowingCapacity;

	private String mobiles;

	private String mortgage;

	@Column(name="mortgage_loan_insurance")
	private String mortgageLoanInsurance;

	@Column(name="movies_concerts")
	private String moviesConcerts;

	@Column(name="newspapers_books")
	private String newspapersBooks;

	@Column(name="other_communications")
	private String otherCommunications;

	@Column(name="other_entertainment")
	private String otherEntertainment;

	@Column(name="other_finances")
	private String otherFinances;

	@Column(name="other_health")
	private String otherHealth;

	@Column(name="other_household")
	private String otherHousehold;

	@Column(name="other_insurance")
	private String otherInsurance;

	@Column(name="other_transport")
	private String otherTransport;

	private String parking;

	private String pharmaceuticals;

	private String province;

	@Column(name="public_transport")
	private String publicTransport;

	@Column(name="restaurant_meals")
	private String restaurantMeals;

	@Column(name="savings_investment")
	private String savingsInvestment;

	private String scenario;

	@Column(name="total_income")
	private String totalIncome;

	@Column(name="total_monthly_expenses")
	private String totalMonthlyExpenses;

	@Column(name="total_of_mortgage")
	private String totalOfMortgage;

	private String tuition;

	@Column(name="vision_care")
	private String visionCare;

	private String water;

	//bi-directional one-to-one association to Profile
	@OneToOne
	private Profile profile;

	public Budget() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAcompteFromDownPaymentEntered() {
		return this.acompteFromDownPaymentEntered;
	}

	public void setAcompteFromDownPaymentEntered(String acompteFromDownPaymentEntered) {
		this.acompteFromDownPaymentEntered = acompteFromDownPaymentEntered;
	}

	public String getAlimony() {
		return this.alimony;
	}

	public void setAlimony(String alimony) {
		this.alimony = alimony;
	}

	public String getAnimalCare() {
		return this.animalCare;
	}

	public void setAnimalCare(String animalCare) {
		this.animalCare = animalCare;
	}

	public String getAutomobile() {
		return this.automobile;
	}

	public void setAutomobile(String automobile) {
		this.automobile = automobile;
	}

	public String getCableSatellite() {
		return this.cableSatellite;
	}

	public void setCableSatellite(String cableSatellite) {
		this.cableSatellite = cableSatellite;
	}

	public String getCharitableDonations() {
		return this.charitableDonations;
	}

	public void setCharitableDonations(String charitableDonations) {
		this.charitableDonations = charitableDonations;
	}

	public String getChildcareCosts() {
		return this.childcareCosts;
	}

	public void setChildcareCosts(String childcareCosts) {
		this.childcareCosts = childcareCosts;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getClothes() {
		return this.clothes;
	}

	public void setClothes(String clothes) {
		this.clothes = clothes;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getDebtsCarLoans() {
		return this.debtsCarLoans;
	}

	public void setDebtsCarLoans(String debtsCarLoans) {
		this.debtsCarLoans = debtsCarLoans;
	}

	public String getDebtsCreditCard() {
		return this.debtsCreditCard;
	}

	public void setDebtsCreditCard(String debtsCreditCard) {
		this.debtsCreditCard = debtsCreditCard;
	}

	public String getDebtsOtherExpenses() {
		return this.debtsOtherExpenses;
	}

	public void setDebtsOtherExpenses(String debtsOtherExpenses) {
		this.debtsOtherExpenses = debtsOtherExpenses;
	}

	public String getDentalCare() {
		return this.dentalCare;
	}

	public void setDentalCare(String dentalCare) {
		this.dentalCare = dentalCare;
	}

	public String getDepreciationPeriod() {
		return this.depreciationPeriod;
	}

	public void setDepreciationPeriod(String depreciationPeriod) {
		this.depreciationPeriod = depreciationPeriod;
	}

	public String getDownPaymentEntered() {
		return this.downPaymentEntered;
	}

	public void setDownPaymentEntered(String downPaymentEntered) {
		this.downPaymentEntered = downPaymentEntered;
	}

	public String getElectricity() {
		return this.electricity;
	}

	public void setElectricity(String electricity) {
		this.electricity = electricity;
	}

	public String getFeeCondo() {
		return this.feeCondo;
	}

	public void setFeeCondo(String feeCondo) {
		this.feeCondo = feeCondo;
	}

	public String getFeeHeatingCosts() {
		return this.feeHeatingCosts;
	}

	public void setFeeHeatingCosts(String feeHeatingCosts) {
		this.feeHeatingCosts = feeHeatingCosts;
	}

	public String getFeePropertyTax() {
		return this.feePropertyTax;
	}

	public void setFeePropertyTax(String feePropertyTax) {
		this.feePropertyTax = feePropertyTax;
	}

	public String getGasoline() {
		return this.gasoline;
	}

	public void setGasoline(String gasoline) {
		this.gasoline = gasoline;
	}

	public String getGrocery() {
		return this.grocery;
	}

	public void setGrocery(String grocery) {
		this.grocery = grocery;
	}

	public String getHabitation() {
		return this.habitation;
	}

	public void setHabitation(String habitation) {
		this.habitation = habitation;
	}

	public String getHobbiesSocialClubs() {
		return this.hobbiesSocialClubs;
	}

	public void setHobbiesSocialClubs(String hobbiesSocialClubs) {
		this.hobbiesSocialClubs = hobbiesSocialClubs;
	}

	public String getIncomeApplicant() {
		return this.incomeApplicant;
	}

	public void setIncomeApplicant(String incomeApplicant) {
		this.incomeApplicant = incomeApplicant;
	}

	public String getIncomeCoApplicant() {
		return this.incomeCoApplicant;
	}

	public void setIncomeCoApplicant(String incomeCoApplicant) {
		this.incomeCoApplicant = incomeCoApplicant;
	}

	public String getIncomeOthers() {
		return this.incomeOthers;
	}

	public void setIncomeOthers(String incomeOthers) {
		this.incomeOthers = incomeOthers;
	}

	public String getInternet() {
		return this.internet;
	}

	public void setInternet(String internet) {
		this.internet = internet;
	}

	public String getLandline() {
		return this.landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getLifeDisability() {
		return this.lifeDisability;
	}

	public void setLifeDisability(String lifeDisability) {
		this.lifeDisability = lifeDisability;
	}

	public String getLoansLinesOfCredit() {
		return this.loansLinesOfCredit;
	}

	public void setLoansLinesOfCredit(String loansLinesOfCredit) {
		this.loansLinesOfCredit = loansLinesOfCredit;
	}

	public String getMaintenance() {
		return this.maintenance;
	}

	public void setMaintenance(String maintenance) {
		this.maintenance = maintenance;
	}

	public String getMaintenanceRepairs() {
		return this.maintenanceRepairs;
	}

	public void setMaintenanceRepairs(String maintenanceRepairs) {
		this.maintenanceRepairs = maintenanceRepairs;
	}

	public String getMaximumBorrowingCapacity() {
		return this.maximumBorrowingCapacity;
	}

	public void setMaximumBorrowingCapacity(String maximumBorrowingCapacity) {
		this.maximumBorrowingCapacity = maximumBorrowingCapacity;
	}

	public String getMobiles() {
		return this.mobiles;
	}

	public void setMobiles(String mobiles) {
		this.mobiles = mobiles;
	}

	public String getMortgage() {
		return this.mortgage;
	}

	public void setMortgage(String mortgage) {
		this.mortgage = mortgage;
	}

	public String getMortgageLoanInsurance() {
		return this.mortgageLoanInsurance;
	}

	public void setMortgageLoanInsurance(String mortgageLoanInsurance) {
		this.mortgageLoanInsurance = mortgageLoanInsurance;
	}

	public String getMoviesConcerts() {
		return this.moviesConcerts;
	}

	public void setMoviesConcerts(String moviesConcerts) {
		this.moviesConcerts = moviesConcerts;
	}

	public String getNewspapersBooks() {
		return this.newspapersBooks;
	}

	public void setNewspapersBooks(String newspapersBooks) {
		this.newspapersBooks = newspapersBooks;
	}

	public String getOtherCommunications() {
		return this.otherCommunications;
	}

	public void setOtherCommunications(String otherCommunications) {
		this.otherCommunications = otherCommunications;
	}

	public String getOtherEntertainment() {
		return this.otherEntertainment;
	}

	public void setOtherEntertainment(String otherEntertainment) {
		this.otherEntertainment = otherEntertainment;
	}

	public String getOtherFinances() {
		return this.otherFinances;
	}

	public void setOtherFinances(String otherFinances) {
		this.otherFinances = otherFinances;
	}

	public String getOtherHealth() {
		return this.otherHealth;
	}

	public void setOtherHealth(String otherHealth) {
		this.otherHealth = otherHealth;
	}

	public String getOtherHousehold() {
		return this.otherHousehold;
	}

	public void setOtherHousehold(String otherHousehold) {
		this.otherHousehold = otherHousehold;
	}

	public String getOtherInsurance() {
		return this.otherInsurance;
	}

	public void setOtherInsurance(String otherInsurance) {
		this.otherInsurance = otherInsurance;
	}

	public String getOtherTransport() {
		return this.otherTransport;
	}

	public void setOtherTransport(String otherTransport) {
		this.otherTransport = otherTransport;
	}

	public String getParking() {
		return this.parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public String getPharmaceuticals() {
		return this.pharmaceuticals;
	}

	public void setPharmaceuticals(String pharmaceuticals) {
		this.pharmaceuticals = pharmaceuticals;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPublicTransport() {
		return this.publicTransport;
	}

	public void setPublicTransport(String publicTransport) {
		this.publicTransport = publicTransport;
	}

	public String getRestaurantMeals() {
		return this.restaurantMeals;
	}

	public void setRestaurantMeals(String restaurantMeals) {
		this.restaurantMeals = restaurantMeals;
	}

	public String getSavingsInvestment() {
		return this.savingsInvestment;
	}

	public void setSavingsInvestment(String savingsInvestment) {
		this.savingsInvestment = savingsInvestment;
	}

	public String getScenario() {
		return this.scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getTotalIncome() {
		return this.totalIncome;
	}

	public void setTotalIncome(String totalIncome) {
		this.totalIncome = totalIncome;
	}

	public String getTotalMonthlyExpenses() {
		return this.totalMonthlyExpenses;
	}

	public void setTotalMonthlyExpenses(String totalMonthlyExpenses) {
		this.totalMonthlyExpenses = totalMonthlyExpenses;
	}

	public String getTotalOfMortgage() {
		return this.totalOfMortgage;
	}

	public void setTotalOfMortgage(String totalOfMortgage) {
		this.totalOfMortgage = totalOfMortgage;
	}

	public String getTuition() {
		return this.tuition;
	}

	public void setTuition(String tuition) {
		this.tuition = tuition;
	}

	public String getVisionCare() {
		return this.visionCare;
	}

	public void setVisionCare(String visionCare) {
		this.visionCare = visionCare;
	}

	public String getWater() {
		return this.water;
	}

	public void setWater(String water) {
		this.water = water;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}