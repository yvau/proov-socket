package com.dtr.oas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the binding database table.
 * 
 */
@Entity
@NamedQuery(name="Binding.findAll", query="SELECT b FROM Binding b")
public class Binding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="date_of_binding")
	private Timestamp dateOfBinding;

	@Column(name="status_interest")
	private String statusInterest;

	@Column(name="status_reading")
	private String statusReading;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to Property
	@ManyToOne
	private Property property;

	//bi-directional many-to-one association to Proposal
	@ManyToOne
	private Proposal proposal;

	public Binding() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfBinding() {
		return this.dateOfBinding;
	}

	public void setDateOfBinding(Timestamp dateOfBinding) {
		this.dateOfBinding = dateOfBinding;
	}

	public String getStatusInterest() {
		return this.statusInterest;
	}

	public void setStatusInterest(String statusInterest) {
		this.statusInterest = statusInterest;
	}

	public String getStatusReading() {
		return this.statusReading;
	}

	public void setStatusReading(String statusReading) {
		this.statusReading = statusReading;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Proposal getProposal() {
		return this.proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

}