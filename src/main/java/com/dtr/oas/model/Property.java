package com.dtr.oas.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;

import java.util.List;


/**
 * The persistent class for the property database table.
 * 
 */
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id")
@NamedQuery(name="Property.findAll", query="SELECT p FROM Property p")
public class Property implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="property_age")
	private String propertyAge;

	@Column(name="property_date_on_market")
	private String propertyDateOnMarket;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(name="property_description")
	private String propertyDescription;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(name="property_features")
	private String propertyFeatures;

	@Column(name="property_num_of_bathrooms")
	private Integer propertyNumOfBathrooms;

	@Column(name="property_num_of_bedrooms")
	private Integer propertyNumOfBedrooms;

	@Column(name="property_num_of_floors")
	private String propertyNumOfFloors;

	@Column(name="property_price")
	private double propertyPrice;

	@Column(name="property_sale_type")
	private String propertySaleType;

	@Column(name="property_save_date")
	private Timestamp propertySaveDate;

	@Column(name="property_sq_feet")
	private int propertySqFeet;

	@Column(name="property_type")
	private String propertyType;
	
	@Column(name="number_of_views")
	private int numberOfViews;

	@Column(name="property_bid_request")
	private  boolean propertyBidRequest;

  public boolean isPropertyBidRequest() {
    return propertyBidRequest;
  }

  public void setPropertyBidRequest(boolean propertyBidRequest) {
    this.propertyBidRequest = propertyBidRequest;
  }

  //bi-directional many-to-one association to Binding
	@OneToMany(mappedBy="property")
	private List<Binding> bindings;

	//bi-directional many-to-one association to Calendar
	@OneToMany(mappedBy="property")
	private List<Calendar> calendars;

	//bi-directional many-to-one association to Location
	@ManyToOne(cascade = CascadeType.ALL)
	private Location location;

	//bi-directional many-to-one association to Profile
  @JsonIgnore
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to PropertyPhoto
	@OneToMany(mappedBy="property")
	private List<PropertyPhoto> propertyPhotos;

	//bi-directional many-to-one association to ViewProperty
	@OneToMany(mappedBy="property")
	private List<ViewProperty> viewProperties;

	public Property() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPropertyAge() {
		return this.propertyAge;
	}

	public void setPropertyAge(String propertyAge) {
		this.propertyAge = propertyAge;
	}

	public String getPropertyDateOnMarket() {
		return this.propertyDateOnMarket;
	}

	public void setPropertyDateOnMarket(String propertyDateOnMarket) {
		this.propertyDateOnMarket = propertyDateOnMarket;
	}

	public String getPropertyDescription() {
		return this.propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyFeatures() {
		return this.propertyFeatures;
	}

	public void setPropertyFeatures(String propertyFeatures) {
		this.propertyFeatures = propertyFeatures;
	}

	public Integer getPropertyNumOfBathrooms() {
		return this.propertyNumOfBathrooms;
	}

	public void setPropertyNumOfBathrooms(Integer propertyNumOfBathrooms) {
		this.propertyNumOfBathrooms = propertyNumOfBathrooms;
	}

	public Integer getPropertyNumOfBedrooms() {
		return this.propertyNumOfBedrooms;
	}

	public void setPropertyNumOfBedrooms(Integer propertyNumOfBedrooms) {
		this.propertyNumOfBedrooms = propertyNumOfBedrooms;
	}

	public String getPropertyNumOfFloors() {
		return this.propertyNumOfFloors;
	}

	public void setPropertyNumOfFloors(String propertyNumOfFloors) {
		this.propertyNumOfFloors = propertyNumOfFloors;
	}

	public double getPropertyPrice() {
		return this.propertyPrice;
	}

	public void setPropertyPrice(double propertyPrice) {
		this.propertyPrice = propertyPrice;
	}

	public String getPropertySaleType() {
		return this.propertySaleType;
	}

	public void setPropertySaleType(String propertySaleType) {
		this.propertySaleType = propertySaleType;
	}

	public Timestamp getPropertySaveDate() {
		return this.propertySaveDate;
	}

	public void setPropertySaveDate(Timestamp propertySaveDate) {
		this.propertySaveDate = propertySaveDate;
	}

	public int getPropertySqFeet() {
		return this.propertySqFeet;
	}

	public void setPropertySqFeet(int propertySqFeet) {
		this.propertySqFeet = propertySqFeet;
	}

	public String getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public int getNumberOfViews() {
		return numberOfViews;
	}

	public void setNumberOfViews(int numberOfViews) {
		this.numberOfViews = numberOfViews;
	}

	public List<Binding> getBindings() {
		return this.bindings;
	}

	public void setBindings(List<Binding> bindings) {
		this.bindings = bindings;
	}

	public Binding addBinding(Binding binding) {
		getBindings().add(binding);
		binding.setProperty(this);

		return binding;
	}

	public Binding removeBinding(Binding binding) {
		getBindings().remove(binding);
		binding.setProperty(null);

		return binding;
	}

	public List<Calendar> getCalendars() {
		return this.calendars;
	}

	public void setCalendars(List<Calendar> calendars) {
		this.calendars = calendars;
	}

	public Calendar addCalendar(Calendar calendar) {
		getCalendars().add(calendar);
		calendar.setProperty(this);

		return calendar;
	}

	public Calendar removeCalendar(Calendar calendar) {
		getCalendars().remove(calendar);
		calendar.setProperty(null);

		return calendar;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<PropertyPhoto> getPropertyPhotos() {
		return this.propertyPhotos;
	}

	public void setPropertyPhotos(List<PropertyPhoto> propertyPhotos) {
		this.propertyPhotos = propertyPhotos;
	}

	public PropertyPhoto addPropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().add(propertyPhoto);
		propertyPhoto.setProperty(this);

		return propertyPhoto;
	}

	public PropertyPhoto removePropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().remove(propertyPhoto);
		propertyPhoto.setProperty(null);

		return propertyPhoto;
	}

	public List<ViewProperty> getViewProperties() {
		return this.viewProperties;
	}

	public void setViewProperties(List<ViewProperty> viewProperties) {
		this.viewProperties = viewProperties;
	}

	public ViewProperty addViewProperty(ViewProperty viewProperty) {
		getViewProperties().add(viewProperty);
		viewProperty.setProperty(this);

		return viewProperty;
	}

	public ViewProperty removeViewProperty(ViewProperty viewProperty) {
		getViewProperties().remove(viewProperty);
		viewProperty.setProperty(null);

		return viewProperty;
	}

}