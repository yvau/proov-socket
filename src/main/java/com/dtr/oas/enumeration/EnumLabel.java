package com.dtr.oas.enumeration;

public class EnumLabel{
	public enum constructionOfProperty { 
		existing,isNew;
	}
	
	public enum typeOfAccommodation {
		detachedhouse, semidetachedhouse, townhouse, duplex, smallbuilding, residentialtower;		
	}
	
	public enum propertyType {
		coproperty, freeholdhome;
	}
	
	public enum water {
		municipal, wells;
	}
	
	public enum ground {
		concreteblocks, treatedwood, concrete;
	}
	
	public enum typeOfExteriorSiding{
		vinylsiding, aluminumsiding, brickandsiding, bricks, wood;
	}
	
	public enum windowsSiding{
		isSimple, isTriple, isDouble, isLowemissivity;
	}
	
	public enum sewer{
		municipal, septictank;
	}
	
	public enum windowsConstruction{
		wood, vinyl, aluminium, other;
	}
	
	public enum garage {
		attached, detached;
	}
	
	public enum furnished {
		Furnished_fridge, Furnished_oven;
	}
	
	public enum availabilityDay {
		sunday, monday, tuesday, wednesday, thursday, friday, saturday;
	}
	
	public enum availabilityTime {
		nineAm, tenAm, elevenAm, twelvePm, onePm, twoPm, threePm, fourPm, fivePm, sixPm, sevenPm;
	}
	
	/*public enum propertyTypeRent {
		PropertyTypeRent_oneandhalf, PropertyTypeRent_twoandhalf, PropertyTypeRent_threeandhalf, PropertyTypeRent_fourandhalf, PropertyTypeRent_fiveandhalf, PropertyTypeRent_sixandhalf, PropertyTypeRent_sevenandhalf, PropertyTypeRent_eightandhalf, PropertyTypeRent_nineandhalf;
	} */
	
	public enum featuresRent {
		animalsallowed, washer, garden, lift, balcony, smokingpermitted, swimmingpool;
	}

	public enum propertyTypeOfProperty {
		condoApartment, townhouse, cottageBungalow, house, maisonetteDuplex, residentialFarm, fullBuilding, garage, horizontalProperty,
		landLotPlot, loft, manorPalaceCastle, mobileHome, penthouse, villa, rental, newBuildings;	
	}
	
	public enum propertySaleType {
		rent, sale;
	}
	
	public enum yesNoLabel {
		yes, no;
	}
	
	public enum role {
		buyer, seller;
	}
	
	/*public enum statusNotification {
		unread, read;
	}*/
	
	public enum idJsLabel {
		Property, Rent, Buy, Visit;
	}
	
	public enum proposalType {
		LabelBuyingProposal("proposal.buying"),
	    LabelRentingProposal("proposal.renting")
	    ;

	    private final String text;

	    /**
	     * @param text
	     */
	    private proposalType(final String text) {
	        this.text = text;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	
	public enum statusReading {
		LabelNotRead("label.read.not"),
	    LabelRead("label.read.yes")
	    ;

	    private final String text;

	    /**
	     * @param text
	     */
	    private statusReading(final String text) {
	        this.text = text;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	
	public enum emailType {
		AppProfileActivation("app.profile.activation"),
	    AppProfileRecover("app.profile.recover")
	    ;

	    private final String text;

	    /**
	     * @param text
	     */
	    private emailType(final String text) {
	        this.text = text;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	
	//a effacer ou mod1f because of invoke qui appelle les methods
	public enum modalId {
		TargetModalRent("con-rent-modal"),
		TargetModalBuy("con-buy-modal"),
	    TargetModalProperty("con-property-modal")
	    ;

	    private final String text;

	    /**
	     * @param text
	     */
	    private modalId(final String text) {
	        this.text = text;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	
	public enum labelNumber {
		LabelPageMax(9),
		MinimumPasswordLength(6);

	    private final int number;

	    /**
	     * @param text
	     */
	    private labelNumber(final int number) {
	        this.number = number;
	    }
	    
	    public int getNumber() {
	        return number;
	    }
	}

}