package com.dtr.oas.enumeration;

public class ResponseAction {
	public enum responseAction { 
	 HIDE, REDIRECT, SUCCESS, FAIL, INVOKE;
	}
	
	public enum functionToInvoke {
		callBackCreateCriteria("createCriteria"),
		callBackBindings("bindings"),
		//callBackVoid("returnVoid"),
		callBackRemoveCriteria("removeCriteria"),
		callBackSuccessFunctionFromModal("successFunctionFromModal"),
		callBackSuccessFunctionRequestScheduled("successFunctionRequestScheduled");


	    private final String text;

	    /**
	     * @param text
	     */
	    private functionToInvoke(final String text) {
	        this.text = text;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
	}
}
