package com.dtr.oas.json;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;


public class CustomDateSerializer extends JsonSerializer<Timestamp> {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm "); 

    @Override
    public void serialize(Timestamp value, JsonGenerator gen, 
                          SerializerProvider arg2)
        throws IOException, JsonProcessingException {
    	
    	String formattedDate = dateFormat.format(value).trim();

        gen.writeString(formattedDate);
    }
}