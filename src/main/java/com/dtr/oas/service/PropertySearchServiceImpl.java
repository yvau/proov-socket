package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Profile;
import com.dtr.oas.model.PropertySearch;
import com.dtr.oas.repositories.PropertySearchRepository;

@Service
public class PropertySearchServiceImpl implements PropertySearchService {
	
	@Autowired
	PropertySearchRepository propertySearchRepository;
	
	public void save(PropertySearch propertySearch){
		propertySearchRepository.saveAndFlush(propertySearch);
		return ;
	}
	
	public PropertySearch findByProfile(Profile profile) {
		return propertySearchRepository.findByProfile(profile);
	}
	
	public void delete(PropertySearch propertySearch){
			if(propertySearch.getId() != null){
				propertySearchRepository.delete(propertySearch);
			}
		  return ;
	}
	
}
