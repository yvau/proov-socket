package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.model.Country;

public interface CountryService {
	
	public Country find(String id);
	
	public List<Country> findAll();

}
