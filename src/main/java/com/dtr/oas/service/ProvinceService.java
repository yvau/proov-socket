package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.model.Country;
import com.dtr.oas.model.Province;


public interface ProvinceService {
	
	public Province find(String id);
	
	public List<Province> findByCountry(Country country);
	
	public List<Province> findByCountry(String id);
}
