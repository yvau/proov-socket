package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Binding;
import com.dtr.oas.model.Proposal;
import com.dtr.oas.repositories.BindingRepository;

@Service
public class BindingServiceImpl implements BindingService {
	
	@Autowired
	private BindingRepository bindingRepository;
	
	/**
	 * @return
	 */
	public void save(Binding binding) {
    	bindingRepository.saveAndFlush(binding);
    	return;
	}
	
	public List<Binding> findByProposal(Profile profile, Proposal proposal, String statusInterest){
		return bindingRepository.findByProfileAndProposalAndStatusInterest(profile, proposal, statusInterest);
	}
	
	public Binding find(Long id){
		return bindingRepository.findOne(id);
		//return bindingRepository.findByProposal(proposal, pageable);
	}
}
