package com.dtr.oas.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dtr.oas.form.PropertyForm;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;

public interface PropertyService {
	
	public Property find(Long id);
	
	public Page<Property> findAll(PropertyForm searchProperty, Pageable pageable);
	
	public Page<Property> findByProfile(Profile profile, Pageable pageable);
	
	public Property findByProfileAndId(Profile profile, Long id);
	
	public void save(Property property);
	
	public void delete(Profile profile, Property property);
}
