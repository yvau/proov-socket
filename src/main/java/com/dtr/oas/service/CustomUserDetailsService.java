package com.dtr.oas.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dtr.oas.model.Profile;

/**
 * A custom {@link UserDetailsService} where user information
 * is retrieved from a JPA repository
 */
@Service("userDetailsService")
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private ProfileService profileRepository;
	
	

	/**
	 * Returns a populated {@link UserDetails} object. 
	 * The username is first retrieved from the database and then mapped to 
	 * a {@link UserDetails} object.
	 */
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		try {
			Profile profile = profileRepository.findByEmail(email);

			if(profile==null) {throw new UsernameNotFoundException("No such user: " + email);} 
				else if (profile.getRole().isEmpty()) {
					throw new UsernameNotFoundException("User with email" + email + " has no authorities");
            }
			
			
			return new User(
					profile.getEmail(),
					profile.getPassword(),
					profile.getEnabled(),
					profile.getAccountNonExpired(),
					profile.getCredentialsNonExpired(),
					profile.getAccountNonLocked(),
					getAuthorities(profile.getRole()));
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static List<String> getRolesAsList(String role) {
		List<String> rolesAsList = new ArrayList<String>();
		String[] arrayRole = role.split(",");
 
    	for(String roles : arrayRole){
        	rolesAsList.add(roles);
    	}
    	return rolesAsList;
	}

	/**
	 * @param roles
	 * @return
	 */
	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    	for (String role : roles) {
        	authorities.add(new SimpleGrantedAuthority(role));
    	}
    	return authorities;
	}

	/**
	 * @param role
	 * @return
	 */
	public static Collection<? extends GrantedAuthority> getAuthorities(String role) {
    List<GrantedAuthority> authList = getGrantedAuthorities(getRolesAsList(role));
    	return authList;
	}
}
