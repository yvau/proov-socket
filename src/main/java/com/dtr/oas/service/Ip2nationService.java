package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.model.Ip2nation;


public interface Ip2nationService {
	
	public List<Ip2nation> findCountry(String id);
}
