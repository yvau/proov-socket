package com.dtr.oas.service;

public interface PaypalService {
	
	public Boolean getPackage(String thePackage);
	
	public String sendPaypalPayment() throws Exception;
}
