package com.dtr.oas.service;

import com.dtr.oas.model.Budget;
import com.dtr.oas.model.Profile;


public interface BudgetService {
	public void save(Budget budget);
	
	public Budget findByProfile(Profile profile);
	
}
