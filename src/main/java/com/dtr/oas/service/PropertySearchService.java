package com.dtr.oas.service;

import com.dtr.oas.model.Profile;
import com.dtr.oas.model.PropertySearch;

public interface PropertySearchService {
	
	public void save(PropertySearch propertySearch);
	
	public PropertySearch findByProfile(Profile profile);
	
	public void delete(PropertySearch propertySearch);

}
