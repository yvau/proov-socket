package com.dtr.oas.service;

import com.dtr.oas.model.ProposalSearch;
import com.dtr.oas.model.Profile;

public interface ProposalSearchService {
	
	public void save(ProposalSearch owningProposalSearch);
	
	public ProposalSearch findByProfileAndProposalType(Profile profile, String proposalType);
	
	public void delete(ProposalSearch proposalSearch);

}
