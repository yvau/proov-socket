package com.dtr.oas.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Locale;


@Service
public class EmailServiceImpl implements EmailService {
  @Autowired
  private JavaMailSender mailSender;
  
  @Autowired
  private SpringTemplateEngine templateEngine;  

  
  @Async
  public void sendEmail(final String toEmailAddresses, final String fromEmailAddress,
          final String subject, final Object object, final String template, final Locale locale) {

	  sendEmail(toEmailAddresses, fromEmailAddress, subject, null, null, object, template, locale);
  }


  private void sendEmail(final String toEmailAddresses, final String fromEmailAddress,
           final String subject, final String attachmentPath,
           final String attachmentName, final Object object, final String template, final Locale locale) {
	  
	  MimeMessagePreparator preparator = new MimeMessagePreparator() {

		  public void prepare(MimeMessage mimeMessage) throws Exception {

			  MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
			  message.setTo(toEmailAddresses);
			  message.setFrom(new InternetAddress(fromEmailAddress));
			  message.setSubject(subject);
			  
			  final Context ctx = new Context(locale);
			  ctx.setVariable("object", object);
			  String body = templateEngine.process(template, ctx);
			  message.setText(body, true);

		  }
	  };
	  this.mailSender.send(preparator);
  }
  
  
  
  
  
  
  
  
  

}