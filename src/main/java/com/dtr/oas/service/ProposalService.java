package com.dtr.oas.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.dtr.oas.form.ProposalForm;
import com.dtr.oas.model.Proposal;
import com.dtr.oas.model.Profile;



public interface ProposalService {
	
	public void delete(Proposal proposal, Profile profile);
	
	public void save(Proposal proposal);
	
	public Proposal findByProfileAndId(Profile profile, Long id);
	
	public Proposal find(Long id);
	
	public Page<Proposal> findAll(ProposalForm searchProposal,Pageable pageable);
	
	public Page<Proposal> findByProposalType(String proposalType, Pageable pageable);
	
	public Page<Proposal> findByProposalTypeAndProfile(String proposalType, Profile profile, Pageable pageable);
}
