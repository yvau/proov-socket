package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileSetting;
import com.dtr.oas.repositories.ProfileSettingRepository;

@Service
public class ProfileSettingServiceImpl implements ProfileSettingService {
  
  @Autowired
  private ProfileSettingRepository profileSettingRepository;
  
  public ProfileSetting findByProfile(Profile profile){
  
  return profileSettingRepository.findByProfile(profile);
  }
  
  public void save(ProfileSetting profileSetting){
	profileSettingRepository.saveAndFlush(profileSetting);
  }

}