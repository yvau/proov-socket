package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.dtr.oas.model.MembershipPayment;
import com.dtr.oas.repositories.MembershipPaymentRepository;



@Service
public class MembershipPaymentServiceImpl implements MembershipPaymentService {
	
	@Autowired
	MembershipPaymentRepository membershipPaymentRepository;
	
	public Page<MembershipPayment> findAll(Pageable pageable){
		return membershipPaymentRepository.findAll(pageable);
	}
	
}
