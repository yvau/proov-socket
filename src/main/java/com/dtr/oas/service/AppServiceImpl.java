package com.dtr.oas.service;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.model.Profile;

@Service
public class AppServiceImpl implements AppService {

  @Autowired
  private ProfileService profileRepository;

  /**
   * @param maxlength
   * @return
   */
  public String getRandomString(int maxlength) {
    String result = "";
    int i = 0, n = 0, min = 33, max = 122;
    while (i < maxlength) {
      n = (int) (Math.random() * (max - min) + min);
      if (n >= 33 && n < 123) {
        result += (char) n;
        ++i;
      }
    }
    return result;
  }


  public void authorizeUser(Profile profile) {

    UserDetails userDetails = loadUserByUsername(profile.getEmail());

    Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());

    SecurityContextHolder.getContext().setAuthentication(authentication);
  }

  public boolean isAjaxRequest(HttpServletRequest request) {

    String ajaxNameHeader = "xmlhttprequest";
    Boolean ajaxHeaderTrue = request.getHeader("x-requested-with") != null && request.
        getHeader("x-requested-with").toLowerCase().equals(ajaxNameHeader);
    return ajaxHeaderTrue;
  }

  public String removeQueryStringParameter(String url) {
    List<String> paramAsList = new ArrayList<String>();
    String newQueryString = null;

    if (url != null) {
      int i = 0;
      String[] arrayUrl = url.split("&");
      for (String params : arrayUrl) {
        String pointer = arrayUrl[i].split("=")[0];
        if (!pointer.equals("page") && !pointer.equals("rel")) {
          paramAsList.add(params);
        }
        i++;

      }

    }

    String addConcat = (paramAsList.size() > 0 && url != null) ? "?" : "";
    newQueryString = addConcat + StringUtils.collectionToDelimitedString(paramAsList, "&");

    return newQueryString;

  }

  public Pageable pageable(Integer page, String sorting) {
    page = page == null ? 0 : (page.intValue() - 1);
    Sort sort = orderBy(sorting);
    return new PageRequest(page, EnumLabel.labelNumber.LabelPageMax.getNumber(), sort);
  }

  private Sort orderBy(String sorting) {
    sorting = sorting == null ? "id-desc" : sorting;
    String[] sortingArray = sorting.split("-");
    Direction direction = sortingArray[1].toUpperCase().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC;
    return new Sort(direction, sortingArray[0]);
  }

  private UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    try {
      Profile profile = profileRepository.findByEmail(email);

      if (profile == null) {
        throw new UsernameNotFoundException("No such user: " + email);
      } else if (profile.getRole().isEmpty()) {
        throw new UsernameNotFoundException("User with email" + email + " has no authorities");
      }


      return new User(
          profile.getEmail(),
          profile.getPassword(),
          profile.getEnabled(),
          profile.getAccountNonExpired(),
          profile.getCredentialsNonExpired(),
          profile.getAccountNonLocked(),
          CustomUserDetailsService.getAuthorities(profile.getRole()));

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
