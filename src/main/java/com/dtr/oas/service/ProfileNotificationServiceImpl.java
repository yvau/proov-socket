package com.dtr.oas.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dtr.oas.form.NotificationMessage;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileNotification;
import com.dtr.oas.repositories.ProfileNotificationRepository;


@Service
public class ProfileNotificationServiceImpl implements ProfileNotificationService {


  @Autowired
  ProfileNotificationRepository profileNotificationRepository;


  public void save(ProfileNotification profileNotification) {
    profileNotificationRepository.saveAndFlush(profileNotification);
  }

  public String findByProfile(Profile profile) {
    StringJoiner notificationMessage = new StringJoiner(",");
    List<ProfileNotification> profileNotification = profileNotificationRepository.findByProfile1(profile);
    for (ProfileNotification theProfileNotification : profileNotification) {
      notificationMessage.add("{\"notifier\":\" " + theProfileNotification.getProfile2().getEmail() + "\", " +
          "\"verb\":\"" + theProfileNotification.getNotificationVerb() + "\"," +
          "\"object\":\"" + theProfileNotification.getNotificationObject() + "\"," +
          "\"status\":\"" + theProfileNotification.getNotificationStatus() + "\"," +
          "\"date\":\"" + theProfileNotification.getNotificationDate() + "\"," +
          "\"target\":\"" + theProfileNotification.getNotificationTarget() + "\"}");
    }
    return "[" + notificationMessage.toString() + "]";
  }

}