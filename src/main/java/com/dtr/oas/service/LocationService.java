package com.dtr.oas.service;

import com.dtr.oas.model.Location;

public interface LocationService {
	
	public Location save(Location location);
	
	public Location find(String id);
}
