package com.dtr.oas.service;

import static com.dtr.oas.repositories.PropertySpecifications.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dtr.oas.form.PropertyForm;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;
import com.dtr.oas.repositories.PropertyRepository;

@Service
public class PropertyServiceImpl implements PropertyService {
	
  @Autowired
  PropertyRepository propertyRepository;
  
  public void save(Property property){
	  propertyRepository.saveAndFlush(property);
	  return ;
  }
  
  public void delete(Profile profile, Property property){
	  Property propert = propertyRepository.findByProfileAndId(profile, property.getId());
		if(propert != null){
			propertyRepository.delete(property);
		}
	  return ;
  }
  
  public Property find(Long id){
	  return propertyRepository.findOne(id);
  }
  
  public Page<Property> findAll(PropertyForm searchProperty,Pageable pageable){
	  return propertyRepository.findAll(specification(searchProperty), pageable);
  }
  
  public Page<Property> findByProfile(Profile profile, Pageable pageable){
	  return propertyRepository.findByProfile(profile, pageable);
  }
  
  public Property findByProfileAndId(Profile profile, Long id){
	  return propertyRepository.findByProfileAndId(profile, id);
  }
}











