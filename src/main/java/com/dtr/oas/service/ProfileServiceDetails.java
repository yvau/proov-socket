package com.dtr.oas.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Profile;
import com.dtr.oas.repositories.ProfileRepository;

import org.springframework.beans.factory.annotation.Autowired;


@Service
public class ProfileServiceDetails {
	
  @Autowired
  private ProfileRepository profileRepository;
	
  
  public Profile getProfileDetails() {
        User profile = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = profile.getUsername();
        return profileRepository.findByEmail(email);
  }
}











