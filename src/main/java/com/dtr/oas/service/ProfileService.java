package com.dtr.oas.service;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dtr.oas.model.Profile;



public interface ProfileService {
    
    public Profile findByEmail(String Email);
    
    public Profile findByToken(String t);

    public void save(Profile profile,String emailTemplateName, Locale locale);

}
