package com.dtr.oas.service;

import com.dtr.oas.model.ViewProperty;

public interface ViewPropertyService {
	
	public void save(ViewProperty viewProperty);
	
	public ViewProperty findByIp(String ip);
}
