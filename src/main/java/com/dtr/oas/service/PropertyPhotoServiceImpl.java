package com.dtr.oas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Property;
import com.dtr.oas.model.PropertyPhoto;
import com.dtr.oas.repositories.PropertyPhotoRepository;


@Service
public class PropertyPhotoServiceImpl implements PropertyPhotoService {

  @Autowired
  PropertyPhotoRepository propertyPhotoRepository;

  public PropertyPhoto save(PropertyPhoto propertyPhoto) {
    return propertyPhotoRepository.saveAndFlush(propertyPhoto);
  }

  public void delete(PropertyPhoto propertyPhoto) {
    propertyPhotoRepository.delete(propertyPhoto);
    return;
  }

  public List<PropertyPhoto> findByProperty(Property property) {
    return propertyPhotoRepository.findByProperty(property);
  }

  public PropertyPhoto find(Long id) {
    return propertyPhotoRepository.findOne(id);
  }
}
