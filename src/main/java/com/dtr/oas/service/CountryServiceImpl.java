package com.dtr.oas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Country;
import com.dtr.oas.repositories.CountryRepository;


@Service
public class CountryServiceImpl implements CountryService {
	
	@Autowired
	CountryRepository countryRepository;
	
	public Country find(String id) {
		return countryRepository.findOne(id);		
	}
	
	public List<Country> findAll() {
		return countryRepository.findAll();		
	}

}