package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.ViewProperty;
import com.dtr.oas.repositories.ViewPropertyRepository;

@Service
public class ViewPropertyServiceImpl implements ViewPropertyService {
	
	@Autowired
	ViewPropertyRepository viewPropertyRepository;
	
	public void save(ViewProperty viewProperty) {
		viewPropertyRepository.saveAndFlush(viewProperty);
	}
	
	public ViewProperty findByIp(String ip) {
		return viewPropertyRepository.findByIpNameView(ip);
	}
}