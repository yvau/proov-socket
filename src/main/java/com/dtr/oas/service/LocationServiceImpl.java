package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Location;
import com.dtr.oas.repositories.LocationRepository;



@Service
public class LocationServiceImpl implements LocationService {
	
	@Autowired
	LocationRepository locationRepository;
	
	public Location save(Location location) {
		return locationRepository.saveAndFlush(location);
	}
	
	public Location find(String id){
		return locationRepository.findOne(id);
	}
}
