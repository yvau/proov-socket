package com.dtr.oas.service;

import com.dtr.oas.model.LocationHasProfile;
import com.dtr.oas.model.Profile;

public interface LocationHasProfileService {
	
	public void save(LocationHasProfile locationHasProfile);
	
	public LocationHasProfile findByProfile(Profile profile);
}
