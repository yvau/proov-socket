package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.model.Property;
import com.dtr.oas.model.PropertyPhoto;

public interface PropertyPhotoService {

  public PropertyPhoto save(PropertyPhoto propertyPhoto);

  public void delete(PropertyPhoto propertyPhoto);

  public List<PropertyPhoto> findByProperty(Property property);

  public PropertyPhoto find(Long id);

}
