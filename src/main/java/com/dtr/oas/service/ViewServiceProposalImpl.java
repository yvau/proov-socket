package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.ViewProposal;
import com.dtr.oas.repositories.ViewProposalRepository;

@Service
public class ViewServiceProposalImpl implements ViewProposalService {
	
	@Autowired
	private ViewProposalRepository viewProposalRepository;
	
	public ViewProposal findByIp(String ip){
		return viewProposalRepository.findByIpNameView(ip);
	}
	
	public ViewProposal save(ViewProposal viewProposal){
		return viewProposalRepository.saveAndFlush(viewProposal);
	}
}
