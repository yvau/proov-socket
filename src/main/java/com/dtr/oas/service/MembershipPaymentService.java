package com.dtr.oas.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.dtr.oas.model.MembershipPayment;

public interface MembershipPaymentService {
	
	public Page<MembershipPayment> findAll(Pageable pageable);
}
