package com.dtr.oas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.City;
import com.dtr.oas.model.Province;
import com.dtr.oas.repositories.CityRepository;
import com.dtr.oas.repositories.ProvinceRepository;


@Service
public class CityServiceImpl implements CityService {
  
  @Autowired
  CityRepository cityRepository;
  
  @Autowired
  ProvinceRepository provinceRepository;
  
  public City find(String id){
		return cityRepository.findOne(id);
  }
  
  public List<City> findByProvince(Province province){
		return cityRepository.findByProvince(province);
  }
  
  public List<City> findByProvince(String id){
	  try {
		  Province province = provinceRepository.findOne(id);
			return cityRepository.findByProvince(province);
	} catch (Exception e) {
		return null;// TODO: handle exception
	}
	
	
}

}