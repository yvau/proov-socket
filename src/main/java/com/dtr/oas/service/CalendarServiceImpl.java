package com.dtr.oas.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Calendar;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;
import com.dtr.oas.repositories.CalendarRepository;

@Service
public class CalendarServiceImpl implements CalendarService {
	
	@Autowired
	CalendarRepository calendarRepository;
	
	public void save(Calendar calendar){
		calendarRepository.saveAndFlush(calendar);
	}
	
	public Calendar find(Profile profile, Long id){
		return calendarRepository.findByProfile1AndId(profile, id);
	}
	
	public List<Calendar> findByProfileAndStatus(Profile profile, String status){
		return calendarRepository.findByProfile1AndStatus(profile, status);
	}

	public List<Calendar> findByProfileVisitorAndStatus(Profile profile, String status){
		return calendarRepository.findByProfile2AndStatus(profile, status);
	}

	public List<Calendar> findByProfileAndPropertyAndStatus(Profile profile, Property property, String status){
		return calendarRepository.findByProfile1AndPropertyAndStatus(profile, property, status);
	}
	
	public Calendar findByProfileAndStatusAndStartDate(Profile profile, String status, Timestamp timeStampNow){
		return calendarRepository.findByProfileAndStatusAndStartDate(profile, status, timeStampNow);
	}

}
