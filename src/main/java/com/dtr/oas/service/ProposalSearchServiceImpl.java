package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.ProposalSearch;
import com.dtr.oas.model.Profile;
import com.dtr.oas.repositories.ProposalSearchRepository;


@Service
public class ProposalSearchServiceImpl implements ProposalSearchService {
	
	@Autowired
	ProposalSearchRepository proposalSearchRepository;
	
	public void save(ProposalSearch proposalSearch){
		proposalSearchRepository.saveAndFlush(proposalSearch);
		return ;
	}
	
	public ProposalSearch findByProfileAndProposalType(Profile profile, String proposalType){
		return proposalSearchRepository.findByProfileAndProposalType(profile, proposalType);
	}
	
	public void delete(ProposalSearch proposalSearch){
		if(proposalSearch.getId() != null){
			proposalSearchRepository.delete(proposalSearch);
		}
	  return ;
}
}