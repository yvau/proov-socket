package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Budget;
import com.dtr.oas.model.Profile;
import com.dtr.oas.repositories.BudgetRepository;

@Service
public class BudgetServiceImpl implements BudgetService {
	
	@Autowired
	private BudgetRepository budgetRepository;
	
	public void save(Budget budget){
		budgetRepository.saveAndFlush(budget);
	}
	
	public Budget findByProfile(Profile profile){
		return budgetRepository.findByProfile(profile);
	}

}