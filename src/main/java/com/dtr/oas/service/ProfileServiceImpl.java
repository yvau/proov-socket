package com.dtr.oas.service;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.dtr.oas.model.Profile;
import com.dtr.oas.repositories.ProfileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;


@Service
public class ProfileServiceImpl implements ProfileService {
  
  @Autowired
  private ProfileRepository profileRepository;
  
  @Autowired
  private EmailService emailService;
  
  @Autowired
  private MessageSource messageSource;

 
  
  public void save(Profile profile, String emailTemplateName, Locale locale) {
       profileRepository.saveAndFlush(profile); 
        if (emailTemplateName != null){
        	try{
        		String emailRecipient = messageSource.getMessage("label." + emailTemplateName, null ,locale);
        		emailService.sendEmail(profile.getEmail(), "mobiot.fabrice@gmail.com", emailRecipient, profile, emailTemplateName, locale);
        	}
        	catch(Exception e){
        		System.out.print(e);
        	}
        }
        return;
  }
 
  
  public Profile findByEmail(String Email) {	  
	  return profileRepository.findByEmail(Email);
  }
  
  public Profile findByToken(String t) {
	  return profileRepository.findByToken(t);
  }
}











