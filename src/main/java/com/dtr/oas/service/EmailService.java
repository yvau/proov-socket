package com.dtr.oas.service;

import java.util.Locale;

public interface EmailService {
	
	public void sendEmail(final String toEmailAddresses, final String fromEmailAddress,
            final String subject, final Object object, final String template, final Locale locale);

}