package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.form.NotificationMessage;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileNotification;

public interface ProfileNotificationService  {
		
	public void save(ProfileNotification profileNotification);

	public String findByProfile(Profile profile);
	
	//public ProfileNotification findByOneProfile(String id);
}
