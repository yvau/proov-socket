package com.dtr.oas.service;

import com.dtr.oas.model.ProfilePhoto;

public interface ProfilePhotoService {
    
    public ProfilePhoto save(ProfilePhoto profilePhoto);
}
