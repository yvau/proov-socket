package com.dtr.oas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Availability;
import com.dtr.oas.model.Profile;
import com.dtr.oas.repositories.AvailabilityRepository;

@Service
public class AvailabilityServiceImpl implements AvailabilityService {
	
	@Autowired
	AvailabilityRepository availabilityRepository;
	
	public void save(Availability availability) {
		availabilityRepository.saveAndFlush(availability);
		return ;
	}
	
	public List<Availability> findByProfile(Profile profile){
		return availabilityRepository.findByProfile(profile);
	}
}
