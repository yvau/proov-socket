package com.dtr.oas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.LocationHasProfile;
import com.dtr.oas.model.Profile;
import com.dtr.oas.repositories.LocationHasProfileRepository;

@Service
public class LocationHasProfileServiceImpl implements LocationHasProfileService {
	
	@Autowired
	LocationHasProfileRepository locationHasProfileRepository;
	
	public void save(LocationHasProfile locationHasProfile) {
		locationHasProfileRepository.saveAndFlush(locationHasProfile);
		return ;
	}
	
	public LocationHasProfile findByProfile(Profile profile) {
		return locationHasProfileRepository.findByProfile(profile);
	}

}
