package com.dtr.oas.service;

import java.util.List;
import com.dtr.oas.model.Binding;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;
import com.dtr.oas.model.Proposal;

public interface BindingService {
	
	public void save(Binding binding);
	
	public Binding find(Long id);
	
	public List<Binding> findByProposal(Profile profile, Proposal proposal, String statusInterest);

}
