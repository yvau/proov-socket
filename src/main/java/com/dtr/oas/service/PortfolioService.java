package com.dtr.oas.service;

import com.dtr.oas.model.Portfolio;

public interface PortfolioService {

	Portfolio findPortfolio(String username);

}
