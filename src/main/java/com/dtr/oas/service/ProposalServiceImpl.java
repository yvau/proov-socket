package com.dtr.oas.service;

import com.dtr.oas.repositories.ProposalRepository;
import com.dtr.oas.form.ProposalForm;
import com.dtr.oas.model.Proposal;
import com.dtr.oas.model.Profile;
import static com.dtr.oas.repositories.ProposalSpecifications.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class ProposalServiceImpl implements ProposalService {
	
	@Autowired
	ProposalRepository proposalRepository;
	
	public void delete(Proposal proposal, Profile profile) {
		Proposal ownProposal = proposalRepository.findByProfileAndId(profile, proposal.getId());
			if(ownProposal != null){
				proposalRepository.delete(proposal);
			}		
		return;
	}
	
	public void save(Proposal proposal) {
		proposalRepository.saveAndFlush(proposal);
		return;
	}
	
	public Proposal findByProfileAndId(Profile profile, Long id) {
		return proposalRepository.findByProfileAndId(profile, id);
	}
	
	public Proposal find(Long id) {
		try{
			return proposalRepository.findOne(id);
		}catch (Exception e){
			return null;
		}

	}
	
	public Page<Proposal> findAll(ProposalForm searchProposal, Pageable pageable) {
		return proposalRepository.findAll(specification(searchProposal),pageable);
	}
	
	public Page<Proposal> findByProposalType(String proposalType, Pageable pageable) {
		return proposalRepository.findByProposalType(proposalType, pageable);
	}
	
	public Page<Proposal> findByProposalTypeAndProfile(String proposalType, Profile profile, Pageable pageable){
		return proposalRepository.findByProposalTypeAndProfile(proposalType, profile, pageable);
	}
}