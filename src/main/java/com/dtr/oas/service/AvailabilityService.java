package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.model.Availability;
import com.dtr.oas.model.Profile;

public interface AvailabilityService {
	
	public void save(Availability availability);
	
	public List<Availability> findByProfile(Profile profile);
}
