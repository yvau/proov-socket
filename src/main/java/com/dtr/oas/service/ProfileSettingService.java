package com.dtr.oas.service;

import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileSetting;

public interface ProfileSettingService {
	
	public ProfileSetting findByProfile(Profile profile);
	
	public void save(ProfileSetting profileSetting);
}
