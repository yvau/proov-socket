package com.dtr.oas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Country;
import com.dtr.oas.model.Province;
import com.dtr.oas.repositories.CountryRepository;
import com.dtr.oas.repositories.ProvinceRepository;


@Service
public class ProvinceServiceImpl implements ProvinceService {
  
  @Autowired
  ProvinceRepository provinceRepository;
  
  @Autowired
  CountryRepository countryRepository;
  
  public Province find(String id){
	return provinceRepository.findOne(id);
  }
  
  public List<Province> findByCountry(Country country){
	return provinceRepository.findByCountry(country);
  }
  
  public List<Province> findByCountry(String id){
	try {
		Country country = countryRepository.findOne(id);
		return provinceRepository.findByCountry(country);
	} catch (Exception e) {
		return null;// TODO: handle exception
	}
	
}

}