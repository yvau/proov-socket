package com.dtr.oas.service;

import java.util.List;

import com.dtr.oas.model.City;
import com.dtr.oas.model.Province;


public interface CityService {
	
	public City find(String id);
	
	public List<City> findByProvince(Province province);
	
	public List<City> findByProvince(String id);
}
