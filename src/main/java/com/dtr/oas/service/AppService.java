package com.dtr.oas.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Pageable;

import com.dtr.oas.model.Profile;

public interface AppService {
	
	public void authorizeUser(Profile profile);
	
	public boolean isAjaxRequest(HttpServletRequest request);
	
	public String removeQueryStringParameter(String url);
	
	public Pageable pageable(Integer page, String sorting);
	
	public String getRandomString(int maxlength);

}
