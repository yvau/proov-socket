package com.dtr.oas.service;

import com.dtr.oas.model.ViewProposal;

public interface ViewProposalService {
	
	public ViewProposal findByIp(String ip);
	
	public ViewProposal save(ViewProposal viewProposal);

}
