package com.dtr.oas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtr.oas.model.Ip2nation;
import com.dtr.oas.repositories.Ip2nationRepository;


@Service
public class Ip2nationServiceImpl implements Ip2nationService {
	
	@Autowired
	Ip2nationRepository ip2nationRepository;
	
	public List<Ip2nation> findCountry(String ipAddress){
		Long ip =  ipToLong(ipAddress);
		System.out.println(ip);
		return ip2nationRepository.findFirst1ByIdLessThanOrderByIdDesc(ip);
	}
	
	private long ipToLong(String ipAddress) {
		 
		String[] ipAddressInArray = ipAddress.split("\\.");	 
		long result = 0;
		for (int i = 0; i < ipAddressInArray.length; i++) {
	 
			int power = 3 - i;
			int ip = Integer.parseInt(ipAddressInArray[i]);
			result += ip * Math.pow(256, power);
	 
		}
	 
		return result;
	  }

}