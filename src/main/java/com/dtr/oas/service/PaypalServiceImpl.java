package com.dtr.oas.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PaypalServiceImpl implements PaypalService {

  LinkedHashMap<String, String> list = new LinkedHashMap<String, String>();

  @Value("${paypal.username}")
  private String username;

  @Value("${paypal.password}")
  private String password;

  @Value("${paypal.signature}")
  private String signature;

  @Value("${paypal.method}")
  private String method;

  @Value("${paypal.version}")
  private String version;

  @Value("${paypal.endpoint}")
  private String endpoint;

  private String amount;

  @Value("${paypal.currency}")
  private String currency;

  @Value("${paypal.cancelurl}")
  private String cancelUrl;

  @Value("${paypal.returnurl}")
  private String returnUrl;


  private String paypalUrlPayment() {

    list.put("USER", getUsername());
    list.put("METHOD", getMethod());
    list.put("SIGNATURE", getSignature());
    list.put("PWD", getPassword());
    list.put("RETURNURL", getReturnUrl());
    list.put("CANCELURL", getCancelUrl());
    list.put("VERSION", getVersion());
    list.put("PAYMENTREQUEST_0_AMT", getAmount());
    list.put("PAYMENTREQUEST_0_CURRENCYCODE", getCurrency());

    return list.entrySet().stream()
        .map(e -> e.getKey() + "=" + e.getValue())
        .collect(Collectors.joining("&"));
  }


  public Boolean getPackage(String thePackage) {
    list.put("PACK1", "99");
    list.put("PACK2", "199");
    list.put("PACK3", "299");
    list.put("PACK4", "399");

    if (!list.containsKey(thePackage)) {
      return false;
    }
    setAmount(list.get(thePackage));
    return true;
  }


  public String sendPaypalPayment() throws Exception {

    URL obj = new URL(getEndpoint());
    HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

    //add request header
    con.setRequestMethod("POST");
    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    con.setRequestProperty("charset", "utf-8");

    // Send post request
    con.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
    wr.writeBytes(paypalUrlPayment());
    wr.flush();
    wr.close();

    BufferedReader in = new BufferedReader(
        new InputStreamReader(con.getInputStream()));
    String response = in.readLine();

    in.close();

    //print result

    Map<Object, Object> kvs =
        Arrays.asList(URLDecoder.decode(response, "UTF-8").split("&"))
            .stream()
            .map(elem -> elem.split("="))
            .collect(Collectors.toMap(e -> e[0], e -> e[1]));

    return kvs.get("TOKEN").toString();
  }


  public String getUsername() {
    return username;
  }


  public void setUsername(String username) {
    this.username = username;
  }


  public String getPassword() {
    return password;
  }


  public void setPassword(String password) {
    this.password = password;
  }


  public String getSignature() {
    return signature;
  }


  public void setSignature(String signature) {
    this.signature = signature;
  }


  public String getMethod() {
    return method;
  }


  public void setMethod(String method) {
    this.method = method;
  }


  public String getVersion() {
    return version;
  }


  public void setVersion(String version) {
    this.version = version;
  }


  public String getEndpoint() {
    return endpoint;
  }


  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }


  public String getAmount() {
    return amount;
  }


  public void setAmount(String amount) {
    this.amount = amount;
  }


  public String getCurrency() {
    return currency;
  }


  public void setCurrency(String currency) {
    this.currency = currency;
  }


  public String getCancelUrl() {
    return cancelUrl;
  }


  public void setCancelUrl(String cancelUrl) {
    this.cancelUrl = cancelUrl;
  }


  public String getReturnUrl() {
    return returnUrl;
  }


  public void setReturnUrl(String returnUrl) {
    this.returnUrl = returnUrl;
  }

}
