package com.dtr.oas.service;


import java.sql.Timestamp;
import java.util.List;

import com.dtr.oas.model.Calendar;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;

public interface CalendarService {
	
	public void save(Calendar calendar);

	public Calendar find(Profile profile, Long id);

	public List<Calendar> findByProfileAndStatus(Profile profile, String status);

	public List<Calendar> findByProfileVisitorAndStatus(Profile profile, String status);
	
	public List<Calendar> findByProfileAndPropertyAndStatus(Profile profile, Property property, String status);
	
	public Calendar findByProfileAndStatusAndStartDate(Profile profile, String status, Timestamp timeStampNow);

}
