package com.dtr.oas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileServiceDetails;

@ControllerAdvice
public class ExceptionControllerAdvice {

  @Autowired
  ProfileServiceDetails profileServiceDetails;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }

  @ExceptionHandler(NoHandlerFoundException.class)
  public String handle(Exception ex) {

    return "throwexception/notfound";//this is view name
  }
}
