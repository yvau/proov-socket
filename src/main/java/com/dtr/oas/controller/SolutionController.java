package com.dtr.oas.controller;

import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileServiceDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/our_solution")
public class SolutionController {

	@Autowired
	ProfileServiceDetails profileServiceDetails;

	/**
	 * @return
	 */
	@ModelAttribute("profile")
	public Profile getProfileDetails() {
		try{
			Profile profile = profileServiceDetails.getProfileDetails();
			return profile;
		}
		catch(Exception e){
			return null;
		}
	}

	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, produces = "text/html")
    public String solutionAction(ModelMap model){
		model.addAttribute("title","Proov :: our solution");
        return "default/solution";
    }
}
