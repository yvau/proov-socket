package com.dtr.oas.controller;

import com.dtr.oas.service.ProfileServiceDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dtr.oas.form.Greeting;
import com.dtr.oas.form.HelloMessage;
import com.dtr.oas.model.Profile;

@Controller
public class HomeController {

    @Autowired
    ProfileServiceDetails profileServiceDetails;

    /**
     * @return
     */
    @ModelAttribute("profile")
    public Profile getProfileDetails() {
        try{
            Profile profile = profileServiceDetails.getProfileDetails();
            return profile;
        }
        catch(Exception e){
            return null;
        }
    }


	@RequestMapping(value ="/" , method = RequestMethod.GET, produces = "text/html")
    public String home(ModelMap model){
      model.addAttribute("title", "Proov :: Home page");
       return "default/home";
    }
	
	@MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(3000); // simulated delay
        return new Greeting("Hello, " + message.getName() + "!");
    }

}
