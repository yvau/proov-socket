package com.dtr.oas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.model.Country;
import com.dtr.oas.model.Province;
import com.dtr.oas.service.CountryService;
import com.dtr.oas.service.ProvinceService;

/**
 * @author MOBIOT
 */
@Controller
@RequestMapping(value = "/province")
public class ProvinceController {

  @Autowired
  private ProvinceService provinceService;

  @Autowired
  private CountryService countryService;

  /**
   * @param id
   * @param model
   * @return
   */
  @RequestMapping(method = RequestMethod.GET)
  public @ResponseBody List<Province> getJsonProvince(@RequestParam(value = "id", required = false) String id, ModelMap model) {
    if (id == null) {
      return null;
    }
    Country country = countryService.find(id);
    return provinceService.findByCountry(country);
  }

}
