package com.dtr.oas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileServiceDetails;

@Controller
@RequestMapping(value="/partners")
public class PartnersController {

	
	@Autowired
	ProfileServiceDetails profileServiceDetails;

	/**
	 * @return
	 */
	@ModelAttribute("profile")
	public Profile getProfileDetails() {
		try{
			Profile profile = profileServiceDetails.getProfileDetails();
			return profile;
		}
		catch(Exception e){
			return null;
		}
	}

	/**
	 * @return
	 */
	@RequestMapping(produces = "text/html")
    public String partnersAction(ModelMap model){
		model.addAttribute("title", "Proov :: Partner page");
        return "default/partners";
    }
}
