package com.dtr.oas.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;
import com.dtr.oas.model.PropertyPhoto;
import com.dtr.oas.service.AppService;
import com.dtr.oas.service.ProfileServiceDetails;
import com.dtr.oas.service.PropertyPhotoService;
import com.dtr.oas.service.PropertyService;
import com.dtr.oas.validators.ValidationResponse;

/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("hasRole('ROLE_SELLER')")
@RequestMapping
public class ProfilePropertyPhotoController {

  private static final Logger log = LoggerFactory.getLogger(ProfilePropertyPhotoController.class);

  @Autowired
  private PropertyPhotoService propertyPhotoService;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private AppService appService;

  private String fileUploadDirectory = "/resources/images/uploads";


  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * @param id
   * @param model
   * @param request
   * @return
   * @throws NoHandlerFoundException
   */
  @RequestMapping(value = "/profile/property/{id}", method = RequestMethod.GET)
  public String index(@PathVariable("id") long id, ModelMap model, HttpServletRequest request) throws NoHandlerFoundException {
    Boolean ajaxrequest = false;
    Property property = propertyService.findByProfileAndId(getProfileDetails(), id);
    if (property == null) throw new NoHandlerFoundException(request.getMethod(), null,
        new ServletServerHttpRequest(request).getHeaders());
    model.addAttribute("title", "Proov :: property " + property.getPropertyType() + " - " + property.getLocation().getCountry().getId() + ", "
        + property.getLocation().getProvince().getName() + ", "
        + property.getLocation().getCity().getName() + ","
        + property.getLocation().getLocationString());

    model.addAttribute("property", property);
    request.getSession().setAttribute("idProperty", property.getId());
    if (appService.isAjaxRequest(request)) {
      ajaxrequest = true;
    }
    model.addAttribute("ajaxrequest", ajaxrequest);
    return "profileproperty/show";
  }

  /**
   * @param response
   * @param id
   */
  @RequestMapping(value = "/profile/property/{id}/gallery", method = RequestMethod.GET)
  public void imageGallery(HttpServletResponse response, @PathVariable Long id) {
    PropertyPhoto propertyPhoto = propertyPhotoService.find(id);
    ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

    String pathToGraphsDir = requestAttributes.getRequest().getRealPath("/");

    String storageDirectory = pathToGraphsDir + fileUploadDirectory;
    File imageFile = new File(storageDirectory + "/" + propertyPhoto.getThumbnailFilename());
    response.setContentType(propertyPhoto.getContentType());
    response.setContentLength(propertyPhoto.getThumbnailSize().intValue());
    try {
      InputStream is = new FileInputStream(imageFile);
      IOUtils.copy(is, response.getOutputStream());
    } catch (IOException e) {
      log.error("Could not show thumbnail " + id, e);
    }
  }

  /**
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/profile/property/upload", method = RequestMethod.POST)
  public @ResponseBody  ValidationResponse upload(MultipartHttpServletRequest request, HttpServletResponse response) {
    log.debug("uploadPost called");
    ValidationResponse res = new ValidationResponse();
    Long id = (Long) request.getSession().getAttribute("idProperty");

    Iterator<String> itr = request.getFileNames();
    MultipartFile mpf;
    List<PropertyPhoto> list = new LinkedList<>();
    Property property = propertyService.findByProfileAndId(getProfileDetails(), id);

    while (itr.hasNext()) {
      mpf = request.getFile(itr.next());
      log.debug("Uploading {}", mpf.getOriginalFilename());

      String newFilenameBase = UUID.randomUUID().toString();
      String originalFileExtension = mpf.getOriginalFilename().substring(mpf.getOriginalFilename().lastIndexOf("."));
      String newFilename = newFilenameBase + originalFileExtension;

      ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
      String pathToGraphsDir = requestAttributes.getRequest().getRealPath("/");

      String storageDirectory = pathToGraphsDir + fileUploadDirectory;
      String contentType = mpf.getContentType();

      File newFile = new File(storageDirectory + "/" + newFilename);
      if (!newFile.exists())
        newFile.mkdirs();
      try {
        res.setStatus(responseAction.SUCCESS.toString());
        //res.setAction(responseAction.INVOKE.toString());
        //res.setTarget(functionToInvoke.callBackSuccessFunctionFromUpload.toString());
        mpf.transferTo(newFile);

        BufferedImage thumbnail = Scalr.resize(ImageIO.read(newFile), 290);
        //BufferedImage thumbnail = Scalr.resize(ImageIO.read(newFile), Method.ULTRA_QUALITY, 200, 200);
        //BufferedImage thumbnail = Scalr.resize(ImageIO.read(newFile), Method.QUALITY, Mode.FIT_TO_WIDTH, 200,200);
        String thumbnailFilename = newFilenameBase + "-thumbnail.png";
        File thumbnailFile = new File(storageDirectory + "/" + thumbnailFilename);
        ImageIO.write(thumbnail, "png", thumbnailFile);

        PropertyPhoto propertyPhoto = new PropertyPhoto();
        propertyPhoto.setName(mpf.getOriginalFilename());
        propertyPhoto.setThumbnailFilename(thumbnailFilename);
        propertyPhoto.setNewFilename(newFilename);
        propertyPhoto.setContentType(contentType);
        propertyPhoto.setSize(mpf.getSize());
        propertyPhoto.setThumbnailSize(thumbnailFile.length());
        propertyPhoto.setProperty(property);
        propertyPhoto = propertyPhotoService.save(propertyPhoto);

        propertyPhoto.setUrl("/proov/picture/" + propertyPhoto.getId());
        propertyPhoto.setThumbnailUrl("/proov/thumbnail/" + propertyPhoto.getId());
        propertyPhoto.setDeleteUrl("/proov/delete/" + propertyPhoto.getId());
        propertyPhoto.setDeleteType("DELETE");

        list.add(propertyPhoto);


      } catch (IOException e) {
        res.setStatus(responseAction.FAIL.toString());
        log.error("Could not upload file " + mpf.getOriginalFilename(), e);

      }

    }
    return res;

  }

  /**
   * @param response
   * @param id
   */
  @RequestMapping(value = "/picture/{id}", method = RequestMethod.GET)
  public void picture(HttpServletResponse response, @PathVariable Long id) {
    PropertyPhoto propertyPhoto = propertyPhotoService.find(id);
    ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    String pathToGraphsDir = requestAttributes.getRequest().getRealPath("/");

    String storageDirectory = pathToGraphsDir + fileUploadDirectory;
    File imageFile = new File(storageDirectory + "/" + propertyPhoto.getNewFilename());
    response.setContentType(propertyPhoto.getContentType());
    response.setContentLength(propertyPhoto.getSize().intValue());
    try {
      InputStream is = new FileInputStream(imageFile);
      IOUtils.copy(is, response.getOutputStream());
    } catch (IOException e) {
      log.error("Could not show picture " + id, e);
    }
  }

  /**
   * @param response
   * @param id
   */
  @RequestMapping(value = "/thumbnail/{id}", method = RequestMethod.GET)
  public void thumbnail(HttpServletResponse response, @PathVariable Long id) {
    PropertyPhoto propertyPhoto = propertyPhotoService.find(id);
    ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

    String pathToGraphsDir = requestAttributes.getRequest().getRealPath("/");

    String storageDirectory = pathToGraphsDir + fileUploadDirectory;
    File imageFile = new File(storageDirectory + "/" + propertyPhoto.getThumbnailFilename());
    response.setContentType(propertyPhoto.getContentType());
    response.setContentLength(propertyPhoto.getThumbnailSize().intValue());
    try {
      InputStream is = new FileInputStream(imageFile);
      IOUtils.copy(is, response.getOutputStream());
    } catch (IOException e) {
      log.error("Could not show thumbnail " + id, e);
    }
  }

  /**
   * @param id
   * @return
   */
  @RequestMapping(value = "/profile/image/{id}/delete", method = RequestMethod.DELETE)
  public @ResponseBody ValidationResponse delete(@PathVariable Long id) {
    ValidationResponse res = new ValidationResponse();
    PropertyPhoto propertyPhoto = propertyPhotoService.find(id);
    ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    String pathToGraphsDir = requestAttributes.getRequest().getRealPath("/");

    String storageDirectory = pathToGraphsDir + fileUploadDirectory;
    File imageFile = new File(storageDirectory + "/" + propertyPhoto.getNewFilename());
    imageFile.delete();
    File thumbnailFile = new File(storageDirectory + "/" + propertyPhoto.getThumbnailFilename());
    thumbnailFile.delete();
    propertyPhotoService.delete(propertyPhoto);

    return res;
  }

}
