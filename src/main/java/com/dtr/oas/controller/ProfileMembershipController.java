package com.dtr.oas.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.form.MembershipForm;
import com.dtr.oas.model.MembershipPayment;
import com.dtr.oas.model.Profile;
import com.dtr.oas.pagewrapper.PageWrapper;
import com.dtr.oas.service.MembershipPaymentService;
import com.dtr.oas.service.PaypalService;
import com.dtr.oas.service.ProfileServiceDetails;
import com.dtr.oas.validators.ErrorMessage;
import com.dtr.oas.validators.MembershipValidator;
import com.dtr.oas.validators.ValidationResponse;


@Controller
@PreAuthorize("hasRole('ROLE_SELLER')")
@RequestMapping(value = "/profile")
public class ProfileMembershipController {

  @Autowired
  MembershipPaymentService membershipPaymentService;

  @Autowired
  MembershipValidator membershipValidator;

  @Autowired
  ProfileServiceDetails profileServiceDetails;

  @Autowired
  private MessageSource messageSource;

  private PaypalService paypalService;

  @Autowired
  ProfileMembershipController(PaypalService paypalService) {
    this.paypalService = paypalService;
  }


  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    Profile profile = profileServiceDetails.getProfileDetails();
    return profile;
  }

  /**
   * @return
   * @throws Exception
   * @throws IOException
   */
  @RequestMapping(value = {"/membership/", "/membership"}, method = RequestMethod.GET, produces = "text/html")
  public String homeAction(ModelMap model) throws Exception {
    model.addAttribute("title", "Proov :: Profile membership home page");
    /*model.addAttribute("subscription", gitProperties.sendPaypalPayment("PACK2"));*/
    return "profilemembership/home";
  }

  /**
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/membership/subscription", method = RequestMethod.GET, produces = "text/html")
  public String subscriptionForm(@RequestParam(value = "s", required = true) String s, ModelMap model) throws Exception {
    Boolean pack = paypalService.getPackage(s);
    if (!pack) {
      return "redirect:/profile/membership/";

    } else {
      MembershipForm membershipForm = new MembershipForm();
      model.addAttribute("title", "Proov :: Profile membership subscription page");
      model.addAttribute("token", paypalService.sendPaypalPayment());
      model.addAttribute("subscription", membershipForm);
      return "profilemembership/subscription";
    }
  }

  /**
   * @param request
   * @param bindingResult
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/membership/subscription/", method = RequestMethod.POST)
  public @ResponseBody ValidationResponse subscriptionAction(@Valid @ModelAttribute("subscription") MembershipForm membershipForm, ModelMap model, HttpServletRequest request, BindingResult bindingResult, Locale locale) throws Exception {
    ValidationResponse res = new ValidationResponse();
    membershipValidator.validate(membershipForm, bindingResult);
    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
            errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
          }
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {

      if (membershipForm.getMembershipPaymentMethod().equals("paypal")) {
        paypalService.sendPaypalPayment();
      }

    }
    return res;
		/*request.getParameter("membershipPaymentMethod");
        return "profilemembership/subscription";*/
  }

  /**
   * @return
   */
  @RequestMapping(value = "/membership/billing/", method = RequestMethod.GET, produces = "text/html")
  public String billingAction(ModelMap model, @RequestParam(value = "page", required = false) Integer page) {
    page = page == null ? 0 : (page.intValue() - 1);
    Pageable pageable = new PageRequest(page, EnumLabel.labelNumber.LabelPageMax.getNumber());
    PageWrapper<MembershipPayment> list = new PageWrapper<MembershipPayment>(membershipPaymentService.findAll(pageable), "");
    model.addAttribute("title", "Proov :: Profile membership billing page");
    model.addAttribute("list", list);
    return "profilemembership/billing";
  }

}
