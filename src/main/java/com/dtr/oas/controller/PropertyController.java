package com.dtr.oas.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import javax.servlet.http.HttpServletRequest;

import com.dtr.oas.model.*;
import com.dtr.oas.model.Calendar;
import com.dtr.oas.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.dtr.oas.enumeration.ResponseStatus;
import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.EnumLabel.idJsLabel;
import com.dtr.oas.enumeration.EnumLabel.statusReading;
import com.dtr.oas.enumeration.ResponseAction.functionToInvoke;
import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.form.PropertyForm;
import com.dtr.oas.pagewrapper.PageWrapper;
import com.dtr.oas.validators.ValidationResponse;

/**
 * @author MOBIOT
 */
@Controller
@RequestMapping(value = "/property")
public class PropertyController {

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private CountryService countryService;

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private BindingService bindingService;
  
  @Autowired
  private ProfileService profileService;

  @Autowired
  private CalendarService calendarService;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private ProfileNotificationService profileNotificationService;
  
  @Autowired
  private ProfileSettingService profileSettingService;

  @Autowired
  private ViewPropertyService viewPropertyService;

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  @Autowired
  private AppService appService;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * @return
   */
  @ModelAttribute("country")
  public
  @ResponseBody
  List<Country> getCountryList() {
    List<Country> countryList = countryService.findAll();
    return countryList;
  }

  @ModelAttribute("propertyTypeOfProperty")
  public List<EnumLabel.propertyTypeOfProperty> getPropertyTypeOfPropertyList() {
    List<EnumLabel.propertyTypeOfProperty> propertyTypeOfProperty = new ArrayList<EnumLabel.propertyTypeOfProperty>(Arrays.asList(EnumLabel.propertyTypeOfProperty.values()));
    return propertyTypeOfProperty;
  }


  /**
   * @param model
   * @param page
   * @param request
   * @return
   */
  @RequestMapping(method = RequestMethod.GET, produces = "text/html")
  public String homeAction(ModelMap model, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "sorting", required = false) String sorting, PropertyForm searchForm, HttpServletRequest request) {
    model.addAttribute("title", "Proov :: Properties");
    PropertyForm searchPropertyForm = new PropertyForm();
    model.addAttribute("searchForm", searchPropertyForm);

    Pageable pageable = appService.pageable(page, sorting);
    PageWrapper<Property> pageIn = new PageWrapper<Property>(propertyService.findAll(searchForm, pageable), "/property/" + appService.removeQueryStringParameter(request.getQueryString()));
    model.addAttribute("page", pageIn);

    return "property/list";
  }


  /**
   * @param model
   * @param id
   * @param request
   * @return
   * @throws NoHandlerFoundException
   */
  @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET, produces = "text/html")
  public String showAction(ModelMap model, @PathVariable("id") long id, HttpServletRequest request) throws NoHandlerFoundException {
    Property property = propertyService.find(id);
    if (property == null) throw new NoHandlerFoundException(null, null, null);
    model.addAttribute("title", "Proov :: "+property.getPropertyType()+" :: "+property.getLocation().getCountry().getId()+", "
                                           +property.getLocation().getProvince().getName()+", "
                                           +property.getLocation().getCity().getName()+", "
                                            +property.getLocation().getLocationString());

    setPropertyViewAction(property, request);
    model.addAttribute("property", property);
    return "property/show";
  }

  /**
   * @param id
   * @param request
   * @return
   */
  @PreAuthorize("hasRole('ROLE_BUYER')")
  @RequestMapping(value ="/{id}", method = RequestMethod.POST)
  public @ResponseBody ValidationResponse scheduleAction(@PathVariable("id") long id, @RequestParam(value = "response", required = false) String response,  @RequestParam(value = "bid", required = false) String bid, HttpServletRequest request, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    try {
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.INVOKE.toString());


      String valueTime = request.getParameter("dateAppointment");

      Property property = propertyService.find(id);
      property.setPropertyBidRequest(true);
      Calendar calendar = new Calendar();
      calendar.setStatus(ResponseStatus.pending.toString());
      calendar.setStartDate(getTimeStamp(valueTime, 0));
      calendar.setEndDate(getTimeStamp(valueTime, 1));
      calendar.setProfile1(property.getProfile());
      calendar.setProfile2(getProfileDetails());
      calendar.setProperty(property);
      calendar.setText(ResponseStatus.pending.toString());
      calendarService.save(calendar);

      ProfileNotification profileNotification = saveNotification(property, getProfileDetails());
      propertyService.save(property);
      sendNotification(profileNotification, locale);

      if(response == null && bid == null){

        res.setTarget(functionToInvoke.callBackSuccessFunctionRequestScheduled.toString());
        String[] arguments = {"fa-calendar", "fa-check-circle", "text-success"};
        res.setArguments(arguments);

      } else if(! response.equals(null) && !bid.equals("accepted")) {
        Binding binding = bindingService.find(Long.valueOf(bid));
        binding.setStatusInterest(ResponseStatus.accepted.toString());
        binding.setStatusReading(statusReading.LabelRead.toString());
        bindingService.save(binding);

        int bindingSize = bindingService.findByProposal(getProfileDetails(), binding.getProposal(), ResponseStatus.pending.toString()).size();
        if(bindingSize == 0){
        	ProfileSetting profileSetting = getProfileDetails().getProfileSetting();
        	if(binding.getProposal().getProposalType().equals(EnumLabel.proposalType.LabelBuyingProposal.toString())){
        		profileSetting.setShowNotificationBuyingProposal(false);
        	}else{
        		profileSetting.setShowNotificationRentingProposal(false);
        	}
        	profileSettingService.save(profileSetting);
        } 
        else if(bindingSize > 0 ){
        	ProfileSetting profileSetting = getProfileDetails().getProfileSetting();
        	if(binding.getProposal().getProposalType().equals(EnumLabel.proposalType.LabelBuyingProposal.toString())){
        		profileSetting.setShowNotificationBuyingProposal(true);
        	}else{
        		profileSetting.setShowNotificationRentingProposal(true);
        	}
        	profileSettingService.save(profileSetting);
        }

        String[] argument = {binding.getProperty().getId().toString()};
        String messageNotification = messageSource.getMessage("label.property.request.accepted", argument, locale);
        simpMessagingTemplate.convertAndSendToUser(binding.getProperty().getProfile().getEmail(),"/queue/note",messageNotification);

        res.setTarget(functionToInvoke.callBackSuccessFunctionFromModal.toString());
        String[] args = {messageSource.getMessage("label.operation", null, locale)};
        String message = messageSource.getMessage("message.save.success", args, locale);
        String proposalType = binding.getProposal().getProposalType().equals(EnumLabel.proposalType.LabelBuyingProposal.toString()) ? "buying_proposal" : "renting_proposal";
        String[] arguments = {String.valueOf(id), message, String.valueOf(bindingSize), String.valueOf(binding.getProposal().getId()), proposalType};
        res.setArguments(arguments);
      }

      return res;

    } catch (ClassCastException e) {
      res.setStatus(responseAction.FAIL.toString());
      return res;
    }

  }

  @Async
  private void setPropertyViewAction(Property property, HttpServletRequest request) {

    String referrer = request.getHeader("Referer");
    if (referrer != null) {

      String ipNameView = request.getRemoteAddr();
      ViewProperty viewProperty = viewPropertyService.findByIp(ipNameView);
      if (viewProperty != null) {
        int newCount = viewProperty.getIpCountView() + 1;
        viewProperty.setIpCountView(newCount);
        viewPropertyService.save(viewProperty);

      } else {
        viewProperty = new ViewProperty();
        viewProperty.setIpCountView(1);
        viewProperty.setIpNameView(ipNameView);
        viewProperty.setProperty(property);
        viewPropertyService.save(viewProperty);
      }

      int numberOfViews = property.getNumberOfViews() + 1;
      property.setNumberOfViews(numberOfViews);
      propertyService.save(property);
    }

  }

  /**
   * @param valueTime
   * @param Hour
   * @return
   */
  private Timestamp getTimeStamp(String valueTime, int Hour) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    LocalDateTime dateTime = LocalDateTime.parse(valueTime, formatter);
    Timestamp timestamp = Timestamp.valueOf(dateTime.plusHours(Hour));

    return timestamp;
  }

  /**
   * @param property
   * @param profile
   * @return
   */
  private ProfileNotification saveNotification(Property property, Profile profile) {
    Instant timestamp = Instant.now();
    ProfileNotification profileNotification = new ProfileNotification();
    ProfileSetting profileSetting = property.getProfile().getProfileSetting();
    profileSetting.setShowNotificationProperty(true);
    profileNotification.setId(appService.getRandomString(12));
    profileNotification.setNotificationStatus(statusReading.LabelNotRead.toString());
    profileNotification.setNotificationObject(idJsLabel.Property.toString());
    profileNotification.setNotificationTarget(String.valueOf(property.getId()));
    profileNotification.setNotificationDate(Timestamp.from(timestamp));

    profileNotification.setProfile1(property.getProfile());
    profileNotification.setProfile2(profile);

    profileNotificationService.save(profileNotification);
    profileSettingService.save(profileSetting);

    return profileNotification;
  }

  /**
   * @param locale
   * @return
   */
  private void sendNotification(ProfileNotification profileNotification, Locale locale) {
    Profile profile = profileNotification.getProfile1();
	int notificationCount = profile.getNotificationCount() == null ? 0 : Integer.parseInt(profile.getNotificationCount());
    int newNotificationCount = notificationCount + 1;
    profile.setNotificationCount(String.valueOf(newNotificationCount));
    profileService.save(profile, null, locale);

    profileNotificationService.save(profileNotification);
    String notification = messageSource.getMessage("label.property.notification.message", null, locale);
    String message = "{\"notifier\":\" "+ notification + "\", \"notificationCount\":\" "+ newNotificationCount +"\", \"css\":\"property\"}";
    simpMessagingTemplate.convertAndSendToUser(profileNotification.getProfile1().getEmail(), "/queue/note", message);
    //emailService.sendEmail(profile.getEmail(), "mobiot.fabrice@gmail.com", "Proov :: Request for visiting a property", calendar, "schedule-request.vm");
    return;
  }

}
