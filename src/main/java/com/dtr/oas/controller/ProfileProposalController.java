package com.dtr.oas.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.EnumLabel.proposalType;
import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.form.ProposalForm;
import com.dtr.oas.model.City;
import com.dtr.oas.model.Country;
import com.dtr.oas.model.Location;
import com.dtr.oas.model.Proposal;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Province;
import com.dtr.oas.pagewrapper.PageWrapper;
import com.dtr.oas.service.AppService;
import com.dtr.oas.service.CityService;
import com.dtr.oas.service.CountryService;
import com.dtr.oas.service.LocationService;
import com.dtr.oas.service.ProfileServiceDetails;
import com.dtr.oas.service.ProposalService;
import com.dtr.oas.service.ProvinceService;
import com.dtr.oas.validators.BuyingProposalValidator;
import com.dtr.oas.validators.ErrorMessage;
import com.dtr.oas.validators.RentingProposalValidator;
import com.dtr.oas.validators.ValidationResponse;

@Controller
@PreAuthorize("hasRole('ROLE_BUYER')")
@RequestMapping(value = "/profile")
@SessionAttributes("message")
public class ProfileProposalController {

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private LocationService locationService;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private CountryService countryService;

  @Autowired
  private ProvinceService provinceService;

  @Autowired
  private CityService cityService;

  @Autowired
  private ProposalService proposalService;

  @Autowired
  private AppService appService;

  @Autowired
  private RentingProposalValidator rentingProposalValidator;

  @Autowired
  private BuyingProposalValidator buyingProposalValidator;

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }


  /**
   * @return
   */
  @ModelAttribute("message")
  public String getMessage() {
    String message = "null";
    return message;
  }

  /**
   * @return
   */
  @ModelAttribute("country")
  public
  @ResponseBody
  List<Country> getCountryList() {
    List<Country> countryList = countryService.findAll();
    return countryList;
  }

  @ModelAttribute("propertyTypeOfProperty")
  public List<EnumLabel.propertyTypeOfProperty> getPropertyTypeOfPropertyList() {
    List<EnumLabel.propertyTypeOfProperty> propertyTypeOfProperty = new ArrayList<EnumLabel.propertyTypeOfProperty>(Arrays.asList(EnumLabel.propertyTypeOfProperty.values()));
    return propertyTypeOfProperty;
  }

  @ModelAttribute("propertyType")
  public List<EnumLabel.propertyType> getPropertyTypeList() {
    List<EnumLabel.propertyType> propertyType = new ArrayList<EnumLabel.propertyType>(Arrays.asList(EnumLabel.propertyType.values()));
    return propertyType;
  }

  @ModelAttribute("labelYesNo")
  public List<EnumLabel.yesNoLabel> getyesNoLabelList() {
    List<EnumLabel.yesNoLabel> yesNoLabels = new ArrayList<EnumLabel.yesNoLabel>(Arrays.asList(EnumLabel.yesNoLabel.values()));
    return yesNoLabels;
  }

  @ModelAttribute("typeOfAccommodation")
  public List<EnumLabel.typeOfAccommodation> getTypeOfAccommodationList() {
    List<EnumLabel.typeOfAccommodation> typeOfAccommodation = new ArrayList<EnumLabel.typeOfAccommodation>(Arrays.asList(EnumLabel.typeOfAccommodation.values()));
    return typeOfAccommodation;
  }

  @ModelAttribute("featuresRent")
  public List<EnumLabel.featuresRent> getFeaturesRentList() {
    List<EnumLabel.featuresRent> featuresRent = new ArrayList<EnumLabel.featuresRent>(Arrays.asList(EnumLabel.featuresRent.values()));
    return featuresRent;
  }


  @ModelAttribute("constructionOfProperty")
  public List<EnumLabel.constructionOfProperty> getConstructionOfPropertyList() {
    List<EnumLabel.constructionOfProperty> constructionOfProperty = new ArrayList<EnumLabel.constructionOfProperty>(Arrays.asList(EnumLabel.constructionOfProperty.values()));
    return constructionOfProperty;
  }

  @ModelAttribute("garage")
  public List<EnumLabel.garage> getGarageList() {
    List<EnumLabel.garage> garage = new ArrayList<EnumLabel.garage>(Arrays.asList(EnumLabel.garage.values()));
    return garage;
  }

  @ModelAttribute("windowsSiding")
  public List<EnumLabel.windowsSiding> getWindowsSidingList() {
    List<EnumLabel.windowsSiding> windowsSiding = new ArrayList<EnumLabel.windowsSiding>(Arrays.asList(EnumLabel.windowsSiding.values()));
    return windowsSiding;
  }

  @ModelAttribute("windowsConstruction")
  public List<EnumLabel.windowsConstruction> getWindowsConstructionList() {
    List<EnumLabel.windowsConstruction> windowsConstruction = new ArrayList<EnumLabel.windowsConstruction>(Arrays.asList(EnumLabel.windowsConstruction.values()));
    return windowsConstruction;
  }

  @ModelAttribute("water")
  public List<EnumLabel.water> getWaterList() {
    List<EnumLabel.water> water = new ArrayList<EnumLabel.water>(Arrays.asList(EnumLabel.water.values()));
    return water;
  }

  @ModelAttribute("ground")
  public List<EnumLabel.ground> getGroundList() {
    List<EnumLabel.ground> ground = new ArrayList<EnumLabel.ground>(Arrays.asList(EnumLabel.ground.values()));
    return ground;
  }

  @ModelAttribute("sewer")
  public List<EnumLabel.sewer> getSewerList() {
    List<EnumLabel.sewer> sewer = new ArrayList<EnumLabel.sewer>(Arrays.asList(EnumLabel.sewer.values()));
    return sewer;
  }

  @ModelAttribute("typeOfExteriorSiding")
  public List<EnumLabel.typeOfExteriorSiding> getTypeOfExteriorSidingList() {
    List<EnumLabel.typeOfExteriorSiding> typeOfExteriorSiding = new ArrayList<EnumLabel.typeOfExteriorSiding>(Arrays.asList(EnumLabel.typeOfExteriorSiding.values()));
    return typeOfExteriorSiding;
  }

  /**
   * @param model
   * @param page
   * @param request
   * @return
   */
  @RequestMapping(value = {"/renting-proposal/"}, method = RequestMethod.GET, produces = "text/html")
  public String homeRentingProposalAction(ModelMap model, @RequestParam(value = "filter", required = false) String filter, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "sorting", required = false) String sorting, HttpServletRequest request) {
    ProposalForm searchProposal = new ProposalForm();
    model.addAttribute("title", "Proov :: profile renting proposal");
    searchProposal.setProposalType(proposalType.LabelRentingProposal.toString());
    if (filter != null) {
      searchProposal.setNumberOfAnswerPropositionsUnseen(1);
    }
    Pageable pageable = appService.pageable(page, sorting);
    PageWrapper<Proposal> list = new PageWrapper<Proposal>(proposalService.findAll(searchProposal, pageable), "/profile/renting-proposal/" + appService.removeQueryStringParameter(request.getQueryString()));
    model.addAttribute("list", list);
    model.addAttribute("proposalType", "renting");
    return "profileproposal/home";
  }


  /**
   * @param model
   * @param page
   * @param request
   * @return
   */
  @RequestMapping(value = {"/buying-proposal/"}, method = RequestMethod.GET, produces = "text/html")
  public String homeBuyingProposalAction(ModelMap model, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "filter", required = false) String filter, @RequestParam(value = "sorting", required = false) String sorting, HttpServletRequest request) {
    ProposalForm searchProposal = new ProposalForm();
    model.addAttribute("title", "Proov :: profile buying proposal");
    searchProposal.setProposalType(proposalType.LabelBuyingProposal.toString());
    if (filter != null) {
      searchProposal.setNumberOfAnswerPropositionsUnseen(1);
    }
    Pageable pageable = appService.pageable(page, sorting);
    PageWrapper<Proposal> list = new PageWrapper<Proposal>(proposalService.findAll(searchProposal, pageable), "/profile/buying-proposal/" + appService.removeQueryStringParameter(request.getQueryString()));
    model.addAttribute("list", list);
    model.addAttribute("proposalType", "buying");
    return "profileproposal/home";
  }

  /**
   * @param model
   * @param id
   * @return
   * @throws NoHandlerFoundException
   */
  @RequestMapping(value = {"/proposal/{id}"}, method = RequestMethod.GET, produces = "text/html")
  public String showAction(ModelMap model, @PathVariable("id") Long id) throws NoHandlerFoundException {
    Proposal proposal = proposalService.findByProfileAndId(getProfileDetails(), id);
    if (proposal == null) throw new NoHandlerFoundException(null, null, null);
    model.addAttribute("title", "Proov :: profile proposal " + proposal.getProposalType());
    model.addAttribute("proposal", proposal);
    return "profileproposal/show";
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(value = {"/buying-proposal/new"}, method = RequestMethod.GET, produces = "text/html")
  public String profileBuyingProposalForm(ModelMap model, SessionStatus sessionStatus) {
    sessionStatus.setComplete();
    Proposal proposal = new Proposal();
    model.addAttribute("title", "Proov :: new buying proposal");
    model.addAttribute("proposal", proposal);

    return "profileproposal/newbuyingproposal";
  }

  /**
   * @param proposal
   * @param bindingResult
   * @param model
   * @param redirectAttributes
   * @param locale
   * @return
   */
  @RequestMapping(value = {"/buying-proposal/new"}, method = RequestMethod.POST)
  public @ResponseBody ValidationResponse profileBuyingProposalAction(@Valid @ModelAttribute("proposal") Proposal proposal, BindingResult bindingResult, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    buyingProposalValidator.validate(proposal, bindingResult);
    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {

      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      Country country = countryService.find(request.getParameter("location.country.id"));
      Province province = provinceService.find(request.getParameter("location.province.id"));
      City city = cityService.find(request.getParameter("location.city.id"));
      Location location = new Location();
      location.setId(appService.getRandomString(12));
      location.setCountry(country);
      location.setProvince(province);
      location.setCity(city);
      location.setLocationZipCode(request.getParameter("location.locationZipCode"));
      location.setLocationString(request.getParameter("location.locationString"));

      proposal.setLocation(location);
      proposal.setNumberOfAnswerPropositionsUnseen(0);
      proposal.setNumberOfAnswerPropositionsSeen(0);
      proposal.setNumberOfViews(0);
      Date dateTime = new Date();
      proposal.setDateCreated(new Timestamp(dateTime.getTime()));
      proposal.setProfile(profileServiceDetails.getProfileDetails());
      proposal.setProposalType(EnumLabel.proposalType.LabelBuyingProposal.toString());
      proposalService.save(proposal);

      res.setTarget(request.getContextPath() + "/profile/buying-proposal/new");
      simpMessagingTemplate.convertAndSend("/notification/proposal/buy", proposal.getId());
      String[] args = {messageSource.getMessage("label.proposal.buying", null, locale)};
      String message = messageSource.getMessage("message.save.success", args, locale);

      model.addAttribute("message", message);

    }
    return res;
  }


  /**
   * @param model
   * @param id
   * @param request
   * @param sessionStatus
   * @return
   * @throws NoHandlerFoundException
   */
  @RequestMapping(value = {"/proposal/{id}/edit"}, method = RequestMethod.GET, produces = "text/html")
  public String profileProposalForm(ModelMap model, @PathVariable("id") Long id, HttpServletRequest request, SessionStatus sessionStatus) throws NoHandlerFoundException {
    sessionStatus.setComplete();
    Proposal proposal = proposalService.findByProfileAndId(getProfileDetails(), id);
    if (proposal == null) throw new NoHandlerFoundException(null, null, null);
    model.addAttribute("title", "Proov :: edit " + proposal.getProposalType());
    request.getSession().setAttribute("idVariable", proposal.getLocation().getId());
    ProposalForm proposalForm = new ProposalForm();
    proposalForm.setLocation(proposal.getLocation());
    proposalForm.setPropertyType(proposal.getPropertyType());
    proposalForm.setMinimumPrice(String.valueOf(proposal.getMinimumPrice()));
    proposalForm.setMaximumPrice(String.valueOf(proposal.getMaximumPrice()));
    proposalForm.setBathroomsMin(proposal.getBathroomsMin());
    if(proposal.getProposalType().equals("proposal.renting")){
      proposalForm.setBathroomsMax(proposal.getBathroomsMax());
      proposalForm.setBedroomsMax(proposal.getBathroomsMax());
    }

    proposalForm.setBedroomsMin(proposal.getBedroomsMin());

    proposalForm.setParking(proposal.getParking());
    proposalForm.setFurnished(proposal.getFurnished());
    proposalForm.setProposalType(proposal.getProposalType());
    proposalForm.setPropertyTypeInProposal(proposal.getPropertyTypeInProposal());
    proposalForm.setRushPriority(proposal.getRushPriority());
    proposalForm.setTypeOfAccommodation(proposal.getTypeOfAccommodation());
    proposalForm.setConstructionOfProperty(proposal.getConstructionOfProperty());
    proposalForm.setPropertyType(proposal.getPropertyType());
    proposalForm.setSecurityFacilities(proposal.getSecurityFacilities());
    proposalForm.setAgeOfProperty(proposal.getAgeOfProperty());
    proposalForm.setPrivateEntry(proposal.getPrivateEntry());
    proposalForm.setTypeOfExteriorSiding(proposal.getTypeOfExteriorSiding());
    proposalForm.setGarage(proposal.getGarage());
    proposalForm.setWindowsSiding(proposal.getWindowsSiding());
    proposalForm.setMasterBedroomWithBathroom(proposal.getMasterBedroomWithBathroom());
    proposalForm.setWindowsConstruction(proposal.getWindowsConstruction());
    proposalForm.setUnhinderedAccess(proposal.getUnhinderedAccess());
    proposalForm.setCarShelter(proposal.getCarShelter());
    proposalForm.setWater(proposal.getWater());
    proposalForm.setGround(proposal.getGround());
    proposalForm.setSewer(proposal.getSewer());
    proposalForm.setFeatures(proposal.getFeatures());

    model.addAttribute("proposal", proposalForm);
    model.addAttribute("province", provinceService.findByCountry(proposal.getLocation().getCountry()));
    model.addAttribute("city", cityService.findByProvince(proposal.getLocation().getProvince()));

    if (proposal.getProposalType().equals(EnumLabel.proposalType.LabelRentingProposal.toString())) {
      return "profileproposal/updaterentingproposal";
    }
    return "profileproposal/updatebuyingproposal";
  }

  @RequestMapping(value = {"/proposal/{id}/edit"}, method = RequestMethod.POST)
  public
  @ResponseBody
  ValidationResponse updateAction(@Valid @ModelAttribute("proposal") ProposalForm proposalForm, @PathVariable("id") Long id, HttpServletRequest request, BindingResult bindingResult, ModelMap model, Locale locale) {
    ValidationResponse res = new ValidationResponse();

    if (request.getParameter("proposalType").equals(EnumLabel.proposalType.LabelBuyingProposal.toString())) {
      buyingProposalValidator.validate(proposalForm, bindingResult);
    } else {
      rentingProposalValidator.validate(proposalForm, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      Proposal proposal = proposalService.find(id);
      Country country = countryService.find(proposalForm.getLocation().getCountry().getId());
      Province province = provinceService.find(proposalForm.getLocation().getProvince().getId());
      City city = cityService.find(proposalForm.getLocation().getCity().getId());
      String idL = (String) request.getSession().getAttribute("idVariable");
      Location location = locationService.find(idL);
      location.setId(location.getId());
      location.setCountry(country);
      location.setProvince(province);
      location.setCity(city);
      location.setLocationString(proposalForm.getLocation().getLocationString());

      proposal.setLocation(location);
      proposal.setPropertyType(proposalForm.getPropertyType());
      proposal.setMinimumPrice(Double.parseDouble(proposalForm.getMinimumPrice()));
      proposal.setMaximumPrice(Double.parseDouble(proposalForm.getMaximumPrice()));
      proposal.setBathroomsMin(proposalForm.getBathroomsMin());
      proposal.setBathroomsMax(proposalForm.getBathroomsMax());
      proposal.setBedroomsMin(proposalForm.getBedroomsMin());
      proposal.setBedroomsMax(proposalForm.getBedroomsMax());
      proposal.setFeatures(proposalForm.getFeatures());
      proposal.setParking(proposalForm.getParking());
      proposal.setFurnished(proposalForm.getFurnished());
      proposalService.save(proposal);

      res.setTarget(request.getRequestURI());

      String[] args = {messageSource.getMessage("label." + proposalForm.getProposalType(), null, locale)};
      String message = messageSource.getMessage("message.save.success", args, locale);

      model.addAttribute("message", message);
    }
    return res;
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(value = {"/renting-proposal/new"}, method = RequestMethod.GET, produces = "text/html")
  public String profileRentingProposalForm(ModelMap model, SessionStatus sessionStatus) {
    sessionStatus.setComplete();
    ProposalForm proposal = new ProposalForm();
    model.addAttribute("title", "Proov :: new renting proposal");
    model.addAttribute("proposal", proposal);

    return "profileproposal/newrentingproposal";
  }

  /**
   * @param bindingResult
   * @param model
   * @param locale
   * @return
   */
  @RequestMapping(value = {"/renting-proposal/new"}, method = RequestMethod.POST)
  public @ResponseBody ValidationResponse profileRentingProposalAction(@Valid @ModelAttribute("proposal") ProposalForm proposalForm, HttpServletRequest request, BindingResult bindingResult, ModelMap model, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    rentingProposalValidator.validate(proposalForm, bindingResult);
    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      Proposal proposal = new Proposal();

      Country country = countryService.find(proposalForm.getLocation().getCountry().getId());
      Province province = provinceService.find(proposalForm.getLocation().getProvince().getId());
      City city = cityService.find(proposalForm.getLocation().getCity().getId());
      Location location = new Location();
      location.setId(appService.getRandomString(12));
      location.setCountry(country);
      location.setProvince(province);
      location.setCity(city);

      proposal.setLocation(location);
      proposal.setNumberOfAnswerPropositionsUnseen(0);
      proposal.setNumberOfAnswerPropositionsSeen(0);
      proposal.setNumberOfViews(0);
      Date dateTime = new Date();
      proposal.setPropertyTypeInProposal(proposalForm.getPropertyTypeInProposal());
      proposal.setMinimumPrice(Double.parseDouble(proposalForm.getMinimumPrice()));
      proposal.setMaximumPrice(Double.parseDouble(proposalForm.getMaximumPrice()));
      proposal.setBathroomsMin(proposalForm.getBathroomsMin());
      proposal.setBathroomsMax(proposalForm.getBathroomsMax());
      proposal.setBedroomsMin(proposalForm.getBedroomsMin());
      proposal.setBathroomsMax(proposalForm.getBathroomsMax());
      proposal.setParking(proposalForm.getParking());
      proposal.setFurnished(proposalForm.getFurnished());
      proposal.setPropertyType(proposalForm.getPropertyType());
      proposal.setFeatures(proposalForm.getFeatures());
      proposal.setDateCreated(new Timestamp(dateTime.getTime()));
      proposal.setProfile(profileServiceDetails.getProfileDetails());
      proposal.setProposalType(EnumLabel.proposalType.LabelRentingProposal.toString());
      proposalService.save(proposal);

      res.setTarget(request.getContextPath() + "/profile/renting-proposal/new");

      String[] args = {messageSource.getMessage("label.proposal.renting", null, locale)};
      String message = messageSource.getMessage("message.save.success", args, locale);

      model.addAttribute("message", message);
    }
    return res;

  }


}