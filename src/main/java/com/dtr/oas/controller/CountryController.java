package com.dtr.oas.controller; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.model.Country;
import com.dtr.oas.service.CountryService;


/**
 * @author MOBIOT
 *
 */
@Controller 
@RequestMapping(value="/country")
public class CountryController {
	
	@Autowired
    private CountryService countryService;

	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Country> getCountryList() {
		List<Country> countryList = countryService.findAll();
		
		return countryList;
	}    

}