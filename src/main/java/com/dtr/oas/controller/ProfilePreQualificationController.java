package com.dtr.oas.controller;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.model.Budget;
import com.dtr.oas.model.Country;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileSetting;
import com.dtr.oas.service.BudgetService;
import com.dtr.oas.service.CityService;
import com.dtr.oas.service.CountryService;
import com.dtr.oas.service.ProfileServiceDetails;
import com.dtr.oas.service.ProfileSettingService;
import com.dtr.oas.service.ProvinceService;
import com.dtr.oas.validators.ValidationResponse;


/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("hasRole('ROLE_BUYER')")
@RequestMapping(value = "/profile/prequalification")
public class ProfilePreQualificationController {

  @Autowired
  private CountryService countryService;

  @Autowired
  private ProvinceService provinceService;

  @Autowired
  private CityService cityService;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private BudgetService budgetService;

  @Autowired
  private ProfileSettingService profileSettingService;

  /**
   * @return
   */

  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * @return
   */
  @ModelAttribute("country")
  public @ResponseBody List<Country> getCountryList() {
    List<Country> countryList = countryService.findAll();
    return countryList;
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(method = RequestMethod.GET, produces = "text/html")
  public String preQualificationForm(ModelMap model) {
    Budget budget = budgetService.findByProfile(getProfileDetails());
    if (budget == null) {
      budget = new Budget();
    }

    model.addAttribute("title", "Proov :: Profile prequalification page");
    model.addAttribute("budget", budget);
    model.addAttribute("province", provinceService.findByCountry(budget.getCountry()));
    model.addAttribute("city", cityService.findByProvince(budget.getProvince()));

    return "profileprequalification/new";
  }

  @RequestMapping(method = RequestMethod.GET, params = "show", produces = "text/html")
  public String preQualificationShow(ModelMap model) {
    Budget budget = budgetService.findByProfile(getProfileDetails());
    model.addAttribute("title", "Proov :: Profile show prequalification");
    model.addAttribute("budget", budget);
    return "profileprequalification/show";
  }


  @RequestMapping(method = RequestMethod.POST)
  public @ResponseBody ValidationResponse preQualificationAction(@ModelAttribute("budget") Budget budget, HttpServletRequest request) {
    ValidationResponse res = new ValidationResponse();
    res.setStatus(responseAction.SUCCESS.toString());
    res.setAction(responseAction.REDIRECT.toString());
    Budget budget1 = budgetService.findByProfile(getProfileDetails());
    if (budget1.getId() != null) {
      budget.setId(budget1.getId());
    }

    if(budget1.getDateCreated() == null){
      Instant timestamp = Instant.now();
      budget.setDateCreated(Timestamp.from(timestamp));
    }

    budget.setProfile(getProfileDetails());
    res.setTarget(request.getContextPath() + "/profile/prequalification?show");
    ProfileSetting profileSetting = profileSettingService.findByProfile(getProfileDetails());
    if (!profileSetting.getShowPrequalificationCompleted()) {
      profileSetting.setShowPrequalificationCompleted(true);
      profileSettingService.save(profileSetting);
    }
    budgetService.save(budget);

    return res;
  }

}
