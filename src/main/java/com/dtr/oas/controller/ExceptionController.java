package com.dtr.oas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileServiceDetails;

@Controller
public class ExceptionController {
	
	@Autowired
	ProfileServiceDetails profileServiceDetails;

	/**
	 * @return
	 */
	@ModelAttribute("profile")
	public Profile getProfileDetails() {
		try{
			Profile profile = profileServiceDetails.getProfileDetails();
			return profile;
		}
		catch(Exception e){
			return null;
		}
	}

	@RequestMapping(value="/403", method = RequestMethod.GET, produces = "text/html")
    public String the403Action(ModelMap model){
		model.addAttribute("title", "Proov :: 403 denied");
        return "throwexception/403";
    }

}
