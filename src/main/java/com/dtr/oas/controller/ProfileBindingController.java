package com.dtr.oas.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.time.Instant;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.dtr.oas.form.NotificationMessage;
import com.dtr.oas.model.*;
import com.dtr.oas.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.EnumLabel.idJsLabel;
import com.dtr.oas.enumeration.EnumLabel.statusReading;
import com.dtr.oas.enumeration.ResponseAction.functionToInvoke;
import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.enumeration.ResponseStatus;
import com.dtr.oas.form.BindingForm;
import com.dtr.oas.validators.ValidationResponse;

/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = "/profile/binding")
public class ProfileBindingController {

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private BindingService bindingService;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  @Autowired
  private ProposalService proposalService;

  @Autowired
  private ProfileSettingService profileSettingService;

  @Autowired
  private ProfileNotificationService profileNotificationService;

  @Autowired
  private AppService appService;

  @Autowired
  private ProfileService profileService;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }


  /**
   * @return
   */
  @PreAuthorize("hasRole('ROLE_SELLER')")
  @RequestMapping(method = RequestMethod.POST)
  public @ResponseBody ValidationResponse formBindingAction(@ModelAttribute("binding") BindingForm bindingForm, HttpServletRequest request, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    List<Binding> bindings = bindingForm.getBindings();
    long proposalId = Long.parseLong(request.getParameter("theproposal"));
    Proposal proposal = proposalService.find(proposalId);
    int index = 0;
    if (null != bindings && bindings.size() > 0) {

      for (Binding binding : bindings) {
        try {
          if (binding.getProperty().getId() != null) {
            Binding bindingInForLoop = new Binding();
            bindingInForLoop.setProfile(proposal.getProfile());
            bindingInForLoop.setProperty(binding.getProperty());
            bindingInForLoop.setProposal(binding.getProposal());
            Date dateTime = new Date();
            bindingInForLoop.setDateOfBinding(new Timestamp(dateTime.getTime()));
            bindingInForLoop.setStatusInterest(ResponseStatus.pending.toString());
            bindingInForLoop.setStatusReading(EnumLabel.statusReading.LabelNotRead.toString());

            bindingService.save(bindingInForLoop);
            ProfileNotification profileNotification = saveNotification(proposal, getProfileDetails());
            sendNotification(profileNotification, locale);

          }
        } catch (Exception e) {
          System.out.println(e);
        }

        //index = index + 1;
      }
    }
    ProfileSetting profileSetting = proposal.getProfile().getProfileSetting();
    if (proposal.getProposalType().equals("proposal.buying")) {
      profileSetting.setShowNotificationBuyingProposal(true);
    } else {
      profileSetting.setShowNotificationRentingProposal(true);
    }
    profileSettingService.save(profileSetting);

    res.setAction(responseAction.INVOKE.toString());
    res.setTarget(functionToInvoke.callBackBindings.toString());
    res.setStatus(responseAction.SUCCESS.toString());
    String[] args = {messageSource.getMessage("label.operation", null, locale)};
    String message = messageSource.getMessage("message.save.success", args, locale);
    String[] arguments = {message};
    res.setArguments(arguments);
    return res;
  }

  /**
   * @return
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.POST)
  public @ResponseBody ValidationResponse responseRequestBindingAction(@PathVariable("id") Long id, @RequestParam(value = "response", required = false) String response, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    if (response.equals("accepted") || response.equals("decline")) {
      Binding binding = bindingService.find(id);
      binding.setStatusInterest(response);
      bindingService.save(binding);

      int bindingSize = bindingService.findByProposal(getProfileDetails(), binding.getProposal(), ResponseStatus.pending.toString()).size();
      if (bindingSize == 0) {
        ProfileSetting profileSetting = getProfileDetails().getProfileSetting();
        if (binding.getProposal().getProposalType().equals(EnumLabel.proposalType.LabelBuyingProposal.toString())) {
          profileSetting.setShowNotificationBuyingProposal(false);
        } else {
          profileSetting.setShowNotificationRentingProposal(false);
        }
        profileSettingService.save(profileSetting);
      }
      String[] argument = {binding.getProposal().getId().toString()};
      String messageNotification = messageSource.getMessage("label.property.request.accepted", argument, locale);
      simpMessagingTemplate.convertAndSendToUser(binding.getProperty().getProfile().getEmail(), "/queue/note", messageNotification);
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.INVOKE.toString());
      res.setTarget(functionToInvoke.callBackSuccessFunctionFromModal.toString());
      String[] args = {messageSource.getMessage("label.operation", null, locale)};
      String message = messageSource.getMessage("message.save.success", args, locale);
      String proposalType = binding.getProposal().getProposalType().equals(EnumLabel.proposalType.LabelBuyingProposal.toString()) ? "buying_proposal" : "renting_proposal";
      String[] arguments = {String.valueOf(binding.getProperty().getId()), message, String.valueOf(bindingSize), String.valueOf(binding.getProposal().getId()), proposalType};
      res.setArguments(arguments);
      return res;
    } else {
      res.setStatus(responseAction.FAIL.toString());
      return res;
    }
  }

  /**
   * @param model
   * @param model
   * @param id
   * @return
   */
  @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET, produces = "text/html")
  public String listBindingFromProposalAction(ModelMap model, @RequestParam(value = "sorting", required = false) String sorting, @PathVariable("id") Long id) {
    Proposal proposal = proposalService.find(id);
    List<Binding> binding = bindingService.findByProposal(getProfileDetails(), proposal, ResponseStatus.pending.toString());
    model.addAttribute("title", "Proov :: Profile list binding from proposal page");
    model.addAttribute("list", binding);
    return "profilebinding/list";
  }

  /**
   * @return
   */
  private ProfileSetting profileSetting(ProfileSetting profileSetting) {
    profileSetting.setShowNotificationProperty(true);
    profileSettingService.save(profileSetting);
    return profileSetting;
  }

  /**
   * @param profile
   * @return
   */
  private ProfileNotification saveNotification(Proposal proposal, Profile profile) {
    Instant timestamp = Instant.now();
    ProfileNotification profileNotification = new ProfileNotification();
    profileNotification.setId(appService.getRandomString(12));
    profileNotification.setNotificationStatus(statusReading.LabelNotRead.toString());
    profileNotification.setNotificationObject(proposal.getProposalType());
    profileNotification.setNotificationTarget(String.valueOf(proposal.getId()));
    profileNotification.setNotificationDate(Timestamp.from(timestamp));

    profileNotification.setProfile1(proposal.getProfile());
    profileNotification.setProfile2(profile);

    profileNotificationService.save(profileNotification);
    return profileNotification;
  }

  /**
   * @param locale
   * @return
   */
  private void sendNotification(ProfileNotification profileNotification, Locale locale) {
    Profile profile = profileNotification.getProfile1();
    int notificationCount = profile.getNotificationCount() == null ? 0 : Integer.parseInt(profile.getNotificationCount());
    int newNotificationCount = notificationCount + 1;

    profile.setNotificationCount(String.valueOf(newNotificationCount));
    profileService.save(profile, null, null);
    String[] args = {messageSource.getMessage("label." + profileNotification.getNotificationObject(), null, locale)};
    String notification = messageSource.getMessage("label.proposal.notification.message", args, locale);
    String theProposal = profileNotification.getNotificationObject().equals(EnumLabel.proposalType.LabelBuyingProposal.toString()) ? "buying_proposal" : "renting_proposal";
    /*List<NotificationMessage> profileNotification1 = profileNotificationService.findByProfileForNotification(profile);
    for (NotificationMessage profileNotification2 : profileNotification1){
      System.out.println("*********" + profileNotification2);
    }*/
    String message = "{\"notifier\":\" " + notification + "\", \"css\":\"" + theProposal + "\"}";

    simpMessagingTemplate.convertAndSendToUser(profileNotification.getProfile1().getEmail(), "/queue/note", message);
    //emailService.sendEmail(profile.getEmail(), "mobiot.fabrice@gmail.com", "Proov :: Request for visiting a property", calendar, "schedule-request.vm");
    return;
  }

}
