package com.dtr.oas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.model.Ip2nation;
import com.dtr.oas.service.Ip2nationService;

/**
 * @author MOBIOT
 *
 */
@Controller 
@RequestMapping(value="/ip2nation")
public class Ip2nationController {
	
	@Autowired
    private Ip2nationService ip2nationService;
	
	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Ip2nation> getIp2nationList(String ipAddress) {
		
		
		List<Ip2nation> ip2nationList = ip2nationService.findCountry("52.74.85.216");
		
		return ip2nationList;
	}
}
