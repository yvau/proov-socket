package com.dtr.oas.controller;

import java.security.Principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileNotificationService;
import com.dtr.oas.service.ProfileServiceDetails;


@Controller
public class PortfolioController {

  private static final Log logger = LogFactory.getLog(PortfolioController.class);

  @Autowired
  private ProfileNotificationService profileNotificationService;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    Profile profile = profileServiceDetails.getProfileDetails();
    return profile;
  }

  @SubscribeMapping("/positions")
  public String getPositions(Principal principal) throws Exception {

    return profileNotificationService.findByProfile(getProfileDetails());
  }

  /*@PreAuthorize("hasRole('ROLE_ADMIN')")
  @MessageMapping("/trade")
  public void executeTrade(Trade trade, Principal principal) {
    trade.setUsername(principal.getName());
    logger.debug("Trade: " + trade);
    this.tradeService.executeTrade(trade);
  }*/

  @MessageExceptionHandler
  @SendToUser("/queue/errors")
  public String handleException(Throwable exception) {
    return exception.getMessage();
  }

}
