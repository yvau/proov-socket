package com.dtr.oas.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.EnumLabel.modalId;
import com.dtr.oas.enumeration.ResponseAction;
import com.dtr.oas.model.Country;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileSetting;
import com.dtr.oas.model.PropertySearch;
import com.dtr.oas.model.ProposalSearch;
import com.dtr.oas.service.CityService;
import com.dtr.oas.service.CountryService;
import com.dtr.oas.service.ProfileServiceDetails;
import com.dtr.oas.service.ProfileSettingService;
import com.dtr.oas.service.PropertySearchService;
import com.dtr.oas.service.ProposalSearchService;
import com.dtr.oas.service.ProvinceService;
import com.dtr.oas.validators.BuyingProposalSearchValidator;
import com.dtr.oas.validators.ErrorMessage;
import com.dtr.oas.validators.PropertySearchValidator;
import com.dtr.oas.validators.RentingProposalSearchValidator;
import com.dtr.oas.validators.ValidationResponse;

/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_SELLER','ROLE_BUYER')")
@RequestMapping(value = "/profile/search")
public class ProfileSearchController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ProposalSearchService proposalSearchService;

    @Autowired
    private PropertySearchService propertySearchService;

    @Autowired
    private ProfileServiceDetails profileServiceDetails;

    @Autowired
    private RentingProposalSearchValidator rentingProposalSearchValidator;

    @Autowired
    private BuyingProposalSearchValidator buyingProposalSearchValidator;

    @Autowired
    private PropertySearchValidator propertySearchValidator;

    @Autowired
    private ProfileSettingService profileSettingService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private ProvinceService provinceService;

    @Autowired
    private CityService cityService;


    @ModelAttribute("profile")
    public Profile getProfileDetails() {
        Profile profile = profileServiceDetails.getProfileDetails();
        return profile;
    }


    @ModelAttribute("country")
    public
    @ResponseBody
    List<Country> getCountryList() {
        List<Country> countryList = countryService.findAll();
        return countryList;
    }

    @ModelAttribute("labelYesNo")
    public List<EnumLabel.yesNoLabel> getyesNoLabelList() {
        List<EnumLabel.yesNoLabel> yesNoLabels = new ArrayList<EnumLabel.yesNoLabel>(Arrays.asList(EnumLabel.yesNoLabel.values()));
        return yesNoLabels;
    }


    @ModelAttribute("propertyType")
    public List<EnumLabel.propertyType> getPropertyTypeList() {
        List<EnumLabel.propertyType> propertyType = new ArrayList<EnumLabel.propertyType>(Arrays.asList(EnumLabel.propertyType.values()));
        return propertyType;
    }

    @ModelAttribute("propertySaleType")
    public List<EnumLabel.propertySaleType> getPropertySaleTypeList() {
        List<EnumLabel.propertySaleType> propertySaleType = new ArrayList<EnumLabel.propertySaleType>(Arrays.asList(EnumLabel.propertySaleType.values()));
        return propertySaleType;
    }

    @ModelAttribute("propertyTypeOfProperty")
    public List<EnumLabel.propertyTypeOfProperty> getPropertyTypeOfPropertyList() {
        List<EnumLabel.propertyTypeOfProperty> propertyTypeOfProperty = new ArrayList<EnumLabel.propertyTypeOfProperty>(Arrays.asList(EnumLabel.propertyTypeOfProperty.values()));
        return propertyTypeOfProperty;
    }

/*
	
	
	@ModelAttribute("featuresRent")
	public List<EnumLabel.featuresRent> getFeaturesRentList() {
		List<EnumLabel.featuresRent> featuresRent = new ArrayList<EnumLabel.featuresRent>( Arrays.asList(EnumLabel.featuresRent.values() ));
		return featuresRent;
    }
	

	@RequestMapping(value ="/rent", method = RequestMethod.GET, produces = "text/html")
    public String rentingProposalSearchHome(@RequestParam(value = "page", required = false) Integer page, ModelMap model) {
        try {
			page = page == null ? 0 : (page.intValue() - 1);
			Pageable pageable = new PageRequest(page, EnumLabel.LabelNumber.LabelPageMax.getNumber());
			ProposalSearch owningProposalSearch = owningProposalSearchService.findByProfileAndProposalType(getProfileDetails(), EnumLabel.ProposalType.LabelRentingProposal.toString());
			Page<Proposal>  proposal  = proposalService.findAll(getRentingProposalSearch(owningProposalSearch), pageable);
			model.addAttribute("proposal", proposal.getContent());
			
		} catch (Exception e) {
			// TODO: handle exception
			//crete a fake List<>
			List<Proposal> proposals = new ArrayList<>();
			model.addAttribute("proposal", proposals);
		}
		return "profilesearch/searchrentingproposal";
    }
	

	@RequestMapping(value ="/buying-proposal", method = RequestMethod.POST)
    public @ResponseBody ValidationResponse buyingProposalSearchAction(@Valid @ModelAttribute("owningProposalSearch") ProposalSearch owningProposalSearch, BindingResult bindingResult, ModelMap model, HttpServletRequest request, Locale locale) {
		ValidationResponse res = new ValidationResponse();
		buyingProposalSearchValidator.validate(owningProposalSearch, bindingResult);
		//validation handling
		if (bindingResult.hasErrors() ){
			res.setStatus("FAIL");
			List<FieldError> allErrors = bindingResult.getFieldErrors();
    		List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
    		for (FieldError objectError : allErrors) {
				errorMesages.add(new ErrorMessage(objectError.getField(), objectError.getDefaultMessage()));
			}
    	res.setErrorMessageList(errorMesages);
    	
		}else {
			res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
	        res.setAction(ResponseAction.responseAction.REDIRECT.toString());
			
    		owningProposalSearch.setProfile(getProfileDetails());
    		owningProposalSearchService.save(owningProposalSearch);

    		res.setTarget(request.getRequestURI());
	        
	        String[] args = {messageSource.getMessage("label.menu.yourProperty", null, locale)};
			String message = messageSource.getMessage("message.save.success", args, locale);
			
			model.addAttribute("message", message);
    	}
		return res;
    }
	
	

	@RequestMapping(value ="/buy", method = RequestMethod.GET)
    public String buyingProposalSearchHomeForm(ModelMap model) {
		
		return "profilesearch/searchbuyingproposal";
    }
	
	
	

	@RequestMapping(value ="/property/list", method = RequestMethod.GET, produces = "text/html")
    public String settingPropertySearchList(ModelMap model, @RequestParam(value = "page", required = false) Integer page) {
		page = page == null ? 0 : (page.intValue() - 1);
		Pageable pageable = new PageRequest(page, EnumLabel.LabelNumber.LabelPageMax.getNumber());
		PropertySearch propertySearch = propertySearchService.findByProfile(getProfileDetails());
		Page<Property>  property  = propertyService.findAll(getPropertySearch(propertySearch), pageable);
		model.addAttribute("list", property);
		return "profilesettings/listsearchproperty";
    }
	

	@RequestMapping(value ="/buying-proposal/list", method = RequestMethod.GET, produces = "text/html")
    public String settingBuyingProposalSearchList(ModelMap model, @RequestParam(value = "page", required = false) Integer page) {
		page = page == null ? 0 : (page.intValue() - 1);
		Pageable pageable = new PageRequest(page, EnumLabel.LabelNumber.LabelPageMax.getNumber());
		ProposalSearch owningProposalSearch = owningProposalSearchService.findByProfileAndProposalType(getProfileDetails(), EnumLabel.ProposalType.LabelBuyingProposal.toString());
		Page<Proposal>  owningProposal  = proposalService.findAll(getBuyingProposalSearch(owningProposalSearch), pageable);
		model.addAttribute("list", owningProposal);
		return "profilesettings/listsearchproposal";
    }
	

	@RequestMapping(value ="/renting-proposal/list", method = RequestMethod.GET, produces = "text/html")
    public String settingRentingProposalSearchList(ModelMap model, @RequestParam(value = "page", required = false) Integer page) {
		page = page == null ? 0 : (page.intValue() - 1);
		Pageable pageable = new PageRequest(page, EnumLabel.LabelNumber.LabelPageMax.getNumber());
		ProposalSearch owningProposalSearch = owningProposalSearchService.findByProfileAndProposalType(getProfileDetails(), EnumLabel.ProposalType.LabelRentingProposal.toString());
		Page<Proposal>  owningProposal  = proposalService.findAll(getRentingProposalSearch(owningProposalSearch), pageable);
		//model.addAttribute("list", owningProposal);
		return owningProposal.toString();
    }*/

    /**
     * @param model
     * @return
     */
    @RequestMapping(value = "/property", method = RequestMethod.GET, produces = "text/html")
    public String propertySearchForm(ModelMap model) {

        PropertySearch propertySearch = propertySearchService.findByProfile(getProfileDetails());
        if (propertySearch == null) {
            propertySearch = new PropertySearch();
        }
        model.addAttribute("province", provinceService.findByCountry(propertySearch.getCountry()));
        model.addAttribute("city", cityService.findByProvince(propertySearch.getProvince()));
        model.addAttribute("propertySearch", propertySearch);

        return "profilesearch/searchproperty";
    }


    /**
     * @param request
     * @param locale
     * @return
     */
    @RequestMapping(value = "/property", method = RequestMethod.POST)
    public
    @ResponseBody
    ValidationResponse settingPropertySearchAction(@Valid @ModelAttribute("propertySearch") PropertySearch propertySearch, BindingResult bindingResult, ModelMap model, HttpServletRequest request, Locale locale) {
        ValidationResponse res = new ValidationResponse();
        propertySearchValidator.validate(propertySearch, bindingResult);
        //validation handling
        if (bindingResult.hasErrors()) {
            res.setStatus(ResponseAction.responseAction.FAIL.toString());
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
            ArrayList<Object> argsFor = new ArrayList<Object>();

            for (FieldError objectError : allErrors) {
                try {
                    for (Object d : objectError.getArguments()) {
                        argsFor.add(messageSource.getMessage(d.toString(), null, locale));
                    }
                    errorMesages.add(new ErrorMessage("property_" + objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
                } catch (Exception e) {
                    errorMesages.add(new ErrorMessage("property_" + objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
                }

            }
            res.setErrorMessageList(errorMesages);

        } else {
            res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
            res.setAction(ResponseAction.responseAction.INVOKE.toString());
            res.setTarget(ResponseAction.functionToInvoke.callBackCreateCriteria.toString());
            String[] arguments = {modalId.TargetModalProperty.toString(), EnumLabel.idJsLabel.Property.toString()};
            res.setArguments(arguments);

            propertySearch.setProfile(getProfileDetails());
            propertySearchService.save(propertySearch);

            ProfileSetting profileSetting = profileSettingService.findByProfile(getProfileDetails());
            profileSetting.setShowSearchCriteriaPropertyCompleted(true);
            profileSettingService.save(profileSetting);

            String[] args = {messageSource.getMessage("label.property", null, locale)};
            String message = messageSource.getMessage("message.save.success", args, locale);

            model.addAttribute("message", message);
        }
        return res;
    }

    /**
     * @param model
     * @return
     */
    @RequestMapping(value = "/buy", method = RequestMethod.GET, produces = "text/html")
    public String buySearchForm(ModelMap model) {

        ProposalSearch proposalSearch = proposalSearchService.findByProfileAndProposalType(getProfileDetails(), EnumLabel.proposalType.LabelBuyingProposal.toString());
        if (proposalSearch == null) {
            proposalSearch = new ProposalSearch();
        }
        model.addAttribute("province", provinceService.findByCountry(proposalSearch.getCountry()));
        model.addAttribute("city", cityService.findByProvince(proposalSearch.getProvince()));
        model.addAttribute("proposalSearch", proposalSearch);

        return "profilesearch/searchbuyingproposal";
    }


    /**
     * @param request
     * @param locale
     * @return
     */
    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    public
    @ResponseBody
    ValidationResponse settingBuyingProposalSearchAction(@Valid @ModelAttribute("proposalSearch") ProposalSearch proposalSearch, BindingResult bindingResult, ModelMap model, HttpServletRequest request, Locale locale) {
        ValidationResponse res = new ValidationResponse();
        buyingProposalSearchValidator.validate(proposalSearch, bindingResult);
        //validation handling
        if (bindingResult.hasErrors()) {
            res.setStatus(ResponseAction.responseAction.FAIL.toString());
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
            ArrayList<Object> argsFor = new ArrayList<Object>();

            for (FieldError objectError : allErrors) {
                try {
                    for (Object d : objectError.getArguments()) {
                        argsFor.add(messageSource.getMessage(d.toString(), null, locale));
                    }
                    errorMesages.add(new ErrorMessage("buy_" + objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
                } catch (Exception e) {
                    errorMesages.add(new ErrorMessage("buy_" + objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
                }

            }
            res.setErrorMessageList(errorMesages);

        } else {
            res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
            res.setAction(ResponseAction.responseAction.INVOKE.toString());
            res.setTarget(ResponseAction.functionToInvoke.callBackCreateCriteria.toString());
            String[] arguments = {modalId.TargetModalBuy.toString(), EnumLabel.idJsLabel.Buy.toString()};
            res.setArguments(arguments);

            proposalSearch.setProfile(getProfileDetails());
            proposalSearch.setProposalType(EnumLabel.proposalType.LabelBuyingProposal.toString());
            proposalSearchService.save(proposalSearch);

            ProfileSetting profileSetting = profileSettingService.findByProfile(getProfileDetails());
            profileSetting.setShowSearchCriteriaBuyingProposalCompleted(true);
            profileSettingService.save(profileSetting);


            String[] args = {messageSource.getMessage("label.proposal.buying", null, locale)};
            String message = messageSource.getMessage("message.save.success", args, locale);

            model.addAttribute("message", message);
        }
        return res;
    }

    /**
     * @param model
     * @return
     */
    @RequestMapping(value = "/rent", method = RequestMethod.GET, produces = "text/html")
    public String rentSearchAction(ModelMap model) {

        ProposalSearch proposalSearch = proposalSearchService.findByProfileAndProposalType(getProfileDetails(), EnumLabel.proposalType.LabelRentingProposal.toString());
        if (proposalSearch == null) {
            proposalSearch = new ProposalSearch();
        }
        model.addAttribute("province", provinceService.findByCountry(proposalSearch.getCountry()));
        model.addAttribute("city", cityService.findByProvince(proposalSearch.getProvince()));
        model.addAttribute("proposalSearch", proposalSearch);

        return "profilesearch/searchrentingproposal";
    }

    /**
     * @param request
     * @param locale
     * @return
     */
    @RequestMapping(value = "/rent", method = RequestMethod.POST)
    public
    @ResponseBody
    ValidationResponse rentSearchAction(@Valid @ModelAttribute("proposalSearch") ProposalSearch proposalSearch, BindingResult bindingResult, ModelMap model, HttpServletRequest request, Locale locale) {
        ValidationResponse res = new ValidationResponse();
        rentingProposalSearchValidator.validate(proposalSearch, bindingResult);
        //validation handling
        if (bindingResult.hasErrors()) {
            res.setStatus(ResponseAction.responseAction.FAIL.toString());
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
            ArrayList<Object> argsFor = new ArrayList<Object>();

            for (FieldError objectError : allErrors) {
                try {
                    for (Object d : objectError.getArguments()) {
                        argsFor.add(messageSource.getMessage(d.toString(), null, locale));
                    }
                    errorMesages.add(new ErrorMessage("rent_" + objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
                } catch (Exception e) {
                    errorMesages.add(new ErrorMessage("rent_" + objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
                }

            }
            res.setErrorMessageList(errorMesages);

        } else {
            res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
            res.setAction(ResponseAction.responseAction.INVOKE.toString());
            res.setTarget(ResponseAction.functionToInvoke.callBackCreateCriteria.toString());
            String[] arguments = {modalId.TargetModalRent.toString(), EnumLabel.idJsLabel.Rent.toString()};
            res.setArguments(arguments);

            proposalSearch.setProfile(getProfileDetails());
            proposalSearch.setProposalType(EnumLabel.proposalType.LabelRentingProposal.toString());
            proposalSearchService.save(proposalSearch);

            ProfileSetting profileSetting = profileSettingService.findByProfile(getProfileDetails());
            profileSetting.setShowSearchCriteriaRentingProposalCompleted(true);
            profileSettingService.save(profileSetting);


            String[] args = {messageSource.getMessage("label.property", null, locale)};
            String message = messageSource.getMessage("message.save.success", args, locale);

            model.addAttribute("message", message);
        }
        return res;
    }

    /**
     * @return
     */
    @RequestMapping(value = "/property/delete", method = RequestMethod.POST)
    public
    @ResponseBody
    ValidationResponse deletePropertySearchCriteria() {
        ValidationResponse res = new ValidationResponse();
        PropertySearch propertySearch = propertySearchService.findByProfile(getProfileDetails());
        ProfileSetting profileSetting = propertySearch.getProfile().getProfileSetting();
        profileSetting.setShowSearchCriteriaPropertyCompleted(false);
        profileSettingService.save(profileSetting);

        propertySearchService.delete(propertySearch);
        res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
        res.setTarget(ResponseAction.functionToInvoke.callBackRemoveCriteria.toString());
        String[] arguments = {ResponseAction.responseAction.SUCCESS.toString(), EnumLabel.idJsLabel.Property.toString()};
        res.setArguments(arguments);

        return res;
    }

    /**
     * @return
     */
    @RequestMapping(value = "/rent/delete", method = RequestMethod.POST)
    public @ResponseBody   ValidationResponse deleteRentingProposalSearchCriteria() {
        ValidationResponse res = new ValidationResponse();
        String proposalType = EnumLabel.proposalType.LabelRentingProposal.toString();
        ProposalSearch proposalSearch = proposalSearchService.findByProfileAndProposalType(getProfileDetails(), proposalType);
        ProfileSetting profileSetting = proposalSearch.getProfile().getProfileSetting();
        profileSetting.setShowSearchCriteriaRentingProposalCompleted(false);

        proposalSearchService.delete(proposalSearch);
        res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
        res.setTarget(ResponseAction.functionToInvoke.callBackRemoveCriteria.toString());
        String[] arguments = {ResponseAction.responseAction.SUCCESS.toString(), EnumLabel.idJsLabel.Rent.toString()};
        res.setArguments(arguments);
        return res;
    }

    /**
     * @return
     */
    @RequestMapping(value = "/buy/delete", method = RequestMethod.POST)
    public
    @ResponseBody
    ValidationResponse deleteBuyingProposalSearchCriteria() {
        ValidationResponse res = new ValidationResponse();
        String proposalType = EnumLabel.proposalType.LabelBuyingProposal.toString();
        ProposalSearch proposalSearch = proposalSearchService.findByProfileAndProposalType(getProfileDetails(), proposalType);
        ProfileSetting profileSetting = proposalSearch.getProfile().getProfileSetting();
        profileSetting.setShowSearchCriteriaBuyingProposalCompleted(false);
        profileSettingService.save(profileSetting);

        proposalSearchService.delete(proposalSearch);
        res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
        res.setTarget(ResponseAction.functionToInvoke.callBackRemoveCriteria.toString());
        String[] arguments = {ResponseAction.responseAction.SUCCESS.toString(), EnumLabel.idJsLabel.Buy.toString()};
        res.setArguments(arguments);

        return res;
    }

}
