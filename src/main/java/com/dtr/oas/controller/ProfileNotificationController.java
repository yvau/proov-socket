package com.dtr.oas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileNotificationService;
import com.dtr.oas.service.ProfileServiceDetails;

@Controller
@PreAuthorize("hasRole('ROLE_BUYER')")
@RequestMapping(value="/profile/notification")
public class ProfileNotificationController {
	
	@Autowired
	ProfileNotificationService profileNotificationService;
	
	@Autowired
	ProfileServiceDetails profileServiceDetails;
	
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;
	
	/**
     * @return
     */
    public Profile getProfileDetails() {
        Profile profile = profileServiceDetails.getProfileDetails();
        return profile;
    }
    
    /**
     * @return
     */
    /*@RequestMapping(method = RequestMethod.GET)
    public String newForm() {
    	List<ProfileNotification> profileNotification = profileNotificationService.findByProfile(getProfileDetails());
    	System.out.println(profileNotification.stream().collect(Collectors.groupingBy(ProfileNotification::getNotificationVerb)));
         return "default/partners";
    }*/
}
