package com.dtr.oas.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.dtr.oas.model.Profile;
import com.dtr.oas.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.form.ProposalForm;
import com.dtr.oas.model.Country;
import com.dtr.oas.model.Proposal;
import com.dtr.oas.model.ViewProposal;
import com.dtr.oas.pagewrapper.PageWrapper;

/**
 * @author MOBIOT
 */
@Controller
public class ProposalController {

  @Autowired
  private ProposalService proposalService;

  @Autowired
  private CountryService countryService;

  @Autowired
  private AppService appService;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private ViewProposalService viewProposalService;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * @return
   */
  @ModelAttribute("country")
  public
  @ResponseBody
  List<Country> getCountryList() {
    List<Country> countryList = countryService.findAll();
    return countryList;
  }

  @ModelAttribute("propertyTypeOfProperty")
  public List<EnumLabel.propertyTypeOfProperty> getPropertyTypeOfPropertyList() {
    List<EnumLabel.propertyTypeOfProperty> propertyTypeOfProperty = new ArrayList<EnumLabel.propertyTypeOfProperty>(Arrays.asList(EnumLabel.propertyTypeOfProperty.values()));
    return propertyTypeOfProperty;
  }


  /**
   * @param model
   * @param request
   * @return
   */
  @RequestMapping(value = "proposal/all", produces = "text/html")
  public String homeAction(ModelMap model, ProposalForm searchProposal, HttpServletRequest request) {
    model.addAttribute("title", "Proov :: proposal");
    Pageable pageable = new PageRequest(0, 10);
    model.addAttribute("list", proposalService.findAll(searchProposal, pageable));

    return "proposal/smalllist";
  }

  /**
   * @param model
   * @param page
   * @param request
   * @return
   */
  @RequestMapping(value = {"buy", "buy/"}, produces = "text/html")
  public String listBuyingProposalAction(ModelMap model, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "sorting", required = false) String sorting, ProposalForm searchForm, HttpServletRequest request) {
    model.addAttribute("title", "Proov :: Buying proposal");
    ProposalForm searchProposalForm = new ProposalForm();
    model.addAttribute("searchForm", searchProposalForm);

    Pageable pageable = appService.pageable(page, sorting);
    searchForm.setProposalType(EnumLabel.proposalType.LabelBuyingProposal.toString());
    PageWrapper<Proposal> pageIn = new PageWrapper<Proposal>(proposalService.findAll(searchForm, pageable), "/buy/" + appService.removeQueryStringParameter(request.getQueryString()));
    model.addAttribute("page", pageIn);
    model.addAttribute("proposalType", EnumLabel.proposalType.LabelBuyingProposal.toString());

    return "proposal/list";
  }

  /**
   * @param model
   * @param page
   * @param request
   * @return
   */
  @RequestMapping(value = {"rent", "rent/"}, produces = "text/html")
  public String listRentingProposalAction(ModelMap model, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "sorting", required = false) String sorting, ProposalForm searchForm, HttpServletRequest request) {
    model.addAttribute("title", "Proov :: Renting proposal");
    ProposalForm searchProposalForm = new ProposalForm();
    model.addAttribute("searchForm", searchProposalForm);

    Pageable pageable = appService.pageable(page, sorting);
    searchForm.setProposalType(EnumLabel.proposalType.LabelRentingProposal.toString());
    PageWrapper<Proposal> pageIn = new PageWrapper<Proposal>(proposalService.findAll(searchForm, pageable), "/rent/" + appService.removeQueryStringParameter(request.getQueryString()));
    model.addAttribute("page", pageIn);

    model.addAttribute("proposalType", EnumLabel.proposalType.LabelRentingProposal.toString());

    return "proposal/list";
  }

  /**
   * @param model
   * @param id
   * @return
   * @throws NoHandlerFoundException
   */
  @RequestMapping(value = {"proposal/{id}"}, method = RequestMethod.GET, produces = "text/html")
  public String showProposalAction(ModelMap model, @PathVariable("id") Long id, HttpServletRequest request) throws NoHandlerFoundException {
    Proposal proposal = proposalService.find(id);
    if (proposal == null) throw new NoHandlerFoundException(null, null, null);
    model.addAttribute("title", "Proov :: " + proposal.getProposalType() + " - " + proposal.getId());
    setProposalViewAction(proposal, request);
    model.addAttribute("proposal", proposal);
    model.addAttribute("proposalType", proposal.getProposalType());
    return "proposal/show";
  }

  @Async
  private void setProposalViewAction(Proposal proposal, HttpServletRequest request) {

    String referrer = request.getHeader("Referer");
    if (referrer != null) {

      String ipNameView = request.getRemoteAddr();
      ViewProposal viewProposal = viewProposalService.findByIp(ipNameView);
      if (viewProposal != null) {
        int newCount = viewProposal.getIpCountView() + 1;
        viewProposal.setIpCountView(newCount);
        viewProposalService.save(viewProposal);

      } else {
        viewProposal = new ViewProposal();
        viewProposal.setIpCountView(1);
        viewProposal.setIpNameView(ipNameView);
        viewProposal.setProposal(proposal);
        viewProposalService.save(viewProposal);
      }

      int numberOfViews = proposal.getNumberOfViews() + 1;
      proposal.setNumberOfViews(numberOfViews);
      proposalService.save(proposal);
    }

  }

}
