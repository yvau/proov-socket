package com.dtr.oas.controller;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.dtr.oas.model.*;
import com.dtr.oas.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.converter.SimpleMessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.enumeration.ResponseStatus;
import com.dtr.oas.form.BindingForm;
import com.dtr.oas.form.PropertyForm;
import com.dtr.oas.pagewrapper.PageWrapper;
import com.dtr.oas.validators.ErrorMessage;
import com.dtr.oas.validators.PropertyValidator;
import com.dtr.oas.validators.ValidationResponse;


/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("hasRole('ROLE_SELLER')")
@RequestMapping(value = "/profile/property")
@SessionAttributes("message")
public class ProfilePropertyController {
  private Instant timestamp = Instant.now();

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private CountryService countryService;

  @Autowired
  private ProvinceService provinceService;

  @Autowired
  private CityService cityService;

  @Autowired
  ProfileSettingService profileSettingService;

  @Autowired
  private ProfileNotificationService profileNotificationService;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private ProfileService profileService;

  @Autowired
  private ProposalService proposalService;

  @Autowired
  private BindingService bindingService;

  @Autowired
  SimpMessagingTemplate simpMessagingTemplate;

  @Autowired
  private PropertyValidator propertyValidator;

  @Autowired
  private AppService appService;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }


  /**
   * @return
   */
  @ModelAttribute("message")
  public String getMessage() {
    String message = "null";
    return message;
  }

  /**
   * @return
   */
  @ModelAttribute("country")
  public
  @ResponseBody
  List<Country> getCountryList() {
    List<Country> countryList = countryService.findAll();
    return countryList;
  }

  /**
   * @return
   */
  @ModelAttribute("featuresRent")
  public List<EnumLabel.featuresRent> getFeaturesRentList() {
    List<EnumLabel.featuresRent> featuresRent = new ArrayList<EnumLabel.featuresRent>(Arrays.asList(EnumLabel.featuresRent.values()));
    return featuresRent;
  }

  /**
   * @return
   */
  @ModelAttribute("propertyTypeOfProperty")
  public List<EnumLabel.propertyTypeOfProperty> getPropertyTypeOfPropertyList() {
    List<EnumLabel.propertyTypeOfProperty> propertyTypeOfProperty = new ArrayList<EnumLabel.propertyTypeOfProperty>(Arrays.asList(EnumLabel.propertyTypeOfProperty.values()));
    return propertyTypeOfProperty;
  }

  /**
   *
   * @return
   */
  @ModelAttribute("propertySaleType")
  public List<EnumLabel.propertySaleType> getPropertySaleTypeList() {
    List<EnumLabel.propertySaleType> propertySaleType = new ArrayList<EnumLabel.propertySaleType>(Arrays.asList(EnumLabel.propertySaleType.values()));
    return propertySaleType;
  }


  /**
   * @param model
   * @param page
   * @return
   */
  @RequestMapping(value = {"/all"}, method = RequestMethod.GET, produces = "text/html")
  public String homeAction(ModelMap model, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "sorting", required = false) String sorting, @RequestParam(value = "proposal_id", required = false) Integer proposal_id, HttpServletRequest request) {
    Boolean ajaxrequest = false;
    Pageable pageable = appService.pageable(page, sorting);
    PageWrapper<Property> list = new PageWrapper<Property>(propertyService.findByProfile(getProfileDetails(), pageable), "/profile/property/all" + appService.removeQueryStringParameter(request.getQueryString()));
    model.addAttribute("list", list);
    if (appService.isAjaxRequest(request)) {
      ajaxrequest = true;
    }
    //model.addAttribute("binding", new BindingForm());

    model.addAttribute("ajaxrequest", ajaxrequest);
    return "profileproperty/home";
  }

  /**
   * @param model
   * @param sessionStatus
   * @return
   */
  @RequestMapping(value = {"/new"}, method = RequestMethod.GET, produces = "text/html")
  public String newForm(ModelMap model, @RequestParam(value = "proposal_id", required = false) Long proposal_id, HttpServletRequest request, SessionStatus sessionStatus, HttpServletRequest servrequest) {
    sessionStatus.setComplete();
    model.addAttribute("proposal", proposalService.find(proposal_id));
    PropertyForm propertyForm = new PropertyForm();
    model.addAttribute("property", propertyForm);

    return "profileproperty/new";
  }

  /**
   * @param model
   * @param id
   * @param request
   * @param sessionStatus
   * @return
   * @throws NoHandlerFoundException
   */
  @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.GET, produces = "text/html")
  public String updateForm(ModelMap model, @PathVariable("id") Long id, HttpServletRequest request, SessionStatus sessionStatus) throws NoHandlerFoundException {
    sessionStatus.setComplete();

    Property property = propertyService.findByProfileAndId(getProfileDetails(), id);
    if (property == null) throw new NoHandlerFoundException(null, null, null);

    PropertyForm propertyForm = new PropertyForm();
    propertyForm.setLocation(property.getLocation());
    propertyForm.setZipCode(property.getLocation().getLocationZipCode());
    propertyForm.setAddress(property.getLocation().getLocationString());
    propertyForm.setType(property.getPropertyType());
    propertyForm.setSaleType(property.getPropertySaleType());
    propertyForm.setPropertyPrice(String.valueOf(property.getPropertyPrice()));
    propertyForm.setPropertyAge(property.getPropertyAge());
    propertyForm.setBedrooms(property.getPropertyNumOfBedrooms());
    propertyForm.setBathrooms(property.getPropertyNumOfBathrooms());
    propertyForm.setSqfeet(property.getPropertySqFeet());
    propertyForm.setFloorLevel(property.getPropertyNumOfFloors());
    propertyForm.setPropertyDescription(property.getPropertyDescription());
    propertyForm.setPropertyFeatures(property.getPropertyFeatures());

    model.addAttribute("property", propertyForm);
    model.addAttribute("province", provinceService.findByCountry(property.getLocation().getCountry()));
    model.addAttribute("city", cityService.findByProvince(property.getLocation().getProvince()));
    return "profileproperty/update";
  }

  //enlever les save location bcp bcp inutile

  /**
   * @param bindingResult
   * @param model
   * @param locale
   * @return
   */
  @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
  public @ResponseBody ValidationResponse updateAction(@Valid @ModelAttribute("property") PropertyForm propertyForm, @PathVariable("id") Long id, HttpServletRequest request, BindingResult bindingResult, ModelMap model, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    propertyValidator.validate(propertyForm, bindingResult);
    if (bindingResult.hasErrors()) {
      res.setStatus("FAIL");
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      for (FieldError objectError : allErrors) {
        errorMesages.add(new ErrorMessage(objectError.getField(), objectError.getDefaultMessage()));
      }
      res.setErrorMessageList(errorMesages);
    } else {
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      //save location details
      Profile profile = getProfileDetails();
      Property property = propertyService.find(id);
      Location location = property.getLocation();
      Country country = countryService.find(propertyForm.getLocation().getCountry().getId());
      Province province = provinceService.find(propertyForm.getLocation().getProvince().getId());
      City city = cityService.find(propertyForm.getLocation().getCity().getId());

      location.setCountry(country);
      location.setProvince(province);
      location.setCity(city);
      location.setLocationZipCode(propertyForm.getZipCode());
      location.setLocationString(propertyForm.getAddress());

      property.setNumberOfViews(0);
      property.setPropertyAge(propertyForm.getPropertyAge());
      property.setPropertyDescription(propertyForm.getPropertyDescription());
      property.setPropertyFeatures(propertyForm.getPropertyFeatures());
      property.setPropertyNumOfBathrooms(propertyForm.getBathrooms());
      property.setPropertyNumOfBedrooms(propertyForm.getBedrooms());
      property.setPropertyNumOfFloors(propertyForm.getFloorLevel());
      property.setPropertyPrice(Double.parseDouble(propertyForm.getPropertyPrice()));
      property.setPropertySaleType(propertyForm.getSaleType());
      property.setPropertySaveDate(Timestamp.from(timestamp));
      property.setPropertySqFeet(propertyForm.getSqfeet());
      property.setPropertyType(propertyForm.getType());
      property.setProfile(profile);
      property.setLocation(location);
      propertyService.save(property);

      res.setTarget(request.getRequestURI());

      String[] args = {messageSource.getMessage("label.property", null, locale)};
      String message = messageSource.getMessage("message.save.success", args, locale);

      model.addAttribute("message", message);
    }


    return res;
  }

  /**
   * @param bindingResult
   * @param model
   * @param locale
   * @return
   */
  @RequestMapping(value = {"/new"}, method = RequestMethod.POST)
  public
  @ResponseBody
  ValidationResponse newAction(@Valid @ModelAttribute("property") PropertyForm propertyForm, HttpServletRequest request, BindingResult bindingResult, ModelMap model, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    propertyValidator.validate(propertyForm, bindingResult);
    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {

      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      Property property = new Property();
      Country country = countryService.find(propertyForm.getLocation().getCountry().getId());
      Province province = provinceService.find(propertyForm.getLocation().getProvince().getId());
      City city = cityService.find(propertyForm.getLocation().getCity().getId());
      Location location = new Location();
      location.setId(appService.getRandomString(12));
      location.setCountry(country);
      location.setProvince(province);
      location.setCity(city);
      location.setLocationZipCode(propertyForm.getZipCode());
      location.setLocationString(propertyForm.getAddress());

      Profile profile = getProfileDetails();
      property.setNumberOfViews(0);
      property.setPropertyAge(propertyForm.getPropertyAge());
      property.setPropertyDescription(propertyForm.getPropertyDescription());
      property.setPropertyFeatures(propertyForm.getPropertyFeatures());
      property.setPropertyNumOfBathrooms(propertyForm.getBathrooms());
      property.setPropertyNumOfBedrooms(propertyForm.getBedrooms());
      property.setPropertyNumOfFloors(propertyForm.getFloorLevel());
      property.setPropertyPrice(Double.parseDouble(propertyForm.getPropertyPrice()));
      property.setPropertySaleType(propertyForm.getSaleType());
      property.setPropertySaveDate(Timestamp.from(timestamp));
      property.setPropertyBidRequest(false);
      property.setPropertySqFeet(propertyForm.getSqfeet());
      property.setPropertyType(propertyForm.getType());
      property.setProfile(profile);
      property.setLocation(location);
      propertyService.save(property);

      //a effacer c pour test
      //Long profileHasNotificationCount = profileHasNotificationService.countByStatusAndProfile1("unread", getProfileDetails());

      //
      res.setTarget(request.getContextPath() + "/profile/property/" + property.getId());

      //enregistre binding si le id n'est pas null et s'il existe en base de donne dans
      // less proposals
      if (request.getParameter("proposal_id") != null) {
        Long proposal_id = Long.parseLong(request.getParameter("proposal_id"));
        //updateOwningProposal(owningProposal);
        Proposal proposal = proposalService.find(proposal_id);
        Binding binding = new Binding();
        binding.setProfile(proposal.getProfile());
        binding.setProperty(property);
        binding.setProposal(proposal);
        binding.setDateOfBinding(Timestamp.from(timestamp));
        binding.setStatusInterest(ResponseStatus.pending.toString());
        binding.setStatusReading(EnumLabel.statusReading.LabelNotRead.toString());
        bindingService.save(binding);

        ProfileNotification profileNotification = saveNotification(proposal, getProfileDetails());
        sendNotification(profileNotification, locale);

        ProfileSetting profileSetting = proposal.getProfile().getProfileSetting();
        if(proposal.getProposalType().equals("proposal.buying")) {
          profileSetting.setShowNotificationBuyingProposal(true);
        }else {
          profileSetting.setShowNotificationRentingProposal(true);
        }
        profileSettingService.save(profileSetting);

      }


      String[] args = {messageSource.getMessage("label.property", null, locale)};
      String message = messageSource.getMessage("message.save.success", args, locale);

      model.addAttribute("message", message);
    }
    return res;

  }

  /**
   * @param id
   * @param page
   * @param model
   * @return
   */
  @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET, produces = "text/html")
  public String deleteAction(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, ModelMap model) {
    //redesign delete function here for object property not id and method DELETE not GET
    //propertyService.delete(getProfileDetails(), id);
    //pro
    model.addAttribute("page", (page == null) ? "1" : page.toString());
    return "redirect:/profile/property/";
  }

  /**
   *
   * @param profile
   * @return
   */
  private ProfileNotification saveNotification(Proposal proposal, Profile profile) {

    ProfileNotification profileNotification = new ProfileNotification();
    profileNotification.setId(appService.getRandomString(12));
    profileNotification.setNotificationStatus(EnumLabel.statusReading.LabelNotRead.toString());
    profileNotification.setNotificationObject(proposal.getProposalType());
    profileNotification.setNotificationTarget(String.valueOf(proposal.getId()));
    profileNotification.setNotificationDate(Timestamp.from(timestamp));

    profileNotification.setProfile1(proposal.getProfile());
    profileNotification.setProfile2(profile);

    profileNotificationService.save(profileNotification);
    return profileNotification;
  }

  /**
   * @param locale
   * @return
   */
  private void sendNotification(ProfileNotification profileNotification, Locale locale) {
    Profile profile =  profileNotification.getProfile2();
    int notificationCount = profile.getNotificationCount() == null ? 0 : Integer.parseInt(profile.getNotificationCount());
    int newNotificationCount = notificationCount + 1;

    profile.setNotificationCount(String.valueOf(newNotificationCount));
    profileService.save(profile, null, null);
    String[] args = {messageSource.getMessage("label." + profileNotification.getNotificationObject(), null, locale)};
    String notification = messageSource.getMessage("label.proposal.notification.message", args, locale);
    String theProposal = profileNotification.getNotificationObject().equals(EnumLabel.proposalType.LabelBuyingProposal.toString()) ? "buying_proposal" : "renting_proposal";
    String message = "{\"notifier\":\" "+ notification + "\", \"css\":\"" + theProposal + "\"}";

    simpMessagingTemplate.convertAndSendToUser(profileNotification.getProfile1().getEmail(), "/queue/note", message);
    //emailService.sendEmail(profile.getEmail(), "mobiot.fabrice@gmail.com", "Proov :: Request for visiting a property", calendar, "schedule-request.vm");
    return;
  }

}
