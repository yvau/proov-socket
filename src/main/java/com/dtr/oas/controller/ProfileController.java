package com.dtr.oas.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.dtr.oas.form.BindingForm;
import com.dtr.oas.model.*;
import com.dtr.oas.pagewrapper.PageWrapper;
import com.dtr.oas.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.form.PropertyForm;
import com.dtr.oas.form.ProposalForm;

import javax.servlet.http.HttpServletRequest;

/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_SELLER', 'ROLE_BUYER', 'ROLE_NOACCESS')")
@RequestMapping(value = "/profile")
public class ProfileController {

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private ProposalSearchService proposalSearchService;

  @Autowired
  private ProposalService proposalService;

  @Autowired
  private PropertySearchService propertySearchService;

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private CountryService countryService;

  @Autowired
  private AppService appService;

  /**
   * @return
   */
  @ModelAttribute("country")
  public
  @ResponseBody
  List<Country> getCountryList() {
    List<Country> countryList = countryService.findAll();
    return countryList;
  }


  @ModelAttribute("propertyType")
  public List<EnumLabel.propertyType> getPropertyTypeList() {
    List<EnumLabel.propertyType> propertyType = new ArrayList<EnumLabel.propertyType>(Arrays.asList(EnumLabel.propertyType.values()));
    return propertyType;
  }

  @ModelAttribute("propertyTypeOfProperty")
  public List<EnumLabel.propertyTypeOfProperty> getPropertyTypeOfPropertyList() {
    List<EnumLabel.propertyTypeOfProperty> propertyTypeOfProperty = new ArrayList<EnumLabel.propertyTypeOfProperty>(Arrays.asList(EnumLabel.propertyTypeOfProperty.values()));
    return propertyTypeOfProperty;
  }

  @ModelAttribute("propertySaleType")
  public List<EnumLabel.propertySaleType> getPropertySaleTypeList() {
    List<EnumLabel.propertySaleType> propertySaleType = new ArrayList<EnumLabel.propertySaleType>(Arrays.asList(EnumLabel.propertySaleType.values()));
    return propertySaleType;
  }

  @ModelAttribute("labelYesNo")
  public List<EnumLabel.yesNoLabel> getyesNoLabelList() {
    List<EnumLabel.yesNoLabel> yesNoLabels = new ArrayList<EnumLabel.yesNoLabel>(Arrays.asList(EnumLabel.yesNoLabel.values()));
    return yesNoLabels;
  }

  @ModelAttribute("featuresRent")
  public List<EnumLabel.featuresRent> getFeaturesRentList() {
    List<EnumLabel.featuresRent> featuresRent = new ArrayList<EnumLabel.featuresRent>(Arrays.asList(EnumLabel.featuresRent.values()));
    return featuresRent;
  }

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * @return
   */
  @ModelAttribute("binding")
  public BindingForm bindingForm() {
      BindingForm binding = new BindingForm();
      return binding;
  }
  
  /**
	 * @return
	 */
  @RequestMapping(value = "/list-property.json", method = RequestMethod.GET)
	public @ResponseBody List<Property> getListProperties(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "sorting", required = false) String sorting, HttpServletRequest request) {
    Pageable pageable = appService.pageable(page, sorting);
    List<Property> list = new PageWrapper<Property>(propertyService.findByProfile(getProfileDetails(), pageable), "/profile/property/all" + appService.removeQueryStringParameter(request.getQueryString())).getContent();
		
		return list;
	} 


  /**
   * @return
   */
  @RequestMapping(value = "/property_search/", method = RequestMethod.GET)
  public String getPropertiesFromUserCriteria(@RequestParam(value = "page", required = false) Integer page, ModelMap model) {
    page = page == null ? 0 : (page.intValue() - 1);
    Pageable pageable = new PageRequest(page, EnumLabel.labelNumber.LabelPageMax.getNumber());
    try {
      PropertySearch propertySearch = propertySearchService.findByProfile(getProfileDetails());
      Page<Property> property = propertyService.findAll(getPropertySearch(propertySearch), pageable);
      model.addAttribute("propertySearch", property.getContent());
    } catch (Exception e) {
      // TODO: handle exception
      List<Property> property = new ArrayList<>();
      model.addAttribute("propertySearch", property);
    }


    return "profile/listpropertysearch";
  }

  /**
   * @return
   */
  @RequestMapping(value = "/rent_search/", method = RequestMethod.GET)
  public String getRentingProposalFromUserCriteria(@RequestParam(value = "page", required = false) Integer page, ModelMap model) {
    page = page == null ? 0 : (page.intValue() - 1);
    Pageable pageable = new PageRequest(page, EnumLabel.labelNumber.LabelPageMax.getNumber());
    try {
      ProposalSearch proposalSearch = proposalSearchService.findByProfileAndProposalType(getProfileDetails(), EnumLabel.proposalType.LabelRentingProposal.toString());
      Page<Proposal> proposal = proposalService.findAll(getRentingProposalSearch(proposalSearch), pageable);
      model.addAttribute("proposalSearch", proposal.getContent());
    } catch (Exception e) {
      // TODO: handle exception
      List<Proposal> proposal = new ArrayList<>();
      model.addAttribute("proposalSearch", proposal);
    }


    return "profile/listproposalsearch";
  }

  /**
   * @return
   */
  @RequestMapping(value = "/buy_search/", method = RequestMethod.GET)
  public String getBuyingProposalFromUserCriteria(@RequestParam(value = "page", required = false) Integer page, ModelMap model) {
    page = page == null ? 0 : (page.intValue() - 1);
    Pageable pageable = new PageRequest(page, EnumLabel.labelNumber.LabelPageMax.getNumber());
    try {
      ProposalSearch proposalSearch = proposalSearchService.findByProfileAndProposalType(getProfileDetails(), EnumLabel.proposalType.LabelBuyingProposal.toString());
      Page<Proposal> proposal = proposalService.findAll(getBuyingProposalSearch(proposalSearch), pageable);
      model.addAttribute("proposalSearch", proposal.getContent());

    } catch (Exception e) {
      // TODO: handle exception
      List<Proposal> proposal = new ArrayList<>();
      model.addAttribute("proposalSearch", proposal);
    }

    return "profile/listproposalsearch";
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(method = RequestMethod.GET, produces = "text/html")
  public String homeAction(ModelMap model) {
    PropertySearch propertySearch = new PropertySearch();
    ProposalSearch proposalSearch = new ProposalSearch();
    model.addAttribute("title", "Proov :: Profile home page");
    model.addAttribute("proposalSearch", proposalSearch);
    model.addAttribute("propertySearch", propertySearch);
    return "profile/home";
  }


  /**
   * @return
   */
  private ProposalForm getRentingProposalSearch(ProposalSearch proposal) {
    ProposalForm searchProposalForm = new ProposalForm();
    searchProposalForm.setCity(proposal.getCity());
    searchProposalForm.setCountry(proposal.getCountry());
    searchProposalForm.setProvince(proposal.getProvince());
    searchProposalForm.setProposalType(EnumLabel.proposalType.LabelRentingProposal.toString());
    searchProposalForm.setEmail(getProfileDetails().getEmail());

    return searchProposalForm;
  }

  /**
   * @return
   */
  private ProposalForm getBuyingProposalSearch(ProposalSearch proposal) {
    ProposalForm searchProposalForm = new ProposalForm();
    searchProposalForm.setCity(proposal.getCity());
    searchProposalForm.setCountry(proposal.getCountry());
    searchProposalForm.setProvince(proposal.getProvince());
    searchProposalForm.setProposalType(EnumLabel.proposalType.LabelBuyingProposal.toString());
    searchProposalForm.setEmail(getProfileDetails().getEmail());

    return searchProposalForm;
  }

  /**
   * @return
   */
  private PropertyForm getPropertySearch(PropertySearch propertySearch) {
    PropertyForm searchPropertyForm = new PropertyForm();
        /*searchPropertyForm.setBathrooms(propertySearch.getPropertyNumOfBathrooms());*/
    //searchPropertyForm.setBedrooms(propertySearch.getPropertyNumOfBedrooms());
    searchPropertyForm.setLocationcity(propertySearch.getCity());
    searchPropertyForm.setLocationcountry(propertySearch.getCountry());
    //searchPropertyForm.setFloorLevel(propertySearch.getPropertyNumOfFloors());
    //searchPropertyForm.setMinsqfeet(propertySearch.getPropertySqFeet());
	    	/*searchPropertyForm.setPriceMax(propertySearch.getPropertyPriceMax());
	    	searchPropertyForm.setPriceMin(propertySearch.getPropertyPriceMin());*/
    searchPropertyForm.setLocationprovince(propertySearch.getProvince());
    searchPropertyForm.setEmail(getProfileDetails().getEmail());
    //searchPropertyForm.setSaleType(propertySearch.getPropertySaleType());
    //searchPropertyForm.setType(propertySearch.getPropertyType());
    return searchPropertyForm;
  }

}
