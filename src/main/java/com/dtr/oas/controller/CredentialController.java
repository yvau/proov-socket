package com.dtr.oas.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.form.EmailPasswordForm;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileSetting;
import com.dtr.oas.service.AppService;
import com.dtr.oas.service.ProfileService;
import com.dtr.oas.service.ProfileServiceDetails;
import com.dtr.oas.service.ProfileSettingService;
import com.dtr.oas.validators.ErrorMessage;
import com.dtr.oas.validators.RecoverValidator;
import com.dtr.oas.validators.RegisterValidator;
import com.dtr.oas.validators.ResetPasswordValidator;
import com.dtr.oas.validators.ValidationResponse;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.ui.ModelMap;

/**
 * Specifies controller used to handle methods related to security
 * (login,register,recover,change password...)
 *
 * @author MOBIOT
 */
@Controller
public class CredentialController {

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private ProfileService profileService;

  @Autowired
  private RegisterValidator registerValidator;

  @Autowired
  private RecoverValidator recoverValidator;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private ResetPasswordValidator resetPasswordValidator;

  @Autowired
  private ProfileSettingService profileSettingService;

  @Autowired
  private AppService appService;

  @Autowired
  ProfileServiceDetails profileServiceDetails;

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try{
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    }
    catch(Exception e){
      return null;
    }
  }

  /**
   * This method handle login display form
   *
   * @param request
   * @param model
   * @return login.html form to handle the log of user
   * if the user is already logged it redirect to :/
   */
  @RequestMapping(value = "/login", method = RequestMethod.GET, produces = "text/html")
  public String signInAction(@RequestParam(value = "er", required = false) String er, HttpServletRequest request, RedirectAttributes model) {
    String referrer = request.getHeader("Referer");
    request.getSession().setAttribute("url_prior_login", referrer);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (!(auth instanceof AnonymousAuthenticationToken)) {
      return "redirect:/profile/";
    }
    model.addAttribute("title", "Proov :: Login page");
    return "credential/login";
  }

  /**
   * This method handle registration handling GET Side
   * @param model
   * @return registration form to handle registration of user for
   * any roles(ROLE_SELLER,ROLE_BUYER)
   */
  @RequestMapping(value = {"/register", "/register/success"}, method = RequestMethod.GET, produces = "text/html")
  public String signUpForm(@RequestParam(value = "email", required = false) String email, ModelMap model) {
    model.addAttribute("title", "Proov :: Register page");
    model.addAttribute("account", new Profile());
    return "credential/register";
  }

  /**
   * This method handle registration handling POST Side
   * @param account
   * @param bindingResult
   * @param model
   * @param request
   * @return save user profile information to the database, send email
   * link for confirmation
   */
  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public @ResponseBody ValidationResponse signUpAction(@Valid @ModelAttribute("account") Profile account, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response, Locale locale){
    ValidationResponse res = new ValidationResponse();
    registerValidator.validate(account, bindingResult);
    //validation handling
    if (bindingResult.hasErrors() ){

      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
            errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
          }
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      res.setTarget(request.getContextPath() + "/register/success?email=" + account.getEmail());

      //process data related to date of registration of the user(current datetime)
      Date dateTime = new Date();
      account.setCreateDate(new Timestamp(dateTime.getTime()));

      //process data related to the token
      StandardPasswordEncoder encoder = new StandardPasswordEncoder();
      String token = encoder.encode(request.getParameter("username") + request.getParameter("password") + dateTime);
      account.setToken(token);

      //process data related to the token
      String passwordHash = passwordEncoder.encode(request.getParameter("password"));
      account.setPassword(passwordHash);

      //process data related to the ip address
      String ipAddress = request.getRemoteAddr();
      account.setIpAddress(ipAddress);
      account.setRole("ROLE_NOACCESS");

      //process other data
      account.setAccountNonExpired(true);
      account.setAccountNonLocked(true);
      account.setCredentialsNonExpired(true);
      account.setEnabled(false);

      //persist the data
      profileService.save(account, EnumLabel.emailType.AppProfileActivation.toString(), locale);

    }
    return res;
  }

  /**
   * This method handle activation user handling GET Side
   *
   * @param t
   * @param redirectAttributes
   * @return update user token to null, enable to true
   */
  @RequestMapping(value = "/activation", method = RequestMethod.GET, produces = "text/html")
  public String activationLink(ModelMap model, @RequestParam(value = "t", required = true) String t, Locale locale, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
    //assign the object to profile to select the data to found by token
    Profile profile = profileService.findByToken(t);
    //validation handling
    model.addAttribute("title", "Proov :: Activation page");
    if (profileService.findByToken(t) == null) {
      String error = messageSource.getMessage("label.link.expired", null, locale);
      redirectAttributes.addFlashAttribute("error", error);

      return "redirect:/login";
    }
    ProfileSetting profileSetting = new ProfileSetting();
    profileSetting.setId(profile.getId());
    profileSetting.setShowGettingStarted(true);
    profileSetting.setShowInformationCompleted(false);
    profileSetting.setShowPrequalificationCompleted(false);
    profileSetting.setShowSearchCriteriaPropertyCompleted(false);
    profileSetting.setShowSearchCriteriaBuyingProposalCompleted(false);
    profileSetting.setShowSearchCriteriaRentingProposalCompleted(false);
    profileSetting.setShowNotificationBuyingProposal(false);
    profileSetting.setShowNotificationRentingProposal(false);
    profileSetting.setShowNotificationProperty(false);
    profileSetting.setShowNotificationVisit(false);
    profileSetting.setShowNotificationMembership(false);
    profileSetting.setProfile(profile);
    profileSettingService.save(profileSetting);


    //process the other data
    profile.setToken(null);
    profile.setEnabled(true);
    profileService.save(profile, null, locale);
    appService.authorizeUser(profile);
    String message = messageSource.getMessage("label.profile.activated", null, locale);
    redirectAttributes.addFlashAttribute("message", message);
    return "redirect:/profile/settings/role";
  }

  /**
   * This method handle reset to recover password from email handling GET Side
   *
   * @param t
   * @param model
   * @param redirectAttributes
   * @return EmailPasswordForm form
   */
  @RequestMapping(value = "/reset", params = "t", method = RequestMethod.GET, produces = "text/html")
  public String recoverPasswordByToken(@RequestParam(value = "t", required = true) String t, ModelMap model, Locale locale, RedirectAttributes redirectAttributes) {
    model.addAttribute("title", "Proov :: Reset page");
    Profile profile = profileService.findByToken(t);
    if (profile == null) {
      String error = messageSource.getMessage("label.link.expired", null, locale);
      redirectAttributes.addFlashAttribute("error", error);

      return "redirect:/forgot";
    }
    model.addAttribute("emailPasswordForm", new EmailPasswordForm());


    return "credential/resetpassword";
  }

  /**
   * This method handle reset to recover password from email handling PUT Side
   *
   * @param emailPasswordForm
   * @param bindingResult
   * @param model
   * @return update password set token to null
   */
  @RequestMapping(value = "/reset", method = RequestMethod.POST)
  public
  @ResponseBody
  ValidationResponse recoverPasswordByTokenAction(@Valid @ModelAttribute("emailPasswordForm") EmailPasswordForm emailPasswordForm, BindingResult bindingResult, HttpServletRequest request, Locale locale, ModelMap model) {
    ValidationResponse res = new ValidationResponse();
    resetPasswordValidator.validate(emailPasswordForm, bindingResult);

    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      res.setTarget(request.getContextPath() + "/profile/settings");


      Profile profile = profileService.findByToken(emailPasswordForm.getToken());
      String passwordHash = passwordEncoder.encode(emailPasswordForm.getNewPassword());
      profile.setToken(null);
      profile.setPassword(passwordHash);
      profileService.save(profile, null, locale);
      appService.authorizeUser(profile);
    }
    return res;
  }

  /**
   * This method handle recover password  when password is forgotten handling GET Side
   *
   * @param model
   * @return EmailPasswordForm form
   */
  @RequestMapping(value = {"/forgot", "/forgot/success"}, method = RequestMethod.GET, produces = "text/html")
  public String recoverPasswordForm(ModelMap model) {
    model.addAttribute("title", "Proov :: Forgot password");
    model.addAttribute("emailPasswordForm", new EmailPasswordForm());
    return "credential/forgot";
  }

  /**
   * This method handle recover password  when password is forgotten handling POST Side
   *
   * @param bindingResult
   * @param model
   * @param redirectAttributes
   * @return update user set new token link and send email
   */
  @RequestMapping(value = "/forgot", method = RequestMethod.POST)
  public
  @ResponseBody
  ValidationResponse recoverPasswordAction(@Valid @ModelAttribute("emailPasswordForm") EmailPasswordForm emailPasswordForm, BindingResult bindingResult, ModelMap model, Locale locale, HttpServletRequest request, RedirectAttributes redirectAttributes) {

    ValidationResponse res = new ValidationResponse();
    recoverValidator.validate(emailPasswordForm, bindingResult);
    //validation handling
    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
            errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
          }
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.REDIRECT.toString());
      res.setTarget(request.getContextPath() + "/forgot/success?email=" + emailPasswordForm.getEmail());

      Profile profile = profileService.findByEmail(emailPasswordForm.getEmail());

      Date dateTime = new Date();
      ShaPasswordEncoder tokenEncode = new ShaPasswordEncoder();
      String token = tokenEncode.encodePassword(profile.getEmail() + profile.getPassword() + dateTime, emailPasswordForm.getEmail());
      profile.setToken(token);

      profileService.save(profile, EnumLabel.emailType.AppProfileRecover.toString(), locale);

    }
    return res;
  }
}
