package com.dtr.oas.controller; 

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dtr.oas.model.City;
import com.dtr.oas.model.Province;
import com.dtr.oas.service.CityService;
import com.dtr.oas.service.ProvinceService;


/**
 * Specifies controller used to handle methods related to listing cities related to province only 
 * (getJsonCity)
 * @author MOBIOT
 *
 */
@Controller 
@RequestMapping(value="/city")
public class CityController {
	
	@Autowired
	private ProvinceService provinceService;
	
	@Autowired
	private CityService cityService;
    
	/**
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<City> getJsonCity(@RequestParam(value = "id", required = false) String id, ModelMap model) {
		if (id==null){
			return null;
		}
		Province province = provinceService.find(id);
		return cityService.findByProvince(province);
    }

}