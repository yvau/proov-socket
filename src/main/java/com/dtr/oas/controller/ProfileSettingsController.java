package com.dtr.oas.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.ResponseAction;
import com.dtr.oas.enumeration.ResponseAction.functionToInvoke;
import com.dtr.oas.enumeration.ResponseAction.responseAction;
import com.dtr.oas.form.EmailPasswordForm;
import com.dtr.oas.model.Country;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileSetting;
import com.dtr.oas.service.AppService;
import com.dtr.oas.service.CountryService;
import com.dtr.oas.service.LocationHasProfileService;
import com.dtr.oas.service.ProfilePhotoService;
import com.dtr.oas.service.ProfileService;
import com.dtr.oas.service.ProfileServiceDetails;
import com.dtr.oas.service.ProfileSettingService;
import com.dtr.oas.validators.ChangePasswordValidator;
import com.dtr.oas.validators.EditProfileValidator;
import com.dtr.oas.validators.ErrorMessage;
import com.dtr.oas.validators.RoleValidator;
import com.dtr.oas.validators.ValidationResponse;

/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_SELLER','ROLE_NOACCESS', 'ROLE_BUYER')")
@RequestMapping(value = "/profile/settings")
@SessionAttributes("message")
public class ProfileSettingsController {

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private ProfileService profileService;

  @Autowired
  private ProfilePhotoService profilePhotoService;

  @Autowired
  private CountryService countryService;

  @Autowired
  private ProfileSettingService profileSettingService;

  @Autowired
  private ChangePasswordValidator changePasswordValidator;

  @Autowired
  private RoleValidator roleValidator;

  @Autowired
  LocationHasProfileService locationHasProfileService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private EditProfileValidator editProfileValidator;

  @Autowired
  private AppService appService;


  private String fileUploadDirectory = "resources\\images\\uploads\\";

  /**
   * @return
   */
  @ModelAttribute("message")
  public String getMessage() {
    String message = "null";
    return message;
  }


  /**
   * @return
   */
  @ModelAttribute("country")
  public @ResponseBody List<Country> getCountryList() {
    List<Country> countryList = countryService.findAll();
    return countryList;
  }

  @ModelAttribute("roles")
  public List<EnumLabel.role> getRoles() {
    List<EnumLabel.role> roles = new ArrayList<EnumLabel.role>(Arrays.asList(EnumLabel.role.values()));
    return roles;
  }

  /**
   * @return
   */
  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }


  /**
   * @param model
   * @return
   *//*
  @SuppressWarnings("deprecation")
	@RequestMapping(method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> uploadProfilePhotoAction(MultipartHttpServletRequest request, HttpServletResponse response) {
		Iterator<String> itr = request.getFileNames();
        MultipartFile mpf;
        List<ProfilePhoto> list = new LinkedList<>();
        
        while (itr.hasNext()) {
            mpf = request.getFile(itr.next());
            
            String newFilenameBase = UUID.randomUUID().toString();
            String originalFileExtension = mpf.getOriginalFilename().substring(mpf.getOriginalFilename().lastIndexOf("."));
            String newFilename = newFilenameBase + originalFileExtension;
            
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			String pathToGraphsDir = requestAttributes.getRequest().getRealPath("/");
    		
            String storageDirectory = pathToGraphsDir+fileUploadDirectory;
            String contentType = mpf.getContentType();
            Date dateTime= new Date();
            Timestamp datecreated = new Timestamp(dateTime.getTime());
            
            File newFile = new File(storageDirectory + "/" + newFilename);
            if (!newFile.exists())
                newFile.mkdirs();
            try {
                mpf.transferTo(newFile);
                
                BufferedImage thumbnail = Scalr.resize(ImageIO.read(newFile), 290);

                String thumbnailFilename = newFilenameBase + "-thumbnail.png";
                File thumbnailFile = new File(storageDirectory + "/" + thumbnailFilename);
                ImageIO.write(thumbnail, "png", thumbnailFile);
                
                ProfilePhoto profilePhoto = new ProfilePhoto();
                profilePhoto.setProfile(getProfileDetails());
                profilePhoto.setName(mpf.getOriginalFilename());
                profilePhoto.setSize(mpf.getSize());
                profilePhoto.setContentType(contentType);
                profilePhoto.setNewfilename(newFilename);
                profilePhoto.setThumbnailsize(thumbnailFile.length());
                profilePhoto.setThumbnailfilename(thumbnailFilename);
                profilePhoto.setDatecreated(datecreated);
                profilePhoto.setLastupdated(datecreated);
                profilePhoto = profilePhotoService.save(profilePhoto);
                
                
                list.add(profilePhoto);
                
            } catch(IOException e) {
                return null;
            }
            
        }
        
        Map<String, Object> files = new HashMap<>();
        files.put("files", list);
        return files;
    }*/


  /**
   * @param model
   * @param model
   * @return
   */

  @RequestMapping(method = RequestMethod.GET, produces = "text/html")
  public String homeForm(SessionStatus sessionStatus, ModelMap model) {
    sessionStatus.setComplete();
    model.addAttribute("title", "Proov :: profile setting");
    Profile profile = getProfileDetails();
    model.addAttribute("account", profile);
    return "profilesettings/home";
  }

  /**
   * @param profile
   * @param bindingResult
   * @param model
   * @param request
   * @param locale
   * @return
   */

  @RequestMapping(method = RequestMethod.POST)
  public @ResponseBody ValidationResponse editInformationAction(@Valid @ModelAttribute("account") Profile profile, BindingResult bindingResult, ModelMap model, HttpServletRequest request, Locale locale) {
    ValidationResponse res = new ValidationResponse();

    editProfileValidator.validate(profile, bindingResult);
    Profile currentAccount = getProfileDetails();
    ProfileSetting profileSetting = profileSettingService.findByProfile(getProfileDetails());

    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
      res.setAction(ResponseAction.responseAction.REDIRECT.toString());
      res.setTarget(request.getContextPath() + "/profile/settings/");

      currentAccount.setFirstname(profile.getFirstname());
      currentAccount.setLastname(profile.getLastname());
      profileService.save(currentAccount, null, locale);
      profileSetting.setShowInformationCompleted(true);
      profileSettingService.save(profileSetting);

      String[] args = {messageSource.getMessage("label.profile.your", null, locale)};
      String message = messageSource.getMessage("message.update.success", args, locale);
      model.addAttribute("message", message);
    }
    return res;
  }

  /**
   * @return
   */
  @RequestMapping(value = "/getting_started", method = RequestMethod.POST)
  public @ResponseBody ValidationResponse gettingStartedAction() {
    ValidationResponse res = new ValidationResponse();
    try {
      res.setStatus(responseAction.SUCCESS.toString());
      String[] arguments = {};
      res.setArguments(arguments);
      ProfileSetting profileSetting = profileSettingService.findByProfile(getProfileDetails());
      profileSetting.setShowGettingStarted(false);
      profileSettingService.save(profileSetting);
      return res;
    } catch (ClassCastException e) {
      res.setStatus(responseAction.FAIL.toString());
      return res;
    }
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(value = "/password", method = RequestMethod.GET, produces = "text/html")
  public String editPasswordForm(ModelMap model, SessionStatus sessionStatus) {
    sessionStatus.setComplete();
    model.addAttribute("title", "Proov :: profile password setting");
    EmailPasswordForm emailPasswordForm = new EmailPasswordForm();
    model.addAttribute("passwordForm", emailPasswordForm);
    return "profilesettings/password";
  }

  /**
   * @param emailPasswordForm
   * @param bindingResult
   * @param model
   * @param locale
   * @param model
   * @return
   */
  @RequestMapping(value = "/password", method = RequestMethod.POST)
  public @ResponseBody ValidationResponse editPasswordAction(@Valid @ModelAttribute("emailPasswordForm") EmailPasswordForm emailPasswordForm, BindingResult bindingResult, HttpServletRequest request, Locale locale, ModelMap model) {
    ValidationResponse res = new ValidationResponse();
    changePasswordValidator.validate(emailPasswordForm, bindingResult);

    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      Profile profile = getProfileDetails();
      res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
      res.setAction(ResponseAction.responseAction.REDIRECT.toString());
      res.setTarget(request.getContextPath() + "/profile/settings/password");
      String passwordHash = passwordEncoder.encode(emailPasswordForm.getNewPassword());
      profile.setPassword(passwordHash);
      profileService.save(profile, null, locale);

      String[] args = {messageSource.getMessage("label.password.your", null, locale)};
      String message = messageSource.getMessage("message.update.success", args, locale);
      model.addAttribute("message", message);
    }
    return res;
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(value = {"/role"}, method = RequestMethod.GET, produces = "text/html")
  public String editRoleForm(ModelMap model, SessionStatus sessionStatus) {
    sessionStatus.setComplete();
    model.addAttribute("title", "Proov :: profile role setting");
    model.addAttribute("roleForm", getProfileDetails());
    return "profilesettings/role";
  }


  @RequestMapping(value = {"/role"}, method = RequestMethod.POST)
  public
  @ResponseBody
  ValidationResponse editRoleAction(@Valid @ModelAttribute("roleForm") Profile roleForm, BindingResult bindingResult, Locale locale, HttpServletRequest request, ModelMap model) {
    ValidationResponse res = new ValidationResponse();
    roleValidator.validate(roleForm, bindingResult);

    if (bindingResult.hasErrors()) {
      res.setStatus(responseAction.FAIL.toString());
      List<FieldError> allErrors = bindingResult.getFieldErrors();
      List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
      ArrayList<Object> argsFor = new ArrayList<Object>();

      for (FieldError objectError : allErrors) {
        try {
          for (Object d : objectError.getArguments()) {
            argsFor.add(messageSource.getMessage(d.toString(), null, locale));
          }
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), argsFor.toArray(), locale)));
        } catch (Exception e) {
          errorMesages.add(new ErrorMessage(objectError.getField(), messageSource.getMessage(objectError.getCode(), null, locale)));
        }

      }
      res.setErrorMessageList(errorMesages);

    } else {
      Profile profile = getProfileDetails();
      res.setStatus(ResponseAction.responseAction.SUCCESS.toString());
      res.setAction(ResponseAction.responseAction.REDIRECT.toString());
      res.setTarget(request.getContextPath() + "/profile/settings/role");
      profile.setRole(roleForm.getRole());
      profileService.save(profile, null, locale);

      appService.authorizeUser(profile);

      String[] args = {messageSource.getMessage("label.role.your", null, locale)};
      String message = messageSource.getMessage("message.update.success", args, locale);
      model.addAttribute("message", message);
    }
    return res;

  }

}
