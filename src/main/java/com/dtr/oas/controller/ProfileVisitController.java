package com.dtr.oas.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.List;
import java.util.Locale;

import com.dtr.oas.model.*;
import com.dtr.oas.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.dtr.oas.enumeration.ResponseStatus;
import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.enumeration.ResponseAction.*;
import com.dtr.oas.validators.ValidationResponse;

/**
 * @author MOBIOT
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_SELLER','ROLE_BUYER')")
@RequestMapping(value = "/profile/visit")
public class ProfileVisitController {

  private Instant timestamp = Instant.now();

  @Autowired
  private ProfileServiceDetails profileServiceDetails;

  @Autowired
  private CalendarService calendarService;

  @Autowired
  private ProfileNotificationService profileNotificationService;

  @Autowired
  private AppService appService;

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private ProfileService profileService;

  @Autowired
  private ProfileSettingService profileSettingService;

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  @Autowired
  private MessageSource messageSource;

  @ModelAttribute("profile")
  public Profile getProfileDetails() {
    try {
      Profile profile = profileServiceDetails.getProfileDetails();
      return profile;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(produces = "text/html")
  public String homeAction(ModelMap model) {
    model.addAttribute("title", "Proov :: visit home page");
    List<Calendar> list1 = calendarService.findByProfileAndStatus(getProfileDetails(), ResponseStatus.accepted.toString());
    List<Calendar> list2 = calendarService.findByProfileVisitorAndStatus(getProfileDetails(), ResponseStatus.accepted.toString());
    model.addAttribute("list", list1);
    model.addAttribute("timeNow", new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss").format(Timestamp.from(timestamp)));
    model.addAttribute("listVisitor", list2);
    return "profilevisit/home";
  }

  /**
   * @param model
   * @return
   */
  @RequestMapping(value = "/{id}")
  public String showPendingVisitAction(ModelMap model, @PathVariable("id") Long id, @RequestParam(value = "pending", required = false) String pending) {
    model.addAttribute("title", "Proov :: show pending visit");
    Property property = propertyService.find(id);
    List<Calendar> list = calendarService.findByProfileAndPropertyAndStatus(getProfileDetails(), property, ResponseStatus.pending.toString());
    model.addAttribute("list", list);

    return "profilevisit/show";
  }

  /**
   * @return
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.POST)
  public @ResponseBody ValidationResponse responseRequestVisitAction(@PathVariable("id") Long id, @RequestParam(value = "response", required = false) String response, Locale locale) {
    ValidationResponse res = new ValidationResponse();
    if (response.equals("accepted") || response.equals("decline")) {
      Calendar calendar = calendarService.find(getProfileDetails(), id);
      calendar.setStatus(response);
      calendarService.save(calendar);

      int calendarSize = calendarService.findByProfileAndPropertyAndStatus(getProfileDetails(), calendar.getProperty(), ResponseStatus.pending.toString()).size();
      if (calendarSize == 0) {
        Property property = calendar.getProperty();
        property.setPropertyBidRequest(false);
        propertyService.save(property);
        ProfileSetting profileSetting = getProfileDetails().getProfileSetting();
        profileSetting.setShowNotificationProperty(false);
        profileSettingService.save(profileSetting);
      }

      saveNotification(calendar);
      sendNotification(calendar, locale);


      res.setStatus(responseAction.SUCCESS.toString());
      res.setAction(responseAction.INVOKE.toString());
      res.setTarget(functionToInvoke.callBackSuccessFunctionRequestScheduled.toString());
      String[] args = {messageSource.getMessage("label.operation", null, locale)};
      String messageFunction = messageSource.getMessage("message.save.success", args, locale);
      String[] arguments = {String.valueOf(id), messageFunction, String.valueOf(calendarSize), String.valueOf(calendar.getProperty().getId()), EnumLabel.idJsLabel.Property.toString().toLowerCase()};
      res.setArguments(arguments);
      return res;
    } else {
      res.setStatus(responseAction.FAIL.toString());
      return res;
    }
  }

  @RequestMapping(value = "/visit", produces = "text/html")
  public String visitAction(ModelMap model) {
    model.addAttribute("title", "Proov :: show visit");
    return "profilevisit/visit";
  }

  @RequestMapping(value = "/show", produces = "text/html")
  public String visitorInterfaceAction(ModelMap model) {
    model.addAttribute("title", "Proov :: show visitor interface");
    return "profilevisit/visitorinterface";
  }

  @RequestMapping(value = "/put", produces = "text/html")
  public String hostInterfaceAction(ModelMap model) {
    model.addAttribute("title", "Proov :: show host interface");
    /*List<Calendar> calendar = calendarService.findByProfileAndStatus(getProfileDetails());
    model.addAttribute("calendar", calendar);*/
		/*Date date= new Date();
		Calendar foundVisitOnlineLive = calendarService.getWhetherVisitOnline(getProfileDetails().getId(),  ACCEPTED, new Timestamp(date.getTime()));
		if(foundVisitOnlineLive != null){
			return "redirect:/";
		}*/
    return "profilevisit/hostinterface";
  }

  private ProfileNotification saveNotification(Calendar calendar) {
    ProfileNotification profileNotification = new ProfileNotification();
    profileNotification.setId(appService.getRandomString(12));
    profileNotification.setNotificationStatus(EnumLabel.statusReading.LabelNotRead.toString());
    profileNotification.setNotificationObject(EnumLabel.idJsLabel.Visit.toString());
    profileNotification.setNotificationTarget(String.valueOf(calendar.getId()));
    profileNotification.setNotificationDate(Timestamp.from(timestamp));
    profileNotification.setProfile1(calendar.getProfile2());
    profileNotification.setProfile2(calendar.getProfile1());

    profileNotificationService.save(profileNotification);
    return profileNotification;
  }

  /**
   * @param locale
   * @return
   */
  private void sendNotification(Calendar calendar, Locale locale) {
    Profile profile = calendar.getProfile2();
    int newNotificationCount = Integer.parseInt(profile.getNotificationCount()) + 1;
    profile.setNotificationCount(String.valueOf(newNotificationCount));
    profileService.save(profile, null, locale);

    ProfileSetting profileSetting1 = calendar.getProfile1().getProfileSetting();
    profileSetting1.setShowNotificationVisit(true);
    profileSettingService.save(profileSetting1);

    ProfileSetting profileSetting2 = calendar.getProfile2().getProfileSetting();
    profileSetting2.setShowNotificationVisit(true);
    profileSettingService.save(profileSetting2);

    String[] argument = {calendar.getProperty().getId().toString()};
    String notification = messageSource.getMessage("label.property.request.accepted", argument, locale);
    String message = "{\"notifier\":\" " + notification + "\", \"notificationCount\":\" " + newNotificationCount + "\", \"css\":\"visit\"}";
    simpMessagingTemplate.convertAndSendToUser(calendar.getProfile2().getEmail(), "/queue/note", message);
    return;
  }

}
