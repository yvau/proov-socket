package com.dtr.oas.conversionservice;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.core.convert.converter.Converter;

public class TimeFormatter implements Converter<Timestamp, String> {

  private final MessageSource messageSource;
  private SimpleDateFormat df2 = new SimpleDateFormat("MMMM dd, yyyy");
  private SimpleDateFormat df1 = new SimpleDateFormat("HH:mm");


  public TimeFormatter(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  @Override
  public String convert(Timestamp timestamp) {
    Locale locale = Locale.getDefault();
    String dateText = df2.format(timestamp);
    String timeText = df1.format(timestamp);
    String[] args = {dateText,timeText};

    return this.messageSource.getMessage("label.datetime", args, locale);
  }

}