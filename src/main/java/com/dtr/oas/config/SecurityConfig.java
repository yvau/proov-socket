package com.dtr.oas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.dtr.oas.security.CustomAuthenticationFailureHandler;
import com.dtr.oas.security.CustomAuthenticationSuccessHandler;
import com.dtr.oas.service.ProfileServiceDetails;

@Configuration
@EnableWebMvcSecurity
@ComponentScan(basePackages={"com.dtr.oas.service"})
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
	@Autowired
	@Qualifier("userDetailsService")
	private UserDetailsService userDetailsService;
    
	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
	@Bean
	public ProfileServiceDetails profileDetails(){
		ProfileServiceDetails profileDetails = new ProfileServiceDetails();
		return profileDetails;
	}
	
	 @Bean
	 public CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler() {
	    return new CustomAuthenticationSuccessHandler();
	 }
	 
	 @Bean
	 public CustomAuthenticationFailureHandler customAuthenticationFailureHandler() {
	    return new CustomAuthenticationFailureHandler();
	 }
	 
	 @Configuration
		protected static class AuthenticationConfiguration extends
				GlobalAuthenticationConfigurerAdapter {
		 
		 @Autowired
		    private UserDetailsService userDetailsService;
		    @Autowired
		    private PasswordEncoder passwordEncoder;
		    
			@Override
			public void init(AuthenticationManagerBuilder auth) throws Exception {
				auth
	            .userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);

			}
		} 
	 
	 //le ken xssprotect1on
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
    	.headers().xssProtection().disable()
	    .csrf().disable()
            .authorizeRequests()
                .antMatchers("/**").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/register").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/security_check")
                .usernameParameter("email")
                .passwordParameter("password")
                .defaultSuccessUrl("/")
                /*.failureHandler(customAuthenticationFailureHandler())*/
                .failureUrl("/login?er=0")
                .successHandler(customAuthenticationSuccessHandler())
                .permitAll()
                .and()
            .exceptionHandling()
            	.accessDeniedPage("/403")
            	.and()
            .logout()
            	.logoutUrl("/logout")
            	.logoutSuccessUrl("/")
            	.deleteCookies("JSESSIONID")
                .permitAll();
    }
}