package com.dtr.oas.config;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import com.dtr.oas.conversionservice.TimeFormatter;
import org.apache.commons.lang3.CharEncoding;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import com.dtr.oas.json.HibernateAwareObjectMapper;

@Configuration
@EnableWebMvc
@EnableAsync
@EnableScheduling
@EnableCaching
@ComponentScan({ "com.dtr.oas.*"})
@PropertySource("classpath:config.properties")
public class WebAppConfig extends WebMvcConfigurerAdapter {

  private static final String PROPERTY_MAIL_HOST = "mail.host";
  private static final String PROPERTY_MAIL_PORT = "mail.port";
  private static final String PROPERTY_MAIL_USERNAME = "mail.username";
  private static final String PROPERTY_MAIL_PASSWORD = "mail.password";
  private static final String PROPERTY_MAIL_SMTP_STARTTLS = "mail.smtp.starttls.enable";
  private static final String PROPERTY_MAIL_SMTP_AUTH = "mail.smtp.auth";
  private static final String PROPERTY_MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
  private static final String PROPERTY_MAIL_DEBUG = "mail.debug";

  @Resource
  private Environment env;

  //
  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
    mappingJacksonHttpMessageConverter.setObjectMapper(new HibernateAwareObjectMapper());
    mappingJacksonHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON));
    converters.add(mappingJacksonHttpMessageConverter);
  }

  /**
   * Ensures that placeholders are replaced with property values
   */
  @Bean
  static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean
  public HibernateAwareObjectMapper hibernateAwareObjectMapper() {
    HibernateAwareObjectMapper hibernateAwareObjectMapper = new HibernateAwareObjectMapper();
    return hibernateAwareObjectMapper;
  }

  @Bean
  public CommonsMultipartResolver multipartResolver() {
    CommonsMultipartResolver resolver = new CommonsMultipartResolver();
    resolver.setMaxUploadSize(5000000);
    resolver.setDefaultEncoding("utf-8");
    return resolver;
  }

  @Bean
  public JavaMailSenderImpl mailSender() {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost(env.getRequiredProperty(PROPERTY_MAIL_HOST));
    mailSender.setPort(Integer.parseInt(env.getRequiredProperty(PROPERTY_MAIL_PORT)));
    //Set gmail email id
    mailSender.setUsername(env.getRequiredProperty(PROPERTY_MAIL_USERNAME));
    //Set gmail email password
    mailSender.setPassword(env.getRequiredProperty(PROPERTY_MAIL_PASSWORD));
    Properties prop = mailSender.getJavaMailProperties();
    prop.put(PROPERTY_MAIL_TRANSPORT_PROTOCOL, env.getRequiredProperty(PROPERTY_MAIL_TRANSPORT_PROTOCOL));
    prop.put(PROPERTY_MAIL_SMTP_AUTH, env.getRequiredProperty(PROPERTY_MAIL_SMTP_AUTH));
    prop.put(PROPERTY_MAIL_SMTP_STARTTLS, env.getRequiredProperty(PROPERTY_MAIL_SMTP_STARTTLS));
    prop.put(PROPERTY_MAIL_DEBUG, env.getRequiredProperty(PROPERTY_MAIL_DEBUG));
    return mailSender;
  }

  @Bean
  public CacheManager cacheManager() {
    return new EhCacheCacheManager(ehCacheCacheManager().getObject());
  }

  @Bean
  public EhCacheManagerFactoryBean ehCacheCacheManager() {
    EhCacheManagerFactoryBean cacheManagerFactoryBean = new EhCacheManagerFactoryBean();
    cacheManagerFactoryBean.setConfigLocation(new ClassPathResource("cache/ehcache.xml"));
    cacheManagerFactoryBean.setShared(true);
    return cacheManagerFactoryBean;
  }

  @Override
  public void addFormatters(FormatterRegistry formatterRegistry) {
    formatterRegistry.addConverter(new TimeFormatter(configureMessageSource()));
  }


  @Bean
  public ThymeleafViewResolver thymeleafViewResolver() {
    ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
    thymeleafViewResolver.setTemplateEngine(templateEngine());
    thymeleafViewResolver.setCharacterEncoding(CharEncoding.UTF_8);
    return thymeleafViewResolver;
  }


  @Bean
  public ClassLoaderTemplateResolver emailTemplateResolver() {
    ClassLoaderTemplateResolver emailTemplateResolver = new ClassLoaderTemplateResolver();
    emailTemplateResolver.setPrefix("mail/");
    emailTemplateResolver.setSuffix(".html");
    emailTemplateResolver.setTemplateMode("HTML5");
    emailTemplateResolver.setCharacterEncoding(CharEncoding.UTF_8);
    emailTemplateResolver.setOrder(1);
    return emailTemplateResolver;
  }

  @Bean
  public ServletContextTemplateResolver webTemplateResolver() {
    ServletContextTemplateResolver webTemplateResolver = new ServletContextTemplateResolver();
    webTemplateResolver.setPrefix("/WEB-INF/html/");
    webTemplateResolver.setSuffix(".html");
    webTemplateResolver.setTemplateMode("HTML5");
    webTemplateResolver.setCharacterEncoding(CharEncoding.UTF_8);
    webTemplateResolver.setOrder(2);
    return webTemplateResolver;
  }

  @Bean
  public SpringTemplateEngine templateEngine() {
    SpringTemplateEngine templateEngine = new SpringTemplateEngine();
    templateEngine.addTemplateResolver(emailTemplateResolver());
    templateEngine.addDialect(new SpringSecurityDialect());
    templateEngine.addTemplateResolver(webTemplateResolver());
    return templateEngine;
  }

  @Bean
  public ThymeleafViewResolver viewResolver() {
    ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
    viewResolver.setTemplateEngine(templateEngine());

    return viewResolver;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {

    LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
    localeChangeInterceptor.setParamName("lang");
    registry.addInterceptor(localeChangeInterceptor);
  }

   /* @Bean
    public LocaleResolver localeResolver() {

        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(StringUtils.parseLocaleString("en"));
        return cookieLocaleResolver;
    }*/

  @Bean(name = "messageSource")
  public MessageSource configureMessageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasename("messages");
    messageSource.setCacheSeconds(5);
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }


}
