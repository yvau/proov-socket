package com.dtr.oas.form;

import java.util.Date;

import com.dtr.oas.model.Location;
import com.dtr.oas.model.Profile;


/**
 * The persistent class for the owning_proposal database table.
 * 
 */

public class ProposalForm {

	private Long id;
	
	private String country;
	
	private String province;
	
	private String city;

	private String ageOfProperty;

	private int bathroomsMax;

	private int bathroomsMin;

	private int bedroomsMax;

	private int bedroomsMin;

	private String carShelter;

	private String constructionOfProperty;

	private Date dateCreated;

	private String features;

	private String furnished;

	private String garage;

	private String ground;

	private String masterBedroomWithBathroom;

	private String maximumPrice;

	private String minimumPrice;
	
	private double priceMax;
	
	private double priceMin;

	private String parking;

	private String privateEntry;

	private String propertyType;

	private String propertyTypeInProposal;

	private String proposalType;

	private String rushPriority;

	private String securityFacilities;

	private String sewer;

	private Integer totalBinding;

	private Integer totalStatusReadingSeen;

	private Integer totalStatusReadingUnseen;

	private String typeOfAccommodation;

	private String typeOfExteriorSiding;

	private String windowsSiding;

	private String unhinderedAccess;

	private String water;

	private String windowsConstruction;
	
	private int numberOfAnswerPropositionsUnseen;

	private String email;
	
	private Location location;

	public ProposalForm() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAgeOfProperty() {
		return this.ageOfProperty;
	}

	public void setAgeOfProperty(String ageOfProperty) {
		this.ageOfProperty = ageOfProperty;
	}

	public int getBathroomsMax() {
		return bathroomsMax;
	}

	public void setBathroomsMax(int bathroomsMax) {
		this.bathroomsMax = bathroomsMax;
	}

	public int getBathroomsMin() {
		return bathroomsMin;
	}

	public void setBathroomsMin(int bathroomsMin) {
		this.bathroomsMin = bathroomsMin;
	}

	public int getBedroomsMax() {
		return bedroomsMax;
	}

	public void setBedroomsMax(int bedroomsMax) {
		this.bedroomsMax = bedroomsMax;
	}

	public int getBedroomsMin() {
		return bedroomsMin;
	}

	public void setBedroomsMin(int bedroomsMin) {
		this.bedroomsMin = bedroomsMin;
	}

	public String getCarShelter() {
		return this.carShelter;
	}

	public void setCarShelter(String carShelter) {
		this.carShelter = carShelter;
	}

	public String getConstructionOfProperty() {
		return this.constructionOfProperty;
	}

	public void setConstructionOfProperty(String constructionOfProperty) {
		this.constructionOfProperty = constructionOfProperty;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getFeatures() {
		return this.features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getFurnished() {
		return this.furnished;
	}

	public void setFurnished(String furnished) {
		this.furnished = furnished;
	}

	public String getGarage() {
		return this.garage;
	}

	public void setGarage(String garage) {
		this.garage = garage;
	}

	public String getGround() {
		return this.ground;
	}

	public void setGround(String ground) {
		this.ground = ground;
	}

	public String getMasterBedroomWithBathroom() {
		return this.masterBedroomWithBathroom;
	}

	public void setMasterBedroomWithBathroom(String masterBedroomWithBathroom) {
		this.masterBedroomWithBathroom = masterBedroomWithBathroom;
	}

	public String getMaximumPrice() {
		return this.maximumPrice;
	}

	public void setMaximumPrice(String maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	public String getMinimumPrice() {
		return this.minimumPrice;
	}

	public void setMinimumPrice(String minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	public double getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(double priceMax) {
		this.priceMax = priceMax;
	}

	public double getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(double priceMin) {
		this.priceMin = priceMin;
	}

	public String getParking() {
		return this.parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public String getPrivateEntry() {
		return this.privateEntry;
	}

	public void setPrivateEntry(String privateEntry) {
		this.privateEntry = privateEntry;
	}

	public String getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getPropertyTypeInProposal() {
		return propertyTypeInProposal;
	}

	public void setPropertyTypeInProposal(String propertyTypeInProposal) {
		this.propertyTypeInProposal = propertyTypeInProposal;
	}

	public String getProposalType() {
		return this.proposalType;
	}

	public void setProposalType(String proposalType) {
		this.proposalType = proposalType;
	}

	public String getRushPriority() {
		return this.rushPriority;
	}

	public void setRushPriority(String rushPriority) {
		this.rushPriority = rushPriority;
	}

	public String getSecurityFacilities() {
		return this.securityFacilities;
	}

	public void setSecurityFacilities(String securityFacilities) {
		this.securityFacilities = securityFacilities;
	}

	public String getSewer() {
		return this.sewer;
	}

	public void setSewer(String sewer) {
		this.sewer = sewer;
	}

	public Integer getTotalBinding() {
		return this.totalBinding;
	}

	public void setTotalBinding(Integer totalBinding) {
		this.totalBinding = totalBinding;
	}

	public Integer getTotalStatusReadingSeen() {
		return this.totalStatusReadingSeen;
	}

	public void setTotalStatusReadingSeen(Integer totalStatusReadingSeen) {
		this.totalStatusReadingSeen = totalStatusReadingSeen;
	}

	public Integer getTotalStatusReadingUnseen() {
		return this.totalStatusReadingUnseen;
	}

	public void setTotalStatusReadingUnseen(Integer totalStatusReadingUnseen) {
		this.totalStatusReadingUnseen = totalStatusReadingUnseen;
	}

	public String getTypeOfAccommodation() {
		return this.typeOfAccommodation;
	}

	public void setTypeOfAccommodation(String typeOfAccommodation) {
		this.typeOfAccommodation = typeOfAccommodation;
	}

	public String getTypeOfExteriorSiding() {
		return this.typeOfExteriorSiding;
	}

	public void setTypeOfExteriorSiding(String typeOfExteriorSiding) {
		this.typeOfExteriorSiding = typeOfExteriorSiding;
	}

	public String getWindowsSiding() {
		return windowsSiding;
	}

	public void setWindowsSiding(String windowsSiding) {
		this.windowsSiding = windowsSiding;
	}

	public String getUnhinderedAccess() {
		return this.unhinderedAccess;
	}

	public void setUnhinderedAccess(String unhinderedAccess) {
		this.unhinderedAccess = unhinderedAccess;
	}

	public String getWater() {
		return this.water;
	}

	public void setWater(String water) {
		this.water = water;
	}

	public String getWindowsConstruction() {
		return this.windowsConstruction;
	}

	public void setWindowsConstruction(String windowsConstruction) {
		this.windowsConstruction = windowsConstruction;
	}

	public int getNumberOfAnswerPropositionsUnseen() {
		return numberOfAnswerPropositionsUnseen;
	}

	public void setNumberOfAnswerPropositionsUnseen(int numberOfAnswerPropositionsUnseen) {
		this.numberOfAnswerPropositionsUnseen = numberOfAnswerPropositionsUnseen;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

}