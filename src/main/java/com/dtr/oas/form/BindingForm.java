package com.dtr.oas.form;

import java.util.List;

import com.dtr.oas.model.Binding;

public class BindingForm {
	
	 private List<Binding> bindings;
	 
	 public List<Binding> getBindings() {
	        return bindings;
	 }
	 
	 public void setBindings(List<Binding> bindings) {
	        this.bindings = bindings;
	 }

}
