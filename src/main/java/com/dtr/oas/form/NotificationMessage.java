package com.dtr.oas.form;


import com.dtr.oas.model.PortfolioPosition;

import java.util.LinkedHashMap;
import java.util.Map;

public class NotificationMessage{

  private final Map<String,NotificationMessage> positionLookup = new LinkedHashMap<String,NotificationMessage>();

	private String notificationDate;

	private String notificationObject;

	private String notificationStatus;

	private String notificationTarget;

	private String notificationVerb;

	private String profile;

	private String profileNotifier;

	public NotificationMessage(String notificationDate, String notificationObject, String notificationStatus,
							   String notificationTarget, String notificationVerb, String profile, String profileNotifier) {
		
		this.notificationDate = notificationDate;

		this.notificationObject = notificationObject;

		this.notificationStatus = notificationStatus;

		this.notificationTarget = notificationTarget;

		this.notificationVerb = notificationVerb;

		this.profile = profile;

		this.profileNotifier = profileNotifier;
	}

  public NotificationMessage() {

  }

  public String getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}

	public String getNotificationObject() {
		return notificationObject;
	}

	public void setNotificationObject(String notificationObject) {
		this.notificationObject = notificationObject;
	}

	public String getNotificationStatus() {
		return notificationStatus;
	}

	public void setNotificationStatus(String notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	public String getNotificationTarget() {
		return notificationTarget;
	}

	public void setNotificationTarget(String notificationTarget) {
		this.notificationTarget = notificationTarget;
	}

	public String getNotificationVerb() {
		return notificationVerb;
	}

	public void setNotificationVerb(String notificationVerb) {
		this.notificationVerb = notificationVerb;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getProfileNotifier() {
		return profileNotifier;
	}

	public void setProfileNotifier(String profileNotifier) {
		this.profileNotifier = profileNotifier;
	}

	public void addPosition(NotificationMessage notificationMessage) {
		this.positionLookup.put(notificationMessage.getProfileNotifier(), notificationMessage);
	}
	
	@Override
	public String toString() {
		return "Notification [notifier=" + this.profileNotifier + ", verb=" + this.notificationVerb
				+ ", status=" + this.notificationStatus + ", target=" + this.notificationTarget + ", object=" + this.notificationObject + ", profile=" + this.profile + ", date=" + this.notificationDate + "]";
	}

}