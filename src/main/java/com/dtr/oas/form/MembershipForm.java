package com.dtr.oas.form;

public class MembershipForm {
	
	private String membershipPaymentCardNumber;
	
	private String membershipPaymentAmount;
	
	private String membershipPaymentMethod;
	
	private String nameOnCard;
	
	private String expirationDate;
	
	private String verifyNumber;
	
	private String country;
	
	private String province;
	
	private String city;
	
	private String postalCode;
	
	private String billingAddress;

	public String getMembershipPaymentCardNumber() {
		return membershipPaymentCardNumber;
	}

	public void setMembershipPaymentCardNumber(String membershipPaymentCardNumber) {
		this.membershipPaymentCardNumber = membershipPaymentCardNumber;
	}

	public String getMembershipPaymentAmount() {
		return membershipPaymentAmount;
	}

	public void setMembershipPaymentAmount(String membershipPaymentAmount) {
		this.membershipPaymentAmount = membershipPaymentAmount;
	}

	public String getMembershipPaymentMethod() {
		return membershipPaymentMethod;
	}

	public void setMembershipPaymentMethod(String membershipPaymentMethod) {
		this.membershipPaymentMethod = membershipPaymentMethod;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getVerifyNumber() {
		return verifyNumber;
	}

	public void setVerifyNumber(String verifyNumber) {
		this.verifyNumber = verifyNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	
}
