package com.dtr.oas.form;

import javax.persistence.Lob;

import com.dtr.oas.model.Profile;
import org.hibernate.annotations.Type;

import com.dtr.oas.model.Location;

public class PropertyForm {

	private String saleType;
	
	private String locationcountry;
	
	private String locationprovince;
	
	private String locationcity;
	
	private String zipCode;

	private String address;
	
	private String type;

	private double priceMin;

	private double priceMax;
	
	private String propertyPrice;

	private Integer bedrooms;

	private Integer bathrooms;

	private Integer minsqfeet;
	
	private Integer sqfeet;

	private String floorLevel;
	
	private String propertyAge;
	
	private Location location;

	private String email;
	
	
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String propertyDescription;
	
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String propertyFeatures;

	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public String getLocationcountry() {
		return locationcountry;
	}

	public void setLocationcountry(String locationcountry) {
		this.locationcountry = locationcountry;
	}

	public String getLocationprovince() {
		return locationprovince;
	}

	public void setLocationprovince(String locationprovince) {
		this.locationprovince = locationprovince;
	}

	public String getLocationcity() {
		return locationcity;
	}

	public void setLocationcity(String locationcity) {
		this.locationcity = locationcity;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(double priceMin) {
		this.priceMin = priceMin;
	}

	public double getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(double priceMax) {
		this.priceMax = priceMax;
	}

	public String getPropertyPrice() {
		return propertyPrice;
	}

	public void setPropertyPrice(String propertyPrice) {
		this.propertyPrice = propertyPrice;
	}

	public Integer getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public Integer getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(Integer bathrooms) {
		this.bathrooms = bathrooms;
	}

	public Integer getMinsqfeet() {
		return minsqfeet;
	}

	public void setMinsqfeet(Integer minsqfeet) {
		this.minsqfeet = minsqfeet;
	}

	public Integer getSqfeet() {
		return sqfeet;
	}

	public void setSqfeet(Integer sqfeet) {
		this.sqfeet = sqfeet;
	}

	public String getFloorLevel() {
		return floorLevel;
	}

	public void setFloorLevel(String floorLevel) {
		this.floorLevel = floorLevel;
	}

	public String getPropertyAge() {
		return propertyAge;
	}

	public void setPropertyAge(String propertyAge) {
		this.propertyAge = propertyAge;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyFeatures() {
		return propertyFeatures;
	}

	public void setPropertyFeatures(String propertyFeatures) {
		this.propertyFeatures = propertyFeatures;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}