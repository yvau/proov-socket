package com.dtr.oas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.LocationHasProfile;
import com.dtr.oas.model.Profile;


/**
 * @author Yvau
 *
 */
public interface LocationHasProfileRepository extends JpaRepository<LocationHasProfile, String> {
	
	LocationHasProfile findByProfile(Profile profile); 

}