package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;

/**
 * Specifies methods used to obtain and modify person related information
 * which is stored in the database.
 * @author Mobiot Yves-André Fabrice
 */
public interface PropertyRepository extends JpaRepository<Property, Long>, JpaSpecificationExecutor<Property> {

  //@Cacheable("findByProfile1AndId")
	Property findByProfileAndId(Profile profile, Long id);

	//@Cacheable("findByProfile")
	Page<Property> findByProfile(Profile profile, Pageable pageable);
    
}
