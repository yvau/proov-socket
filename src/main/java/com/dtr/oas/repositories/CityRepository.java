package com.dtr.oas.repositories;

import java.util.List;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.City;
import com.dtr.oas.model.Province;


/**
 * @author Yvau
 */
public interface CityRepository extends JpaRepository<City, String> {

  @Cacheable("findByProvince")
  List<City> findByProvince(Province province);

}