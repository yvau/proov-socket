package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dtr.oas.model.Budget;
import com.dtr.oas.model.Profile;


/**
 * @author Yvau
 *
 */
public interface BudgetRepository extends JpaRepository<Budget, Long> {

  @Cacheable("budgetFindByProfile")
	Budget findByProfile(Profile profile);

}