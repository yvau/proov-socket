package com.dtr.oas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Ip2nation;


/**
 * @author Yvau
 *
 */
public interface Ip2nationRepository extends JpaRepository<Ip2nation, Long> {
	
	List<Ip2nation> findFirst1ByIdLessThanOrderByIdDesc(Long id);

}