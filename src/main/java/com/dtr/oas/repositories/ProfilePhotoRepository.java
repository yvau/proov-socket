package com.dtr.oas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.ProfilePhoto;


/**
 * @author Yvau
 *
 */
public interface ProfilePhotoRepository extends JpaRepository<ProfilePhoto, String> {

    

}