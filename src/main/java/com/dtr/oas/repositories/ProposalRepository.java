package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dtr.oas.model.Proposal;
import com.dtr.oas.model.Profile;


/**
 * @author Yvau
 */
public interface ProposalRepository extends JpaRepository<Proposal, Long>, JpaSpecificationExecutor<Proposal> {

  //@Cacheable("findByProposalType")
  Page<Proposal> findByProposalType(String proposalType, Pageable pageable);

  //@Cacheable("findByProposalTypeAndProfile")
  Page<Proposal> findByProposalTypeAndProfile(String proposalType, Profile profile, Pageable pageable);

  //@Cacheable("findByProfile1AndId")
  Proposal findByProfileAndId(Profile profile, Long id);
}