package com.dtr.oas.repositories;


import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Country;
import com.dtr.oas.model.Province;


/**
 * @author Yvau
 */
public interface ProvinceRepository extends JpaRepository<Province, String> {

  @Cacheable("findByCountry")
  List<Province> findByCountry(Country country);

}