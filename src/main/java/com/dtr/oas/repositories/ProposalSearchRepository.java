package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.ProposalSearch;
import com.dtr.oas.model.Profile;


/**
 * @author Yvau
 *
 */
public interface ProposalSearchRepository extends JpaRepository<ProposalSearch, Long> {

	//@Cacheable("findByProfileAndProposalType")
	ProposalSearch findByProfileAndProposalType(Profile profile, String proposalType);

}