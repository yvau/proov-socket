package com.dtr.oas.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Profile;
import com.dtr.oas.model.PropertySearch;


/**
 * @author Yvau
 *
 */
public interface PropertySearchRepository extends JpaRepository<PropertySearch, Long> {

	//@Cacheable("findByProfile")
	PropertySearch findByProfile(Profile profile);
	
	//List<PropertySearch> removeByProfile(Profile profile);
}