package com.dtr.oas.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileNotification;


/**
 * @author Yvau
 */
public interface ProfileNotificationRepository extends JpaRepository<ProfileNotification, String> {

  //Long countByNotificationStatusAndProfile1(String status, Profile profile);

  //@Cacheable("findByProfile1")
  List<ProfileNotification> findByProfile1(Profile profile);
}