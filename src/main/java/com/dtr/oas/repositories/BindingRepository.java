package com.dtr.oas.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dtr.oas.model.Binding;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Proposal;


/**
 * @author Yvau
 *
 */
public interface BindingRepository extends JpaRepository<Binding, Long> {

	//@Cacheable("findByProfileAndProposalAndStatusInterest")
	List<Binding> findByProfileAndProposalAndStatusInterest(Profile profile, Proposal proposal, String statusInterest);
}