package com.dtr.oas.repositories;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dtr.oas.model.Calendar;
import com.dtr.oas.model.Profile;
import com.dtr.oas.model.Property;


/**
 * @author Yvau
 *
 */
public interface CalendarRepository extends JpaRepository<Calendar, Long> {

	//@Cacheable("findByProfile1AndStatus")
	List<Calendar> findByProfile1AndStatus(Profile profile, String status);

	//@Cacheable("findByProfile2AndStatus")
	List<Calendar> findByProfile2AndStatus(Profile profile, String status);
	
	@Query("SELECT c FROM Calendar c WHERE c.profile1 = :profile AND c.status = LOWER(:status) AND :timeStampNow BETWEEN c.startDate AND c.endDate")
	Calendar findByProfileAndStatusAndStartDate(@Param("profile") Profile profile, @Param("status") String status, @Param("timeStampNow") Timestamp timeStampNow);

  //@Cacheable("findByProfile1AndPropertyAndStatus")
	List<Calendar> findByProfile1AndPropertyAndStatus(Profile profile, Property property, String status);

  //@Cacheable("findByProfile1AndId")
	Calendar findByProfile1AndId(Profile profile, Long id);

}