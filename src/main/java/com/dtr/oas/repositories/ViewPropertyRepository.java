package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dtr.oas.model.ViewProperty;


/**
 * @author Yvau
 *
 */
public interface ViewPropertyRepository extends JpaRepository<ViewProperty, Long> {

	@Cacheable("viewPropertyFindByIpNameView")
	ViewProperty findByIpNameView(String ipNameView);
}