package com.dtr.oas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Availability;
import com.dtr.oas.model.Profile;


/**
 * @author Yvau
 *
 */
public interface AvailabilityRepository extends JpaRepository<Availability, String> {

    List<Availability> findByProfile(Profile profile);

}