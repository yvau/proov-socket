package com.dtr.oas.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.dtr.oas.form.ProposalForm;
import com.dtr.oas.model.Proposal;
import com.dtr.oas.model.Proposal_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * A class which is used to create Specification objects which are used
 * to create JPA criteria queries for person information.
 * @author Mobiot Ohouet Yves-Andr�
 */
public class ProposalSpecifications {

	  public static Specification<Proposal> specification(final ProposalForm searchProposal) {
	    return new Specification<Proposal>() {
	      public Predicate toPredicate(Root<Proposal> root, CriteriaQuery<?> query,
	            CriteriaBuilder builder) {
	      List<Predicate> criteriaList = new ArrayList<Predicate>();
	      if (searchProposal.getCountry() != null || searchProposal.getCountry() =="") {
	      Predicate country = builder.and(
	    		  builder.equal(root.get( Proposal_.location ).get("country").get("id"), searchProposal.getCountry() )
	      );
	      criteriaList.add(country);
	      }
	      Predicate province = builder.and(
	    		  builder.equal(root.get( Proposal_.location ).get("province").get("id"), searchProposal.getProvince() )
	      );
	      Predicate city = builder.and(
	    		  builder.equal(root.get( Proposal_.location ).get("city").get("id"), searchProposal.getCity() )
	      );
	      Predicate proposalType = builder.and(
	    		  builder.equal(root.get( Proposal_.proposalType ), searchProposal.getProposalType() )
	      );
	      Predicate propertyTypeInProposal = builder.and(
	    		  builder.equal(root.get( Proposal_.propertyTypeInProposal ), searchProposal.getPropertyTypeInProposal() )
	      );
	      Predicate priceMin = builder.and(
	    		  builder.greaterThanOrEqualTo(root.get( Proposal_.minimumPrice ), searchProposal.getPriceMin() )
	       );
	      Predicate priceMax = builder.and(
	    		  builder.lessThanOrEqualTo(root.get( Proposal_.maximumPrice ), searchProposal.getPriceMax() )
	       );
	      Predicate bathMax = builder.and(
	    		  builder.lessThanOrEqualTo(root.get( Proposal_.bathroomsMax ), searchProposal.getBathroomsMax() )
	       );
	      Predicate bathMin = builder.and(
	    		  builder.greaterThanOrEqualTo(root.get( Proposal_.bathroomsMin ), searchProposal.getBathroomsMin() )
	       );
	      Predicate bedMin = builder.and(
	    		  builder.greaterThanOrEqualTo(root.get( Proposal_.bedroomsMin ), searchProposal.getBedroomsMin() )
	       );
	      Predicate bedMax = builder.and(
	    		  builder.lessThanOrEqualTo(root.get( Proposal_.bedroomsMax ), searchProposal.getBedroomsMax() )
	       );
	      Predicate rush = builder.and(
	    		  builder.equal(root.get( Proposal_.rushPriority ), searchProposal.getRushPriority() )
	       );
	      Predicate answerPropositionsUnseen = builder.and(
	    		  builder.greaterThanOrEqualTo(root.get( Proposal_.numberOfAnswerPropositionsUnseen ), searchProposal.getNumberOfAnswerPropositionsUnseen() )
	       );
				Predicate email = builder.and(
						builder.notEqual(root.get( Proposal_.profile).get("email"), searchProposal.getEmail() )
        );
	      if (searchProposal.getCountry() != null) {
	    	  //criteriaList.add(country);
	      }
	      if (searchProposal.getProvince() != null) {
	    	  criteriaList.add(province);
	      }
	      if (searchProposal.getCity() != null) {
	    	  criteriaList.add(city);
	      }
	      if (searchProposal.getProposalType() != null) {
	    	  criteriaList.add(proposalType);
	      }
	      if (searchProposal.getPropertyTypeInProposal() != null) {
	    	  criteriaList.add(propertyTypeInProposal);
	      }
	      if (searchProposal.getPriceMin() != 0) {
	    	  criteriaList.add(priceMin);
	      }
	      if (searchProposal.getPriceMax() != 0) {
	    	  criteriaList.add(priceMax);
	      }
	      if (searchProposal.getBathroomsMin() != 0) {
	    	  criteriaList.add(bathMin);
	      }
	      if (searchProposal.getBathroomsMax() != 0) {
	    	  criteriaList.add(bathMax);
	      }
	      if (searchProposal.getBedroomsMin() != 0) {
	    	  criteriaList.add(bedMin);
	      }
	      if (searchProposal.getBedroomsMax() != 0) {
	    	  criteriaList.add(bedMax);
	      }
	      if (searchProposal.getRushPriority() != null) {
	    	  criteriaList.add(rush);
	      }
	      if (searchProposal.getNumberOfAnswerPropositionsUnseen() != 0) {
	    	  criteriaList.add(answerPropositionsUnseen);
	      }
        if (searchProposal.getEmail() != null) {
           criteriaList.add(email);
        }
	      
	      Predicate[] predArray = new Predicate[criteriaList.size()];
	         
	      return builder.and((Predicate[]) criteriaList.toArray(predArray));
	      }
	    };
	}
}