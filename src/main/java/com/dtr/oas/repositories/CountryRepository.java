package com.dtr.oas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Country;


/**
 * @author Yvau
 *
 */
public interface CountryRepository extends JpaRepository<Country, String> {

}