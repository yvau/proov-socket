package com.dtr.oas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Location;


/**
 * @author Yvau
 *
 */
public interface LocationRepository extends JpaRepository<Location, String> {

}