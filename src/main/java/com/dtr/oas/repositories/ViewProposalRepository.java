package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.ViewProposal;


/**
 * @author Yvau
 */
public interface ViewProposalRepository extends JpaRepository<ViewProposal, Long> {

  @Cacheable("viewProposalFindByIpNameView")
  ViewProposal findByIpNameView(String ipNameView);

}