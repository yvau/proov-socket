package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Profile;


/**
 * @author Yvau
 *
 */
public interface ProfileRepository extends JpaRepository<Profile, Long> {

	//@Cacheable("findByEmail")
	Profile findByEmail(String email);

	//@Cacheable("findByToken")
	Profile findByToken(String token);
}