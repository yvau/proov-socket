package com.dtr.oas.repositories;


import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dtr.oas.model.Profile;
import com.dtr.oas.model.ProfileSetting;


/**
 * @author Yvau
 */
public interface ProfileSettingRepository extends JpaRepository<ProfileSetting, Long> {

  //@Cacheable("findByProfile")
  ProfileSetting findByProfile(Profile profile);

}