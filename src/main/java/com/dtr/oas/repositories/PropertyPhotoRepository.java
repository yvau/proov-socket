package com.dtr.oas.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dtr.oas.model.Property;
import com.dtr.oas.model.PropertyPhoto;


/**
 * @author Yvau
 *
 */
public interface PropertyPhotoRepository extends JpaRepository<PropertyPhoto, Long> {

    //@Cacheable("findByProperty")
    List<PropertyPhoto> findByProperty(Property property);

}