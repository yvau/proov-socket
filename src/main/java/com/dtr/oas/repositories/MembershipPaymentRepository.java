package com.dtr.oas.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dtr.oas.model.MembershipPayment;
import com.dtr.oas.model.Profile;


/**
 * @author Yvau
 *
 */
public interface MembershipPaymentRepository extends JpaRepository<MembershipPayment, String> {

    //@Cacheable("findByProfile")
    Page<MembershipPayment> findByProfile(Profile profile, Pageable pageable);

}