package com.dtr.oas.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.dtr.oas.form.PropertyForm;
import com.dtr.oas.model.Property;
import com.dtr.oas.model.Property_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * A class which is used to create Specification objects which are used
 * to create JPA criteria queries for person information.
 * @author Mobiot Ohouet Yves-Andr�
 */
public class PropertySpecifications {

	  public static Specification<Property> specification(final PropertyForm searchTerm) {
	    return new Specification<Property>() {
	      public Predicate toPredicate(Root<Property> root, CriteriaQuery<?> query,
	            CriteriaBuilder builder) {
	      List<Predicate> criteriaList = new ArrayList<Predicate>();
	      if (searchTerm.getLocationcountry() != null || searchTerm.getLocationcountry() =="") {
	    	  
	      Predicate country = builder.and(
	    		  builder.equal(root.get( Property_.location ).get("country").get("id"), searchTerm.getLocationcountry() )
	      );
	      criteriaList.add(country);
	      }
	      Predicate province = builder.and(
	    		  builder.equal(root.get( Property_.location ).get("province").get("id"), searchTerm.getLocationprovince() )
	      );
	      Predicate city = builder.and(
	    		  builder.equal(root.get( Property_.location ).get("city").get("id"), searchTerm.getLocationcity() )
	      );
	      Predicate saleType = builder.and(
	    			    builder.equal( root.get( Property_.propertySaleType ), searchTerm.getSaleType())
	    	);
	      Predicate propertyType = builder.and(
  			    builder.equal( root.get( Property_.propertyType ), searchTerm.getType())
  			);
	      Predicate priceMin = builder.and(
	    		  builder.greaterThanOrEqualTo(root.get( Property_.propertyPrice ), searchTerm.getPriceMin() )
	       );
	      Predicate priceMax = builder.and(
	    		  builder.lessThanOrEqualTo(root.get( Property_.propertyPrice ), searchTerm.getPriceMax() )
	      );
	      Predicate bedrooms = builder.and(
	    		  builder.greaterThanOrEqualTo(root.get( Property_.propertyNumOfBedrooms ), searchTerm.getBedrooms() )
	      );
	      Predicate bathrooms = builder.and(
	    		  builder.greaterThanOrEqualTo(root.get( Property_.propertyNumOfBathrooms ), searchTerm.getBathrooms() )
	      );
				Predicate email = builder.and(
						builder.notEqual( root.get( Property_.profile).get("email"), searchTerm.getEmail() )
				);

	      if(searchTerm.getSaleType() != null){
	    	 criteriaList.add(saleType);
	      }
	      if (searchTerm.getType() != null) {
	    	 criteriaList.add(propertyType);
	      }
	      if (searchTerm.getPriceMin() != 0) {
	    	  criteriaList.add(priceMin);
	      }
	      if (searchTerm.getPriceMax() != 0) {
	    	  criteriaList.add(priceMax);
	      }
	      if (searchTerm.getBedrooms() != null) {
	    	  criteriaList.add(bedrooms);
	      }
	      if (searchTerm.getBathrooms() != null) {
	    	  criteriaList.add(bathrooms);
	      }
	      if (searchTerm.getLocationcountry() != null) {
	    	  //criteriaList.add(country);
	      }
	      if (searchTerm.getLocationprovince() != null) {
	    	  criteriaList.add(province);
	      }
	      if (searchTerm.getLocationcity() != null) {
	    	  criteriaList.add(city);
	      }
				if (searchTerm.getEmail() != null) {
					criteriaList.add(email);
				}
	      
	      Predicate[] predArray = new Predicate[criteriaList.size()];
	         
	      return builder.and((Predicate[]) criteriaList.toArray(predArray));
	      }
	    };
	}
}