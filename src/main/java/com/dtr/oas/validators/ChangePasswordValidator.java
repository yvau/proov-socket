package com.dtr.oas.validators;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.form.EmailPasswordForm;
import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileServiceDetails;

import org.springframework.beans.factory.annotation.Autowired;

@Component
public class ChangePasswordValidator implements Validator{
	
	@Autowired
	private ProfileServiceDetails profileServiceDetails;
	
	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return EmailPasswordForm.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		EmailPasswordForm emailPasswordForm = (EmailPasswordForm)obj;
		Profile profile = profileServiceDetails.getProfileDetails();
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "currentPassword",  
			    "label.required.is"); 
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword",  
				    "label.required.is"); 
		  
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reNewPassword",  
				    "label.required.is");
		
		
		boolean matched = BCrypt.checkpw(emailPasswordForm.getCurrentPassword(), profile.getPassword());
		
		 if (!(emailPasswordForm.getCurrentPassword().isEmpty())) {  
			  if (!matched) {  
				  errors.rejectValue("currentPassword", "label.password.current.must.match"); 
			  }
		  }
		  
		 if(!(emailPasswordForm.getNewPassword().equals(emailPasswordForm.getReNewPassword()))){
				errors.rejectValue("reNewPassword", "label.password.dont.match");
		  }
		  
		  if (!(emailPasswordForm.getNewPassword().isEmpty())) {  
			  if (emailPasswordForm.getNewPassword().trim().length() < EnumLabel.labelNumber.MinimumPasswordLength.getNumber()) {   
				  errors.rejectValue("newPassword", "label.password.short"); 
			  }
		  }
		  
		  

	}
	
}