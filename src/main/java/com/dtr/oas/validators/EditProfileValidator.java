package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.model.Profile;

@Component
public class EditProfileValidator implements Validator{   



	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return Profile.class.isAssignableFrom(c);
	}
	
	@Override
	@SuppressWarnings("unused")
	public void validate(Object obj, Errors errors) {
		
		Profile profile = (Profile)obj;
		
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "label.label.isRequired", new Object[] {"label.label.lastname"});
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname",
				"label.required.is");
	
	}
	
}