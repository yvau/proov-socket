package com.dtr.oas.validators;

import java.util.List;

public class ValidationResponse {
	private String Target;
	private String status;
	private String action;
	private String[] arguments;
	private List<ErrorMessage> errorMessageList;
	
	public String getTarget() {
		return Target;
	}
	public void setTarget(String target) {
		Target = target;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	public String[] getArguments() {
		return arguments;
	}
	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}
	public List<ErrorMessage> getErrorMessageList() {
		return this.errorMessageList;
	}
	public void setErrorMessageList(List<ErrorMessage> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
}
