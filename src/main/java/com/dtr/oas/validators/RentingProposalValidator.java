package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.form.ProposalForm;

@Component
public class RentingProposalValidator implements Validator{  

	@Override
	public boolean supports(Class<?> c) {
		//just validate the owningProposalr instances
	        return ProposalForm.class.isAssignableFrom(c);
	}
	
	public boolean isNonIntegerDouble(String value) {
	    try {
	        Double.parseDouble(value);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}
	

	
	@Override
	public void validate(Object obj, Errors errors) {
		
		ProposalForm proposal = (ProposalForm)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.country.id",  
			    "label.required.is");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.province.id",  
			    "label.required.is");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.city.id",  
			    "label.required.is");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyTypeInProposal",  
			    "label.required.is");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maximumPrice",  
			    "label.required.is");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "minimumPrice",  
			    "label.required.is");
	

		if ((proposal.getBathroomsMin() == 0) && (proposal.getBathroomsMax() == 0)) {   
			  errors.rejectValue("bathroomsMin", "label.required.is"); 
		 }
		
		if ((proposal.getBedroomsMin() == 0) && (proposal.getBedroomsMax() == 0)) {   
			  errors.rejectValue("bedroomsMin", "label.required.is"); 
		 }
		
			
		if (proposal.getBathroomsMax() < proposal.getBathroomsMin()){
				errors.rejectValue("bathroomsMin", "label.bathrooms.lesser"); 
		 }
		
		if (proposal.getBedroomsMax() < proposal.getBedroomsMin()){
				errors.rejectValue("bedroomsMin", "label.bedrooms.lesser");
		 }
		
		if (!(proposal.getMaximumPrice().isEmpty()) || !(proposal.getMaximumPrice() == "")) {
			boolean matchMaximumPrice = isNonIntegerDouble(String.valueOf(proposal.getMaximumPrice()));
			if (!matchMaximumPrice) {  
			    errors.rejectValue("maximumPrice", "label.price.incorrect");  
			   }else if(matchMaximumPrice){
				 if (Float.valueOf(proposal.getMaximumPrice()) < 0) {
					errors.rejectValue("maximumPrice", "label.price.incorrect"); 
				 }
			   }
		}
		
		if (!(proposal.getMinimumPrice().isEmpty()) || !(proposal.getMinimumPrice() == "")) {
			boolean matchMinimumPrice = isNonIntegerDouble(String.valueOf(proposal.getMinimumPrice()));
			if (!matchMinimumPrice) {  
			    errors.rejectValue("minimumPrice", "label.price.incorrect");  
			   } else if(matchMinimumPrice){
				   if (Float.valueOf(proposal.getMinimumPrice()) < 0) {
						errors.rejectValue("minimumPrice", "label.price.incorrect"); 
					}
			   }
		}
		
		if (proposal.getMinimumPrice() != "" && proposal.getMaximumPrice() != "") {
	
			if (proposal.getMinimumPrice().equals(proposal.getMaximumPrice())) {
				errors.rejectValue("maximumPrice", "label.price.lesser"); 
			}
		}		
	}
	
}