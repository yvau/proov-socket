package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.form.EmailPasswordForm;

@Component
public class ResetPasswordValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return EmailPasswordForm.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		EmailPasswordForm emailPasswordForm = (EmailPasswordForm)obj;
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword",  
				    "label.required.is"); 
		  
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reNewPassword",  
				    "label.required.is");
		
		  
		 if(!(emailPasswordForm.getNewPassword().equals(emailPasswordForm.getReNewPassword()))){
				errors.rejectValue("reNewPassword", "label.password.dont.match");
		  }
		  
		  if (!(emailPasswordForm.getNewPassword().isEmpty())) {  
			  if (emailPasswordForm.getNewPassword().trim().length() < EnumLabel.labelNumber.MinimumPasswordLength.getNumber()) {   
				  errors.rejectValue("newPassword", "label.password.short"); 
			  }
		  }
		  
		  

	}
	
}