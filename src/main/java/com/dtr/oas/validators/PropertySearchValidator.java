package com.dtr.oas.validators;

import com.dtr.oas.form.PropertyForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.model.PropertySearch;

@Component
public class PropertySearchValidator implements Validator{


	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return PropertySearch.class.isAssignableFrom(c);
	}

	public boolean isNonIntegerDouble(String value) {
		try {
			Double.parseDouble(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	@Override
	public void validate(Object obj, Errors errors) {

		PropertySearch propertySearch = (PropertySearch)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country",
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "province",
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city",
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyType",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertySaleType",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyPriceMin",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyAge",
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyNumOfBedrooms",
				"label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyNumOfBathrooms",
				"label.required.is");

		if(propertySearch.getPropertyPriceMin() != 0 || String.valueOf(propertySearch.getPropertyPriceMin()) != ""){
			if(!isNonIntegerDouble(String.valueOf(propertySearch.getPropertyPriceMin()))){
				errors.rejectValue("propertyPriceMin", "label.price.incorrect");
			}
			if(propertySearch.getPropertyPriceMin() < 0){
				errors.rejectValue("propertyPriceMin", "label.price.incorrect");
			}
		}
	}
	
}