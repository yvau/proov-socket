package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.model.Proposal;

@Component
public class BuyingProposalValidator implements Validator{  


	@Override
	public boolean supports(Class<?> c) {
		//just validate the owningProposalr instances
	        return Proposal.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		
		Proposal owningProposal = (Proposal)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.country.id",  
			    "required.location.country.id", "the country is required.");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.province.id",  
			    "required.location.province.id", "the province is required.");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.city.id",  
			    "required.location.city.id", "the city is required.");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyTypeInProposal",  
			    "required.propertyTypeInProposal", "property type is required.");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "minimumPrice",  
			    "required.minimumPrice", "the minimum price is required.");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maximumPrice",  
			    "required.maximumPrice", "the maximum price is required.");
		
		if (owningProposal.getMaximumPrice() < owningProposal.getMinimumPrice()) {   
			  errors.rejectValue("maximumPrice", "profile.password.length"); 
		  }
	}
	
}