package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.form.EmailPasswordForm;

@Component
public class RoleValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return EmailPasswordForm.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role",  
				    "label.required.is"); 

	}
	
}