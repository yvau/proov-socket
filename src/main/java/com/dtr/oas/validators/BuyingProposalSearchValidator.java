package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.model.ProposalSearch;

@Component
public class BuyingProposalSearchValidator implements Validator {


    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return ProposalSearch.class.isAssignableFrom(c);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        ProposalSearch proposalSearch = (ProposalSearch) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country",
                "label.required.is");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "province",
                "label.required.is");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city",
                "label.required.is");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyTypeInProposal",
                "label.required.is");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "minimumPrice",
                "label.required.is");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maximumPrice",
                "label.required.is");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rushPriority",
                "label.required.is");

        if (proposalSearch.getMaximumPrice() == 0 && proposalSearch.getMinimumPrice() == 0) {
            errors.rejectValue("minimumPrice", "label.required.is");
            errors.rejectValue("maximumPrice", "label.required.is");
        }

        if (String.valueOf(proposalSearch.getMaximumPrice()).equals(String.valueOf(proposalSearch.getMinimumPrice()))) {
            errors.rejectValue("maximumPrice", "label.price.lesser");
        }
    }

}