package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dtr.oas.form.MembershipForm;

@Component
public class MembershipValidator implements Validator{


	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return MembershipForm.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		
		MembershipForm membershipForm = (MembershipForm)obj;
		
		if(membershipForm.getMembershipPaymentMethod().equals("card")){
			if (membershipForm.getNameOnCard().isEmpty()){
				errors.rejectValue("password", "label.password.length"); 
			}
			if (membershipForm.getMembershipPaymentCardNumber().isEmpty()) {
				errors.rejectValue("password", "label.password.length");
			}
			if(membershipForm.getExpirationDate().isEmpty()) {
				errors.rejectValue("password", "label.password.length");
			}
			if(membershipForm.getVerifyNumber().isEmpty()) {
				errors.rejectValue("password", "label.password.length");
			}
		}
		
		

		}
	
}