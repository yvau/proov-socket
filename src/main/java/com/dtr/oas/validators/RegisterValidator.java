package com.dtr.oas.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.enumeration.EnumLabel;
import com.dtr.oas.model.Profile;
import com.dtr.oas.service.ProfileService;

import org.springframework.beans.factory.annotation.Autowired;

@Component
public class RegisterValidator implements Validator {

  private Pattern pattern;
  private Matcher matcher;
  private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
      + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  @Autowired
  private ProfileService profileService;


  @Override
  public boolean supports(Class<?> c) {
    //just validate the profiler instances
    return Profile.class.isAssignableFrom(c);
  }

  @Override
  public void validate(Object obj, Errors errors) {

    Profile profile = (Profile) obj;

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "label.required.is");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "label.required.is");

    if (!(profile.getPassword().isEmpty())) {
      if (profile.getPassword().trim().length() < EnumLabel.labelNumber.MinimumPasswordLength.getNumber()) {
        errors.rejectValue("password", "label.password.length");
      } else if (!(profile.getPassword().equals(profile.getRepassword()))) {
        errors.rejectValue("repassword", "label.password.dont.match");
      }
    }

    String email = profile.getEmail();

    // email validation in spring
    if (!(email.isEmpty())) {
      pattern = Pattern.compile(EMAIL_PATTERN);
      matcher = pattern.matcher(email);
      if (!matcher.matches()) {
        errors.rejectValue("email", "label.email.incorrect");
      } else if (profileService.findByEmail(email) != null) {
        errors.rejectValue("email", "label.email.found");
      }
    }
  }

}