package com.dtr.oas.validators;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.model.Profile;

@Component
public class AvailabilityValidator implements Validator{
	
	private static final int MINIMUM_PASSWORD_LENGTH = 6;
	private Pattern pattern;  
	private Matcher matcher;  	  
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
	   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";   



	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return Profile.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		
		Profile profile = (Profile)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",  
			    "required.password", "the password is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname",  
			    "required.lastname", "the lastname is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gender",  
			    "required.gender", "the gender is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role",  
			    "required.role", "the role is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email",  
			    "required.email", "the email is required.");
		
		
		
		if (!(profile.getPassword().isEmpty())) {  
			  if (profile.getPassword().trim().length() < MINIMUM_PASSWORD_LENGTH) {  
				  errors.rejectValue("password", "profile.password.length"); 
			  }
			  else if(!(profile.getPassword().equals(profile.getRepassword()))){
					errors.rejectValue("repassword", "MustMatch.profile.repassword");
			}
		  }

		String email = profile.getEmail();
		
		// email validation in spring  
		if (!(email.isEmpty())) {  
		   pattern = Pattern.compile(EMAIL_PATTERN);  
		   matcher = pattern.matcher(email);  
		   if (!matcher.matches()) {  
		    errors.rejectValue("email", "email.incorrect", "Enter a correct email");  
		   }
		   
		   /*else if(matcher.matches() && (profileRepository.findByEmail(email) != null)){*/
		   else if(matcher.matches()){
			errors.rejectValue( "email", "Exist.profile.email");
		   }
		 }
	}
	
}