package com.dtr.oas.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.model.ProposalSearch;

@Component
public class RentingProposalSearchValidator implements Validator{


	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return ProposalSearch.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		
		ProposalSearch proposalSearch = (ProposalSearch)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "province",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyTypeInProposal",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "minimumPrice",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maximumPrice",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bathroomsMin",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bedroomsMin",  
			    "label.required.is");

		if (proposalSearch.getMaximumPrice() == 0 && proposalSearch.getMinimumPrice() == 0) {
			errors.rejectValue("minimumPrice", "label.required.is");
			errors.rejectValue("maximumPrice", "label.required.is");

		}

		if (String.valueOf(proposalSearch.getMaximumPrice()).equals(String.valueOf(proposalSearch.getMinimumPrice()))) {
			errors.rejectValue("maximumPrice", "label.price.lesser");

		}

		if(!proposalSearch.getBathroomsMin().isEmpty() && !proposalSearch.getBathroomsMax().isEmpty()) {
			if (Integer.parseInt(proposalSearch.getBathroomsMax()) < Integer.parseInt(proposalSearch.getBathroomsMin())) {
				errors.rejectValue("bathroomsMin", "label.bathrooms.lesser");

			}
		}

		if(!proposalSearch.getBedroomsMin().isEmpty() && !proposalSearch.getBedroomsMax().isEmpty()) {
			if (Integer.parseInt(proposalSearch.getBedroomsMax()) < Integer.parseInt(proposalSearch.getBedroomsMin())) {
				errors.rejectValue("bathroomsMin", "label.bedrooms.lesser");

			}
		}

		
	}
	
}