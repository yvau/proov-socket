package com.dtr.oas.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.form.EmailPasswordForm;
import com.dtr.oas.service.ProfileService;

@Component
public class RecoverValidator implements Validator{
	
	private Pattern pattern;  
	private Matcher matcher;  
	
	@Autowired
	private ProfileService profileService;
	  
	 private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
	   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";   


	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return EmailPasswordForm.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		
		EmailPasswordForm recoverForm = (EmailPasswordForm)obj;
		String email = recoverForm.getEmail();
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email",
				"label.required.is");
		   
		// email validation in spring  
			if (!(email.isEmpty())) {  
			   pattern = Pattern.compile(EMAIL_PATTERN);  
			   matcher = pattern.matcher(email);  
			   if (!matcher.matches()) {  
			    errors.rejectValue("email", "label.email.incorrect"); 
			   }
			   
			   else if(profileService.findByEmail(email) == null){
				   errors.rejectValue("email", "label.email.found.not"); 
				}
			 }


	}
	
}