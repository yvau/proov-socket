package com.dtr.oas.validators;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dtr.oas.form.PropertyForm;
import com.dtr.oas.model.Property;

@Component
public class PropertyValidator implements Validator{ 


	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return Property.class.isAssignableFrom(c);
	}
	
	public boolean isNonIntegerDouble(String value) {
	    try {
	        Double.parseDouble(value);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		
		PropertyForm propertyForm = (PropertyForm)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.country.id",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.province.id",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.city.id",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zipCode",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "saleType",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyAge",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bathrooms",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bedrooms",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sqfeet",  
			    "label.required.is"); 
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "floorLevel",  
			    "label.required.is");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyFeatures",  
			    "label.required.is"); 
		
		if (propertyForm.getPropertyPrice().isEmpty()) {   
			  errors.rejectValue("propertyPrice", "label.price.required"); 
		 }
		

		if (!(propertyForm.getPropertyPrice().isEmpty())) {  
			  boolean match = isNonIntegerDouble((String.valueOf(propertyForm.getPropertyPrice())));  
			   if (!match) {  
			    errors.rejectValue("propertyPrice", "label.price.incorrect");  
			   } else  if(match){
					 if (Double.parseDouble(propertyForm.getPropertyPrice()) < 0) {
							errors.rejectValue("propertyPrice", "label.price.incorrect"); 
						 }
					   }
		}


	}
	
}