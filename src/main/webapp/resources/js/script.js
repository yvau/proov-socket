(function($) {
  'use strict';

  $('form.ejb_eb').submit(function(event) {
    var $form = $(this).closest('form.ejb_eb');
    $form.find(':submit').button('loading');
    $.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      contentType: 'application/x-www-form-urlencoded',
      data: $form.serialize(),
      /* a remettre when 1mplement1ng ajax log1n pour vo1r
        statusCode:  {
      302: function (response) {
        console.log("test");
        console.log(response.target);

      }},*/
      success: function(response) {
        $form.find(':submit').button('reset');
        //$form.find('.form-group').removeClass('has-error');
        $form.find('div').removeClass('has-error');
        $form.find('.help-block').empty();
        $form.find('.alert').remove();
        if (response.status == 'FAIL') {
          for (var i = 0; i < response.errorMessageList.length; i++) {
            var item = response.errorMessageList[i];
            var $controlGroup = $('#' + item.fieldName.toString().replace(/\./g, ''));
            $controlGroup.addClass('has-error');
            //$('div#form-group').addClass('has-error');
            //console.log($controlGroup);
            $controlGroup.find('.help-block').html(item.message);
            console.log(item.fieldName.toString().replace(/\./g, ''));
            console.log(item.message);
          }
        } else {
          if (response.action == 'HIDE') {
            //$("div.list"+response.target).hide(1000);
            $("#" + response.target).modal('hide');
            console.log("#" + response.target);
          } else if (response.action == 'REDIRECT') {
            window.location.href = response.target;
          } else if (response.action == 'INVOKE') {
            callbacks[response.target](response.arguments);
          }
        }

      },
      error: function(jqXhr, textStatus, errorThrown) {
        $form.find(':submit').button('reset');
        console.log(errorThrown);
      }
    });

    event.preventDefault();
  });
/*----------------------------------------------------------------------------*/
  var calc = $("#ejb_Form").calx();
  //var $validator = $("#qualForm").validate();
  var abd = 33;
  var atd = 41;
  var ratePerYear = 0.001899292;
  var depreciationPeriod = 25;
  var acompte = 20;


$.validator.addMethod("greaterThan", function (value, element, param) {
  return $('#maximumPrice').val() > $('#minimumPrice').val();
}, "This field must be greater than minimumPrice");


var $validator = $( "#ejb_Form" ).validate({
  onsubmit: true,
  rules: {
    maximumPrice: {
      greaterThan: true
    }
  }
});
/*--------------------------------------------------------------*/
  var $sections = $('.form-section');

  function navigateTo(index) {
    // Mark the current section with the class 'current'
    $sections
      .removeClass('current')
      .eq(index)
        .addClass('current');
    // Show only the navigation buttons that make sense for the current section:
    $('.form-navigation .previous').toggle(index > 0);
    var atTheEnd = index >= $sections.length - 1;
    $('.form-navigation .next').toggle(!atTheEnd);
    $('.form-navigation [type=submit]').toggle(atTheEnd);
  }

  function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
  }

  // Previous button is easy, just go back
  $('.form-navigation .previous').click(function() {
    navigateTo(curIndex() - 1);
  });

  // Next button goes forward iff current block validates
  $('.form-navigation .next').click(function() {
    if ($('.js_step_form').parsley().validate({group: 'block-' + curIndex()}))
      navigateTo(curIndex() + 1);
  });

  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
  $sections.each(function(index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);
  });
  navigateTo(0); // Start at the beginning
  /*---------------------------------------------------------*/
 /* $(".nxt_btn").click(function() { // Function Runs On NEXT Button Click
    var $valid = $("#ejb_Form").valid();
    if (!$valid) {
      $validator.focusInvalid();
      return false;
    }
    nxtSection();
  });*/

  $(".next_btn").click(function() { // Function Runs On NEXT Button Click
    var $valid = $("#ejb_Form").valid();
    if (!$valid) {
      $validator.focusInvalid();
      return false;
    }
    nextSection();
  });

  $(".pre_btn").click(function() { // Function Runs On PREVIOUS Button Click
    prevSection();
  });

  function goToSection(i, beforeLast) {
    $("fieldset:gt(" + i + ")").removeClass("current").addClass("none");
    $("fieldset:lt(" + i + ")").removeClass("current").addClass("none");
    $("fieldset").eq(i).removeClass("none").addClass("current");

  }

  function prevSection() {
    var i = $("fieldset.current").index();
    console.log(i);
    var beforeLast = $("fieldset").last().index() - 1;
    if (i === ($("fieldset").length - 1)) {
      //$("li").eq(i++).addClass("active");
      $("li.next").show();
      $("li.finish").hide();
      goToSection(i - 1, beforeLast);
    } else {
      $("li.previous").hide();
      goToSection(i - 1, beforeLast);
    }
  }

  function nextSection() {
    var i = $("fieldset.current").index();
    var beforeLast = $("fieldset").last().index() - 1;
    $("li.previous").show();
    if (i < beforeLast) {

      if (i == 0) {
        var mortgageAtd = calculateMortgageAtd();
        var mortgageAbd = calculateMortgageAbd();
        var mortgage = (relationAbdAtd().formulaAbd > abd && relationAbdAtd().formulaAtd) > atd ? mortgageAtd : mortgageAbd;
        calc.calx('setValue', 'B8', mortgage);
        calc.calx('registerVariable', 'maximum_borrowing_capacity_var', function() {
          return calculateMaximumBorrowingCapacity(ratePerYear, $('select#depreciationPeriod').val(), mortgage);
        });
      } else if (i == 1) {

        $("#tab3 input").on('change', function() {
          var cell = $(this).attr('data-cell');

          calc.calx('getSheet').getCell(cell).setFormula('');
          calc.calx('getSheet').getCell(cell).setValue($(this).val());
        });
      }
      //$("li").eq(i++).addClass("active");
      goToSection(i + 1, beforeLast);
    } else {
      console.log("show submit");
      $("li.next").hide();
      $("li.finish").show();
      goToSection(i + 1, beforeLast);
    }
  }

  function nxtSection() {
    var i = $("fieldset.current").index();
    var beforeLast = $("fieldset").last().index() - 1;
    $("li.previous").show();
    if (i < beforeLast) {
      //$("li").eq(i++).addClass("active");
      goToSection(i + 1, beforeLast);
    } else {
      $("li.next").hide();
      $("li.finish").show();
      goToSection(i + 1, beforeLast);
    }
  }

  function checkSolvability() {
    totalIncome = calc.calx('getCell', 'H1').getValue();
    totalMonthlyExpenses = calc.calx('getCell', 'H2').getValue();
    var solvability = totalIncome / totalMonthlyExpenses;
    console.log(solvability);
    return solvability;
  }

  function calculateMaximumBorrowingCapacity(ratePerYear, depreciationPeriod, mortgageLoanMonthly) {
    depreciationPeriod = depreciationPeriod * 12;
    var equationTop = Math.pow(1 + ratePerYear, -depreciationPeriod);
    var calculMaximumBorrowingCapacity = mortgageLoanMonthly * ((1 - equationTop) / ratePerYear);

    return calculMaximumBorrowingCapacity.toFixed(1);
  }

  function calculateMortgageAtd() {
    var mortgageatd = 0;
    var j = 0;
    //determining mortgage atd 40%
    for (j = 0; j < 41; j++) {
      mortgageatd++;
      j = (((mortgageatd + calc.calx('getCell', 'A9').getValue() + calc.calx('getCell', 'A7').getValue() + (feeCondo / 2)) + debtsCreditCard + debtsCarLoans + debtsOtherExpenses) / (calc.calx('getCell', 'AT1').getValue() / 12)) * 100;
    }
    return mortgageatd;
  }

  function calculateMortgageAbd() {
    var mortgageabd = 0;
    var i = 0;
    //determining mortgage abd 32%
    for (i = 0; i < 33; i++) {
      mortgageabd++;
      i = ((mortgageabd + calc.calx('getCell', 'A9').getValue() + calc.calx('getCell', 'A7').getValue()) / (calc.calx('getCell', 'AT1').getValue() / 12)) * 100;
    }
    return mortgageabd;
  }

  function relationAbdAtd() {
    //ABD
    var formulaAbd = calculateMortgageAbd();
    //ATD
    var formulaAtd = calculateMortgageAtd();
    return {
      'formulaAbd': formulaAbd,
      'formulaAtd': formulaAtd
    };
  }

  function totalOfMortgage(downPayment, maximumBorrowingCapacity) {
    var totalOfMortgage = maximumBorrowingCapacity - downPayment;

    return totalOfMortgage;
  }


 $('.js-delete-picture').click(function(e) {
  e.preventDefault();
     if (confirm("Are you sure you want to delete")) {
        var url = $(this).attr("href");
        $.ajax({
            url:url,
            type: 'DELETE',
            success: function(result) {
                // Do something with the result
                location.reload();
            }
        });
       }
   });

/*---------------------------------------------------------------------------------------*/
  $('.ajaxList').change(function(event) {
    var select = $(this);
    var id = '#' + select.data('target');
    $.get(select.data('url'), {
      id: select.val()
    }, function(data) {
      if (data.error) {
        alert(data.error);
      } else {
        var target = $(id).get(0);
        target.options.length = 0;
        for (var i in data) {
          var result = data[i];
          target.options[i] = new Option(result.name, result.id, false, false);
        }
        target.add(new Option("-- Select " + select.data('target') + " --", "", true, true), target.options[0]);
      }
    }, 'json');
  });

  $('.js_form_search').on('change', function(e) {
    var url, urlLoadContent;
    //$("div#loadingContent").removeClass("display-hide");
    if (this.value == null || this.value == '') {
      url = removeParam(this.name, window.location.toString()).url;
      urlLoadContent = removeParam(this.name, window.location.toString()).urlLoadContent;
    } else {
      url = addParam(this.name, this.value).url;
      url = removeParam('page', url).url;
      urlLoadContent = addParam(this.name, this.value).urlLoadContent;
      urlLoadContent = removeParam('page', url).urlLoadContent;
    }
    history.pushState({path: url}, '', url);loadContent(urlLoadContent, ".js_return_list");
    e.preventDefault();
  });

  $('.js_form_orderby').on('change', function(e) {
    var urlLoadContent, value = this.value, direction = $(this).find(':selected').data('dir'),url;
    url = addParam("sorting", value).url;
    urlLoadContent = removeParam('page', url).urlLoadContent;
    url = removeParam('page', url).url;

    history.pushState({path: url}, '', url);loadContent(urlLoadContent, ".js_return_list");
    e.preventDefault();
  });

  function loadContent(url, content) {
    $.ajax({
      url: url,
      success: function(data) {
        $(content).html(data);
      }
    });
    return false;
  }

  function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
      param, params_arr = [],
      queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
      params_arr = queryString.split("&");
      for (var i = params_arr.length - 1; i >= 0; i -= 1) {
        param = params_arr[i].split("=")[0];
        if (param === key) {
          params_arr.splice(i, 1);
        }
      }
      rtn = rtn + "?" + params_arr.join("&");
    }

    return {
      'url': rtn,
      'urlLoadContent': rtn + "&rel=tab"
    };
  }

  function addParam(param, value) {
    var a = document.createElement('a'),
      regex = /(?:\?|&amp;|&)+([^=]+)(?:=([^&]*))*/g;
    var match, str = [];
    a.href = window.location.href;
    param = encodeURIComponent(param);
    while (match = regex.exec(a.search))
      if (param != match[1]) str.push(match[1] + (match[2] ? "=" + match[2] : ""));
    str.push(param + (value ? "=" + encodeURIComponent(value) : ""));
    a.search = str.join("&");
    //a.href = removeParam('page', a.href).url;

    //return a.href;
    return {
      'url': a.href,
      'urlLoadContent': a.href + "&rel=tab"
    };
  }


  Date.prototype.dateTimeNow = function() {

    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();
    var hh = this.getHours().toString();
    var gg = this.getMinutes().toString();
    var ss = this.getSeconds().toString();

    return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]) + ' ' + (hh[1] ? hh : "0" + hh[0]) + ':' + (gg[1] ? gg : "0" + gg[0]) + ':' + (ss[1] ? ss : "0" + ss[0]);
  };

  Date.prototype.dateNow = function() {

    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();

    return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
  };


  function dateCountDown(dateParam, selector) {

    var dateParamWithTime = dateParam.substring(0, dateParam.lastIndexOf('.') + 0);
    var dateParamWithDate = dateParam.substring(0, dateParam.lastIndexOf(' ') + 0);
    var value = "";
    var counter = setInterval(function() {

      if ((moment(displayTime().dateNow()).isBefore(dateParamWithDate)) && (moment(displayTime().dateTimeNow()).isBefore(dateParamWithTime))) {
        value = moment(dateParam, "YYYY-MM-DD HH:mm:ss.fff").calendar();
      } else if ((moment(displayTime().dateNow()).isSame(dateParamWithDate)) && (moment(displayTime().dateTimeNow()).isBefore(dateParamWithTime))) {
        value = moment(dateParamWithTime).fromNow();
      } else {
        value = "";
        clearInterval(counter);
      }
      selector.html(value);

    }, 1000);
  }

  function displayTime(){
  var serverDate=new Date($("table.js-data-countdown").data('time-now'));
  serverDate.setSeconds(serverDate.getSeconds() + 1);
  return serverDate;
  }


  $('[data-countdown]').each(function() {
  console.log(displayTime());
   var $this = $(this),finalDate = $(this).data('countdown');
    dateCountDown(finalDate,$this);
  });


//
function triggerBinding(){

	var $checkboxes = $('.table-list .table-list-cell input[type="checkbox"]');
    $checkboxes.on( "click" , function(){
        var value = $(this).val();

        if($checkboxes.filter(':checked').length > 0){
     		$("button[name='trigger']").removeClass( "disabled");
     	} else{
     		$("button[name='trigger']").addClass("disabled");
     	}

        console.log( value);
    });

    $("button[name='trigger']").on('click', function(e) {
    	e.preventDefault();
    	var form = $(this).closest("form#listingProperties");
    	$("button[name='trigger']").addClass( "disabled");
    	$.post(form.attr('action'), form.serialize(), function(data) {
        	/*$('#results').html(data);*/
        	$("button[name='trigger']").removeClass( "disabled");
        	location.reload();

    	});
  	});

};

function triggerRole(){
var one = $("label.js_form_role:nth-child(1)").hasClass("active");
var two = $("label.js_form_role:nth-child(2)").hasClass("active");
$("label.js_form_role").on('change', function() {

if(one != $("label.js_form_role:nth-child(1)").hasClass("active") || two != $("label.js_form_role:nth-child(2)").hasClass("active") ){
    $("button[type='submit']").removeClass( "disabled");
  } else{
    $("button[type='submit']").addClass("disabled");
  }
});

};

triggerRole();

$('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
});


//
function loadSearchPopUp() {

    if (! $('#js_searchPropertyTab').hasClass( "none" )) {
        $( "#js-property-search-list" ).load( "/oas/profile/property_search/", function() {
        $("#js-property-search-list").removeClass( "module");
      });
    }

    if (! $('#js_searchRentTab').hasClass( "none" )) {
        $( "#js-rent-search-list" ).load( "/oas/profile/rent_search/", function() {
          $("#js-rent-search-list").removeClass( "module");
          $(".js_modal_rent_show").click( function(e) {
            loadModalTemplate($(this).data('url'), $(this).data('value'));
          });

      });
    }

    if (! $('#js_searchBuyTab').hasClass( "none" )) {

      $( "#js-buy-search-list" ).load( "/oas/profile/buy_search/", function() {
          $("#js-buy-search-list").removeClass( "module");
          $(".js_modal_buy_show").click( function(e) {
            loadModalTemplate($(this).data('url'), $(this).data('value'));
          });
      });

    }

    if ((! $('#js_searchBuyTab').hasClass( "none" ))  || (! $('#js_searchRentTab').hasClass( "none" )) || (! $('#js_searchPropertyTab').hasClass( "none" ))) {

      $('.center-top-menu li:not(.none):first').children().tab('show');

    }

};

if($('#sidebar_explore').hasClass( "active" )){
  loadSearchPopUp();
}


var callbacks = {
    createCriteria : function(argument) {
      $("#" + argument[0]).modal('hide');
      $("div.tab-content>div.active").removeClass("active");
      $("ul.center-top-menu>li.active").removeClass("active");
      $("#js_search" + argument[1] + "Link").removeClass("none");
      $("#js_search" + argument[1] + "Tab").removeClass("none");
      $("#js_search" + argument[1] + "Tab").tab('show');
      $("#js_search" + argument[1] + "Link").addClass("active");
      $("#js_search" + argument[1] + "Tab").addClass("active");
      $("#js_modal" + argument[1] + "Criteria").addClass("none");
      $( "#js-" + argument[1].toLowerCase() + "-search-list" ).load( "/oas/profile/" + argument[1].toLowerCase() + "_search/" , function() {
          $("#js-" + argument[1].toLowerCase() + "-search-list").removeClass( "module");
      });
    },
    successFunctionFromModal : function(argument) {
      $( "#request_for_" + argument[0]).hide();
      $("#scheduleAppointement").modal('hide');
          callbacks['bootstrapAlert'](argument[1]);
            if(argument[2] === "0"){
              $("#js_notification_" + argument[4] + "_" + argument[3]).hide();
              $(".module-section").removeClass("none");
              $("span.js_show_notification_" + argument[4]).addClass("none");
            }
    },
    removeCriteria : function(argument) {
      $("div.tab-content>div.active").removeClass("active");
      $("ul.center-top-menu>li.active").removeClass("active");
      $("#js_search" + argument[1] + "Link").addClass("none");
      $("#js_search" + argument[1] + "Link").addClass("none");
      $("#js_search" + argument[1] + "Tab").addClass("none");
      $("#js_modal" + argument[1] + "Criteria").removeClass("none");
      $('.center-top-menu li:not(.none):first').children().tab('show');
    },
    bootstrapAlert : function(message) {
                $('#alert_placeholder')
                .html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>'+message+'</span></div>');
    },
    successFunctionRequestScheduled : function(argument) {
    $( "#request_for_" + argument[0]).hide();
    callbacks['bootstrapAlert'](argument[1]);
      if(argument[2] === "0"){
        $("#js_notification_" + argument[4] + "_" + argument[3]).hide();
        $(".module-section").removeClass("none");
        $("span.js_show_notification_" + argument[4]).addClass("none");
      }
    },
    bindings : function(argument){
      //callbacks['bootstrapAlert'](argument[0]);
      $("#con-existing-property-modal").modal('hide');
    }

};



function loadModalTemplate(url, value) {
  var tpl = $('#render-content').html();
  $.get(url,{ }, function(data){
        if ( data.length == 0 ) {
          console.log("NO DATA!")
        }
        $('#con-existing-property-modal').modal('show') ;
        var rendered = Handlebars.compile(tpl)({data :data});
        $(".js_render_list").empty();
        $(".js_render_list").append(rendered);
        $('#loader').remove();

        //$('.ejb_eb').trigger('reset');
        $('input[type=hidden]').each(function() {
          $(this).val(value);
        });

        triggerBinding();
      },'json');
};



function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
};

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
};

function eraseCookie(name) {
    createCookie(name,"",-1);
};

  $('.js_session_storage').on('click',function(e){

    createCookie('binding', $(this).data('binding'), 1);
    createCookie('value', $(this).data('value'), 1);
    $('#scheduleAppointement').modal('show');

  });

  $('#scheduleAppointement').on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget);

   // Button that triggered the modal
  var modal = $(this);
  var data_value = button.data('value');
  var valTrue = modal.find('form').hasClass("js_binding_form");
  if(valTrue){
    modal.find('form').attr("action", "/oas/property/" + readCookie('value') + "?response=accepted&bid=" + readCookie('binding'));
  }else{
    modal.find('form').attr("action", "/oas/property/" + data_value);
  }

  //data_value = modal.find('form').attr("action");


    rome(inl, {
  min: moment(new Date().dateNow()).add(1, 'days'),
  timeValidator: function (d) {
    var m = moment(d);
    var start = m.clone().hour(8).minute(59).second(59);
    var end = m.clone().hour(20).minute(0).second(1);
    return m.isAfter(start) && m.isBefore(end);
  }
}).on('data', function (value) {
  inlv.innerText = inlv.textContent = value;
  document.getElementById("dateAppointment").value = value;
  $("button[name='save']").removeClass( "disabled");
});
  });


  Dropzone.options.uploadDropzone = {

    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 3,
    maxFiles: 3,
    maxFilesize: 10,

    // Dropzone settings
    init: function() {
      var myDropzone = this;

      $(".js_button_upload").click( function(e) {
        e.preventDefault();
        e.stopPropagation();
        myDropzone.processQueue();
      });
      this.on("addedfile", function(file) {
        // Create the remove button
        var removeButton = Dropzone.createElement("<a href='javascript:;' class='text-center btn-sm btn btn-success'>Remove file</a>");
        // Capture the Dropzone instance as closure.
        var _this = this;
        // Listen to the click event
        removeButton.addEventListener("click", function(e) {
          // Make sure the button click doesn't submit the form:
          e.preventDefault();
          e.stopPropagation();

          // Remove the file preview.
          _this.removeFile(file);
          // If you want to the delete the file on the server as well,
          // you can do the AJAX request here.
        });

        // Add the button to the file preview element.
        file.previewElement.appendChild(removeButton);
      });
      this.on("sendingmultiple", function() {});
      this.on("successmultiple", function(files, response) {
        //loadContent(window.location.pathname, ".js_return_carousel");
      });
      this.on("errormultiple", function(files, response) {});
    }

  };



var radionButtonOnChange = function() {
    var form = $('form[name="subscription"]'),
        radio = $('input[name="membershipPaymentMethod"]'),
        choice = 'paypal';

    radio.change(function(e) {
        choice = this.value;

        if (choice === 'paypal') {
            $( "div .card" ).addClass( "none" );
            $( "button" ).addClass( "btn-warning" );
            $( "button" ).removeClass( "btn-success" );
        } else {
            $( "div .card" ).removeClass( "none" );
            $( "button" ).removeClass( "btn-warning" );
            $( "button" ).addClass( "btn-success" );
        }
    });
};

radionButtonOnChange();


  $(".js_submit_link").on('click', function(e) {
    e.preventDefault();
    var link = $(this).data('remote');

    $.post(link, function(data) {
      callbacks[data.target](data.arguments);
    });
  });



  var stompClient = null;

  function connect() {
    var socket = new SockJS('/oas/hello');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
      //setConnected(true);
      console.log('Connected: ' + frame);
      stompClient.subscribe("/user/queue/note", function(message) {
      getNotificationMessage(JSON.parse(message.body));
      });
      stompClient.subscribe("/app/positions", function(message) {
        console.log(JSON.parse(message.body));
        loadNotification(JSON.parse(message.body));
      });
    });
  };

  function getNotificationMessage(data){
   $(".js_show_notification_" + data["css"].toString()).removeClass('none');
   $("#js_notification_badge").hasClass("badge-danger") ? $("#js_notification_badge") : $("#js_notification_badge").addClass("badge-danger badge");
   $("#js_notification_badge").text(data["notificationCount"].toString());
   $.Notification.notify('custom','top right', 'Notification', data["notifier"].toString());
  };

function loadNotification(data) {
  var tpl = $('#js_notification_list').html();
        if ( data.length == 0 ) {
          console.log("NO DATA!")
        }
        var rendered = Handlebars.compile(tpl)({data :data});
        $(".js_notification_list").append(rendered);
        $('#loader').remove();
};

  function disconnect() {
    if (stompClient != null) {
      stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
  };
  connect();


})(jQuery);
